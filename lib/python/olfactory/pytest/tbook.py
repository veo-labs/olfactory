import asyncio
import re
import textwrap
import jinja2
import functools
import ast
import sys
import json
from olfactory import config, argphlp
from olfactory.pytest import base


_tbook_state = None


INLINE_TEMPLATE_PREFIX = 'tbook/inline/'
INLINE_TEMPLATE_SEPARATOR = '!!'
INLINE_TEMPLATE_METHODS = {}

STATUS_VALUES = {'unknown': 0, 'passed': 5, 'failed': 10}

def tbook_pragma(*args,**kwargs):
    pass


def inline_template(name):
    def _(f):
        global INLINE_TEMPLATE_METHODS
        INLINE_TEMPLATE_METHODS[name]=f
        return f
    return _


DIRECTIVES = {}


def directive(name):
    def _(f):
        global DIRECTIVES
        DIRECTIVES[name]=f
        return f
    return _


FILTERS = {}


def filter(name):
    def _(f):
        global FILTERS
        FILTERS[name]=f
        return f
    return _


class Master:
    def __init__(self):
        global _tbook_state
        if _tbook_state is not None:
            self.__dict__ = _tbook_state
            return
        _tbook_state = self.__dict__
        self._cur_tst = None
        self._tests = []
        self._tests_by_nodeids = {}
        self._source_maps = {}
        self._output_filename = None

    def isOn(self):
        if self._output_filename is not None:
            return True
        else:
            return False

    def forgetCurrentTest(self):
        self._cur_tst = None

    def pytest_unconfigure(self,config):
        if not self.isOn():
            return
        self.renderSkeleton()

    def pytest_runtest_logreport(self,report):
        if not self.isOn():
            return
        tst = self._cur_tst
        if tst is None:
            return
        vc = STATUS_VALUES[tst._status]
        vn = STATUS_VALUES[report.outcome]
        if vn > vc:
            tst._status = report.outcome
        tst._duration = max(tst._duration, report.duration)
        if len(report.longreprtext) > 0:
          tst._tracebacks.append(report.longreprtext)

    def renderSkeleton(self):
        loader = self._createTemplateLoader()
        env = jinja2.Environment(loader=loader)
        self._addFilters(env)
        symbols = self._createTemplateSymbols()
        t = env.get_template('tbook/skeleton')
        content = t.render(**symbols)
        if self._output_filename=='-':
            f = sys.stdout
        else:
            f = open(self._output_filename, 'w')
        f.write(content)
        if self._output_filename!='-':
            f.close()

    def _addFilters(self, env):
        filters = env.filters
        for n,f0 in FILTERS.items():
            f = functools.partial(f0,self)
            if n in filters:
                raise AssertionError(f'There is alreary a filter named {n!r}')
            filters[n]=f

    def emitRaw(self, rawstring):
        tst = self._cur_tst
        tst._emitted_data += rawstring

    def emitCode(self,code,lexer=None):
        text = self._formatCode(code,lexer)
        self.emitRaw(text)

    def _formatDictionary(self,d):
        text = ''
        text += '.. list-table::\n'
        text += '\n'
        text += '  * - Name\n'
        text += '    - Value\n'
        for k,v in d.items():
            text += f'  * - ``{k}``\n'
            text += f'    - ``{repr(v)}``\n'
        text += '\n'
        return text

    def emitDictionary(self,d):
        text = self._formatDictionary(d)
        self.emitRaw(text)

    def emitTestCode(self,*names):
        code = self._getTestCode(*names)
        text = self.emitCode(code, 'python3')
        return text

    def _getTestCode(self,*names):
        l = len(names)
        if l==0:
            names = [None]
        elif l>1:
            raise AssertionError('only one name allowed for now')
        tst = self._cur_tst
        map = PyTestFileMap(tst._filename)
        map.parse()
        code = map.getCode(names[0],lineno_first=tst._lineno)
        return code

    def _formatCode(self,code,lexer=None):
        code = textwrap.dedent(code)
        code = textwrap.indent(code,'   ')
        if lexer is None:
            text = f'.. code::\n\n{code}\n\n'
        else:
            text = f'.. code:: {lexer}\n\n{code}\n\n'
        return text

    def emit(self,obj):
        if hasattr(obj,'_tbook_emit'):
            obj._tbook_emit(self)
        else:
            raise AssertionError(f"don't know how to insert {obj!r} in tbook")

    def emitTitle(self,title,*,underlining_char='-'):
        tst = self._cur_tst
        line = self._underline(title,underlining_char)
        tst._emitted_data += f"{title}\n{line}\n\n"

    @filter('underline')
    def _underline(self,title, underlining_char):
        l = len(title)
        return underlining_char*l

    def _createTemplateLoader(self):
        conf = config.Config()
        loaders = []
        loaders.append(jinja2.FileSystemLoader("%s/lib/jinja2-templates" % conf.getTopDir()))
        loaders.append(jinja2.FunctionLoader(self._loadInlineTemplate))
        loader = jinja2.ChoiceLoader(loaders)
        return loader

    def _loadInlineTemplate(self, name):
        if not name.startswith(INLINE_TEMPLATE_PREFIX):
            return None
        l = name[len(INLINE_TEMPLATE_PREFIX):].split(INLINE_TEMPLATE_SEPARATOR)
        if len(l) != 2:
            raise ValueError(name)
        tst = self._tests_by_nodeids[l[0]]
        f = INLINE_TEMPLATE_METHODS[l[1]]
        return f(tst)

    def _createTemplateSymbols(self):
        tests = []
        for tst in self._tests:
            cnt = Container(tst._createTemplateSymbols())
            tests.append(cnt)
        return {'tests': tests}


class Pragma:
    def __init__(self,name,*args,lineno):
        self.name = name
        self.args = args
        self.lineno = lineno

    @property
    def a0(self):
        if len(self.args)==0:
            return None
        else:
            return self.args[0]


class PyTestFileMap(ast.NodeVisitor):
    def __init__(self,filename):
        super().__init__()
        self._pragmas = []
        self._depth = -1
        self._linenos_to_depths = {}
        self._linenos_per_depths = {}
        self._filename = filename
        self._lines = None

    def parse(self):
        with open(self._filename,'r') as f:
            self._lines = f.readlines()
        tree = ast.parse(''.join(self._lines), filename=self._filename, mode='exec')
        self.visit(tree)

    def visit(self, node):
        if hasattr(node,'lineno'):
            d = self._depth
            l = node.lineno
            if l not in self._linenos_to_depths:
                self._linenos_to_depths[l]=d
            else:
                self._linenos_to_depths[l] = min(self._linenos_to_depths[l], d)
            if d not in self._linenos_per_depths:
                self._linenos_per_depths[d] = []
            self._linenos_per_depths[d].append(l)
        self._depth += 1
        rval = super().visit(node)
        self._depth -= 1
        if node.__class__!=ast.Call:
            return rval
        if 'id' not in node.func._fields:
            return rval
        if node.func.id!='tbook_pragma':
            return rval
        args = []
        for a in node.args:
            if 's' not in a._fields:
                return rval
            args.append(a.s)
        self._pragmas.append(Pragma(*args,lineno=node.lineno))
        return rval

    def getLastLineno(self,lineno_first):
        delta = 0
        while True:
            l = lineno_first+delta
            if l in self._linenos_to_depths:
                break
            l = lineno_first-delta
            if l in self._linenos_to_depths:
                break
            delta += 1
            if delta>100:
                raise AssertionError
        d = self._linenos_to_depths[l]
        ll = self._linenos_per_depths[d]
        for idx,l in enumerate(ll):
            if l>=lineno_first:
                break
        if idx<(len(ll)-1):
            return ll[idx+1]-1
        else:
            return len(self._lines)

    def getCode(self,name,*,lineno_first=None):
        pairs = []
        curp = None
        for p in self._pragmas:
            if (curp is None) and p.name=='code' and p.a0==name:
                curp = p
            elif (curp is not None) and p.name=='/code' and p.a0==name:
                pairs.append((curp.lineno+1,p.lineno-1))
                curp = None
        if lineno_first is not None:
            lineno_last = self.getLastLineno(lineno_first)
        if len(pairs)==0:
            return '(test_code_not_found)\n'
        for pp in pairs:
            if lineno_first is None:
                break
            elif pp[0]>=lineno_first and pp[1]<=lineno_last:
                break
        lines = self._lines[pp[0]-1:pp[1]]
        content = ''.join(lines)
        return textwrap.dedent(content)


class Directive:
    def __init__(self,tst):
        self._tst = tst
        self._master = tst._master


@directive('render_testcode')
class RenderTestCodeDirective(Directive):
    def __call__(self,*names):
        master = self._master
        code = master._getTestCode(*names)
        text = master._formatCode(code, 'python3')
        return text


@directive('render_fixtures')
class RenderFixtures(Directive):
    def __call__(self,*names):
        d = {}
        master = self._master
        tst = self._tst
        for k in names:
            ctx = {'fixtures': tst._fixtures}
            v = eval(f'fixtures.{k}', ctx, ctx)
            d[k] = v
        text = master._formatDictionary(d)
        return text


@directive('set_brief')
class SetBrief(Directive):
    def __call__(self,brief):
        tst = self._tst
        tst._brief = brief
        return brief


class Container:
    def __init__(self,dct=None):
        if dct is None:
            return
        for k,v in dct.items():
            self.__dict__[k]=v


def normalize_fixtures(fixtures):
    class Encoder(json.JSONEncoder):
        VALID_TYPES = {int, str, bool, list, dict}
        def default(self, obj):
            if type(obj) in self.VALID_TYPES:
                to_string = False
            elif hasattr(obj,'_tbook_encode'):
                return obj._tbook_encode()
            else:
                to_string = True
            if to_string:
                return repr(obj)
            else:
                return super().default(obj)
    content = json.dumps(fixtures,cls=Encoder,indent=4,sort_keys=True)
    fixtures = json.loads(content,object_hook=Container)
    return fixtures


class Test:
    def __init__(self,master,request):
        self._master = master
        self._status = 'unknown'
        self._nodeid = request.node.nodeid
        self._filename = request.module.__file__
        self._lineno = request.node.location[1]
        self._duration = 0
        self._tracebacks = []
        self._brief = ''
        doc = request.node._obj.__doc__
        if doc is None:
            doc = ""
        self._docstring = textwrap.dedent(doc)
        self._rendered_docstring = ''
        cnt = Container()
        for n in INLINE_TEMPLATE_METHODS.keys():
            setattr(cnt, n, f'{INLINE_TEMPLATE_PREFIX}{self._nodeid}{INLINE_TEMPLATE_SEPARATOR}{n}')
        self._inline_templates = cnt
        master._cur_tst = self
        master._tests_by_nodeids[self._nodeid] = self
        master._tests.append(self)
        self._rank = len(master._tests)
        self._emitted_data = ''
        d = {}
        for k in request.fixturenames:
            if k==request.fixturename:
                continue
            v = request.getfixturevalue(k)
            d[k]=v
        self._fixtures = normalize_fixtures(d)

    def renderDoctring(self):
        master = self._master
        if master._output_filename is None:
            return
        env = jinja2.Environment()
        t = env.from_string(self._docstring)
        self._master._addFilters(env)
        symbols = self._createTemplateSymbols()
        self._rendered_docstring = t.render(**symbols)

    def _createTemplateSymbols(self):
        symbols = {}
        for n,obj in DIRECTIVES.items():
            if issubclass(obj,Directive):
                f = obj(self)
            else:
                f = functools.partial(obj,self)
            symbols[n] = f
        for n in 'status', 'nodeid', 'duration', 'brief', 'tracebacks', 'fixtures', 'inline_templates', 'rank', 'has_emitted_data':
            symbols[n] = getattr(self,f'_{n}')
        return symbols

    @property
    def _has_emitted_data(self):
        if len(self._emitted_data) > 0:
            return True
        else:
            return False

    @inline_template('rendered_docstring')
    def _getRenderedDocstring(self):
        return self._rendered_docstring

    @inline_template('emitted')
    def _getEmitted(self):
        return self._emitted_data


class TBookFixture(base.Fixture):
    SUB_FIXTURES='request'

    async def _up(self):
        request = self._getSubFixtureValue('request')
        tbk = Master()
        tst = Test(tbk,request)
        if tbk.isOn():
            tst.renderDoctring()
        return tbk

    async def _down(self,tbk):
        tbk = Master()
        tbk.forgetCurrentTest()


def pytest_configure(config):
    tbk = Master()
    config.pluginmanager.register(tbk)


def pytest_addoption(parser):
    group = parser.getgroup('olfactory tbook')
    tbk = Master()
    group.addoption("--tbkO", "--tbook-output-filename", default=argphlp.ValueOpt(tbk, '_output_filename', name='--tbkO'),
        action='append', help="Test book output filename")


__all__ = [
    'TBookFixture',
    'tbook_pragma',
]
