import pytest
import inspect
import collections


class Fixture:
    SUB_FIXTURES=None

    def __init__(self,names,*,subfixs=None,**ignored):
        self._names = names.split(',')
        self._subfixs_mapping = collections.OrderedDict()
        self._subfixs_values = {}
        self._parseSubFixturesMapping(self.SUB_FIXTURES)
        self._parseSubFixturesMapping(subfixs)
        args = ','.join(self._subfixs_mapping.values())
        code = f'async def _({args}):\n'
        for k,v in self._subfixs_mapping.items():
            code += f'    self._subfixs_values["{k}"] = {v}\n'
        code += '    obj = await self._up()\n'
        code += '    yield obj\n'
        code += '    await self._down(obj)\n'
        self._sub_fixtures = {}
        ctx = {'self': self}
        exec(code, ctx, ctx)
        self.__call__ = ctx['_']

    def _parseSubFixturesMapping(self,subfixs):
        if subfixs is None:
            return
        d = self._subfixs_mapping
        for p0 in subfixs.split(','):
            p = p0.split(':')
            if len(p)==1:
                d[p[0]]=p[0]
            elif len(p)==2:
                d[p[0]]=p[1]
            else:
                raise ValueError(p0)

    def _getSubFixtureValue(self, name):
        return self._subfixs_values[name]

    def inject(self,**kwargs):
        if len(kwargs)==0:
            decorate=pytest.fixture
        else:
            decorate=pytest.fixture(**kwargs)
        fix = decorate(self.__call__)
        frm = inspect.currentframe()
        while frm.f_globals['__name__'] == __name__:
            frm = frm.f_back
        for n in self._names:
            frm.f_globals[n] = fix
        return fix

    async def _up(self):
        pass

    async def _down(self,obj):
        pass


__all__ = ['Fixture']
