import arsenic
import mimetypes
import aiohttp.web
import textwrap
import os
import logging
import asyncio
import random
from olfactory import config, argphlp
from olfactory.pytest import base
from olfactory.ocp import client


logger = logging.getLogger(__name__)


HTTPD_ADDR = '127.0.0.1'
HTTPD_PORT = 9080
HTTPD_BASEURL = f'http://{HTTPD_ADDR}:{HTTPD_PORT}'


class Options:
    def __init__(self):
        self.pause = False

_options = Options()


def pytest_addoption(parser):
    group = parser.getgroup('olfactory devel')
    group.addoption("--wtr-pause",  default=argphlp.ValueOpt(_options,'pause',name="--wtr-pause"),
        action='append_const', const=True, help="Pause web test runner")


class WebTestRunnerFixture(base.Fixture):
    async def _up(self):
        wtr_ = WebTestRunner()
        return wtr_

    async def _down(self,wtr_):
        await wtr_.cleanup()


class WebTestRunner:
    def __init__(self):
        @aiohttp.web.middleware
        async def cache_control(request, handler):
            response: web.Response = await handler(request)
            response.headers.setdefault('Cache-Control', 'no-cache')
            return response
        self._asession = None
        self._happ = aiohttp.web.Application(middlewares=[cache_control])
        self._hrunner = None

    async def startArsenic(self):
        adrv = arsenic.services.Chromedriver(log_file=os.devnull)
        args = ['--disable-gpu', '--headless', '--no-sandbox']
        abrow = arsenic.browsers.Chrome(**{"goog:chromeOptions": {"args": args}})
        self._asession = await arsenic.start_session(adrv, abrow, bind=HTTPD_BASEURL)

    async def _redirect_to_wtr_static(self, request):
        raise aiohttp.web.HTTPFound('/wtr-static')

    async def startHTTPD(self):
        app = self._happ
        conf = config.Config()
        topdir = conf.getTopDir()
        app.router.add_get('/', self._redirect_to_wtr_static)
        app.router.add_static('/wtr-static', f'{topdir}/tests/wtr-static', show_index=True, follow_symlinks=True)
        app.router.add_static('/olfactory', f'{topdir}/lib/js/olfactory', show_index=True, follow_symlinks=True)
        runner = aiohttp.web.AppRunner(app)
        await runner.setup()
        site = aiohttp.web.TCPSite(runner, HTTPD_ADDR, HTTPD_PORT)
        await site.start()
        self._hrunner = runner

    async def cleanup(self):
        runner = self._hrunner
        arsnc = self._asession
        if runner is not None:
            await runner.cleanup()
            self._hrunner = None
        self._happ = None
        if arsnc is not None:
            await arsenic.stop_session(arsnc)
            self._asession = None

    def addStringRoute(self, path, text, *, content_type=None):
        ct, enc = mimetypes.guess_type(path)
        if content_type==None:
            content_type=ct
        def _(request):
            r = aiohttp.web.Response(text=text, content_type=content_type)
            return r
        app = self._happ
        app.router.add_get(path, _)

    async def setText(self,text):
        text = textwrap.dedent(text)
        self.addStringRoute('/test.js', text)

    async def executeScript(self,text):
        arsnc = self._asession
        rval = await arsnc.execute_script(text)
        return rval

    async def execute(self,*,timeout=1):
        await self.startArsenic()
        await self.startHTTPD()
        if _options.pause:
            logger.info('pause web test runner')
            while True:
                await asyncio.sleep(1)
        arsnc = self._asession
        await arsnc.get('/wtr-static/main.html')
        await arsnc.execute_script('wtrmain.start();')
        await arsnc.wait_for_element(timeout, '#test_done')
        rval = await arsnc.execute_script('return wtrmain.get_info();')
        if '_exc' in rval:
            client.translate_error(rval)
        elif '_result' in rval:
            return rval['_result']
        else:
            return None

    async def testDiv(self,text,*,timeout=2):
        a = random.randint(2,50)
        b = random.randint(2,150)
        a = a*b
        conf = config.Config()
        text = text.format(a=a,b=b,url=conf.getItem('test_ocp_connect_url'))
        await self.setText(text)
        c = await self.execute(timeout=timeout)
        expected_c = a/b
        print(f'a={a} b={b} c={c}')
        if c != expected_c:
            raise AssertionError(f'unexpected value: got {c} should be {expected_c}')
        return c


__all__ = [
    'WebTestRunnerFixture',
    'HTTPD_BASEURL', 
    'HTTPD_ADDR', 
    'HTTPD_PORT'
]
