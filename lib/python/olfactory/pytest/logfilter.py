import pytest
import asyncio
import logging
from olfactory import loghlp


logger = logging.getLogger(__name__)


LINE = '=' * 80


def pytest_addoption(parser):
    opts = loghlp.Options()
    loghlp.tune_argparser(parser)


_done = False

def pytest_fixture_setup(fixturedef, request):
    global _done
    if _done:
        return
    opts = loghlp.Options()
    if opts.getStreamname()=='unset':
        opts.setStreamname('stderr')
    if opts.altered_by_argparser:
        loghlp.install_from_ns(request.config.option)
    _done = True


def pytest_runtest_makereport(item, call):
    if call.when=='setup':
        msg = '* begin of test call phase'
    elif call.when=='call':
        msg = '* end of test call phase'
    else:
        return
    logger.debug('')
    logger.debug(LINE)
    logger.debug(msg)
    logger.debug(LINE)
