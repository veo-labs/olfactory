import string
import random
from olfactory.pytest.dec import *
from olfactory import excdef, config


async def _adec_auth_handler(socket,authparams):
    force=authparams.get('force',None)
    username=authparams.get('username',None)
    password=authparams.get('password',None)
    msg=authparams.get('msg','<default message>')
    retpub=authparams.get('retpub', False)
    retpriv=authparams.get('retpriv',False)
    rval = {}
    if retpub:
        rval['public']='public'
    if retpriv:
        rval['_private']='private'
    if force=='ierr':
        raise AssertionError(msg)
    elif force=='aerr':
        raise excdef.AuthError(msg)
    elif force=='retmsg':
        return msg
    elif force=='retnone':
        return None
    elif force=='retnum':
        return int(msg)
    elif username=='groz' and password=='secret':
        return rval
    else:
        raise AssertionError()


adec = DecFixture('adec')
@adec.plg_trigger
def _adec_init(server:'init'):
    server.setAuthHandler(_adec_auth_handler)


async def _pdec_auth_handler(socket,authparams):
    return authparams


def _pdec_perm_handler(socket,frmtype,label,name,authinfo):
    force=authinfo.get('force',None)
    msg=authinfo.get('msg', '<default message>')
    evalstr=authinfo.get('evalstr',None)
    if force=='ierr':
        raise AssertionError(msg)
    elif force=='rettrue':
        return True
    elif evalstr is not None:
        rval = eval(evalstr)
        return rval
    raise AssertionError()


pdec = DecFixture('pdec')
@pdec.plg_trigger
def _pdec_init(server:'init'):
    server.setAuthHandler(_pdec_auth_handler)
    server.setPermHandler(_pdec_perm_handler)


def generate_utf8_strings(*,num,size):
    chars = ''
    chars += string.ascii_letters
    chars += string.punctuation
    chars += string.digits
    chars += '你好' # chinese translation of 'hello'
    chars += 'Привет' # russian translation of 'hello'
    l = []
    for j in range(num):
        s = ''
        for i in range(size):
            idx = random.randint(0,len(chars)-1)
            s += chars[idx]
        l.append(s)
    return l


__all__ = ['adec','pdec','generate_utf8_strings']
