from olfactory import plugin
from olfactory.pytest import base


IMPORT_ALL_DONE = False


class PluginManagerFixture(base.Fixture):
    async def _up(self):
        global IMPORT_ALL_DONE
        plgmng = plugin.Manager()
        if not IMPORT_ALL_DONE:
            plgmng.importAll()
            IMPORT_ALL_DONE = True
        return plgmng

    async def _down(self,plgmng):
        await plgmng.ensureTriggersExecutionFinished()
        plgmng.clearProvisions()


__all__ = ['PluginManagerFixture']
