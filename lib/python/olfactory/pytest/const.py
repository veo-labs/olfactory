from olfactory.pytest import base


class Container:
    def _tbook_encode(self):
        return self.__dict__

class ConstantsFixture(base.Fixture):
    SUB_FIXTURES='request'

    async def _up(self):
        request = self._getSubFixtureValue('request')
        consts = Container()
        mod = request.node.module
        for mrk in request.node.own_markers:
            if mrk.name=='def_constants':
                for k,v in mrk.kwargs.items():
                    setattr(consts,k,v)
            elif mrk.name=='use_constants':
                for k in mrk.args:
                    v = getattr(mod,k)
                    setattr(consts,k,v)
        return consts


__all__ = [
    'ConstantsFixture'
]