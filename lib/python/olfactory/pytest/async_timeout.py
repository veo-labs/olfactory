import asyncio
import pytest
from olfactory.stamp import *


_TIMEOUT = None
_CANCEL_PERIOD = 0.3


_reporter = None


def pytest_runtestloop(session):
    global _reporter
    _reporter = session.config.pluginmanager.getplugin('terminalreporter')


def pytest_addoption(parser):
    parser.addoption('--async-timeout', type=float)
    parser.addini('async_timeout', 'async timeout')


def pytest_configure(config):
    global _TIMEOUT
    _TIMEOUT = _get_timeout(config)


def _get_timeout(config):
    timeout = config.getvalue('async_timeout')
    if timeout is not None:
        return timeout
    timeout = config.getini('async_timeout')
    try:
        timeout=float(timeout)
    except ValueError:
        timeout=-1
    return timeout


def pytest_runtest_setup(item):
    set_stamp(item,'phase','setup')


def pytest_runtest_call(item):
    set_stamp(item,'phase','call')


def pytest_runtest_teardown(item, nextitem):
    set_stamp(item,'phase','teardown')


def _cancel_setup_and_call_phases(item,event_loop):
    ph = get_stamp(item,'phase','setup')
    for tsk in asyncio.Task.all_tasks():
        tsk.cancel()
    event_loop.call_later(_CANCEL_PERIOD, _cancel_setup_and_call_phases, item, event_loop)


def _on_timeout(request,event_loop,timeout):
    item = request.node
    ph = get_stamp(item,'phase','setup')
    r = _reporter
    tbstyle = r.config.option.tbstyle
    if (tbstyle != "no") and (tbstyle != "line"):
        r.write_line('')
        r.write_line(f'~~~~~~~~~~ Timeout ({timeout:.1f}s) occured in {item.name} during {ph} phase ~~~~~~~~~~')
        r.write_line('')
    _cancel_setup_and_call_phases(item,event_loop)


@pytest.fixture
async def async_timeout(request,event_loop):
    item = request.node
    mrk = item.get_closest_marker('async_timeout')
    if mrk is not None:
        timeout = mrk.args[0]
    else:
        timeout = _TIMEOUT
    if timeout is None:
        pass
    elif not (isinstance(timeout,int) or isinstance(timeout,float)):
        raise AssertionError('timeout must be a number')
    elif timeout <= 0:
        raise AssertionError('timeout must be a positive number')
    else:
        event_loop.call_later(timeout, _on_timeout, request, event_loop, timeout)


def pytest_runtest_setup(item):
    mrk = item.get_closest_marker('asyncio')
    if mrk is None:
        return
    method = mrk.kwargs.get('method', None)
    item.fixturenames.insert(0,'async_timeout')
