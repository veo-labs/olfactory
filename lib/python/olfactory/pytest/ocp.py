import asyncio
import logging
import sys
from olfactory.ocp import server, client
from olfactory.pytest import base
from olfactory import config, loghlp, excdef


logger = logging.getLogger(__name__)


class OCPFixture(base.Fixture):
    def __init__(self, names, *, url=None, **kwargs):
        super().__init__(names, **kwargs)
        if url is None:
            self._url = self._initDefaultURL()
        else:
            self._url = url
        self._kwargs = kwargs


class OCPServerFixture(OCPFixture):
    SUB_FIXTURES='plgmng'

    def __init__(self, names, *, close_on_teardown=True, url=None, **kwargs):
        super().__init__(names, url=url, **kwargs)
        self._close_on_teardown = close_on_teardown

    def _initDefaultURL(self):
        conf = config.Config()
        return conf.getItem('test_ocp_listen_url')

    async def _up(self):
        loop = asyncio.get_event_loop()
        kwargs = self._kwargs
        srv = server.Server(self._url, loop=loop, identity=self._names[0], **kwargs)
        await srv.listen()
        return srv

    async def _down(self,srv):
        if self._close_on_teardown:
            await srv.close()


class OCPClientFixture(OCPFixture):
    def _initDefaultURL(self):
        conf = config.Config()
        return conf.getItem('test_ocp_connect_url')

    async def _up(self):
        loop = asyncio.get_event_loop()
        kwargs = self._kwargs
        cl = client.Client(self._url, loop=loop, identity=self._names[0], **kwargs)
        await cl.connect()
        return cl

    async def _down(self,cl):
        try:
            await cl.disconnect()
        except excdef.StateError:
            pass


class NodeFinalizationError(Exception):
    pass


class OCPNodeFactoryFixture(base.Fixture):
    def __init__(self, names, **kwargs):
        super().__init__(names, **kwargs)
        self._nodes = set()
        self._loop = None

    async def _up(self):
        self._loop = asyncio.get_event_loop()
        return self

    def instanciateClient(self,*,url=None,username=None,password=None,**kwargs):
        conf = config.Config()
        if url is None:
            url = conf.getItem('test_ocp_connect_url')
        if username is not None  or  password is not None:
            idx = url.find('://')
            if idx < 0:
                raise AssertionError
            url_extra = ''
            if username is not None:
                url_extra += username
            if password is not None:
                url_extra += ':' + password
            url = url[:idx+3] + url_extra + '@' + url[idx+3:]
        cl = client.Client(url, loop=self._loop, **kwargs)
        self.own(cl)
        return cl

    def instanciateServer(self,*,url=None,**kwargs):
        conf = config.Config()
        if url is None:
            url = conf.getItem('test_ocp_listen_url')
        srv = server.Server(url, loop=self._loop, **kwargs)
        self.own(srv)
        return srv

    def own(self,*nodes):
        for n in nodes:
            self._nodes.add(n)

    async def _finalizeNode(self,node):
        if isinstance(node,client.Client):
            await node.disconnect()
        elif isinstance(node,server.Server):
            await node.close()
        else:
            raise NodeFinalizationError(f'unexpected node type {n!r}')

    async def finalizeAllNodes(self,*,clear_nodes=False):
        nodes = list(self._nodes)
        if clear_nodes:
            self._nodes.clear()
        error = None
        for n in nodes:
            try:
                await self._finalizeNode(n)
            except excdef.StateError:
                continue
            except:
                triplet = sys.exc_info()
                loghlp.log_exception(logger, title='_finalizeNode() failed')
                et,ev,tb = triplet
                if error is not None:
                    error = ev
        if error is not None:
            raise error

    async def _down(self,obj):
        await self.finalizeAllNodes(clear_nodes=True)


__all__ = [
    'OCPFixture', 
    'OCPClientFixture',
    'OCPServerFixture',
    'OCPNodeFactoryFixture'
]
