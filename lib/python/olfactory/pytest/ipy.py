import asyncio
from olfactory.pytest import base


class IPythonCmdError(Exception):
    pass


class IPythonFixture(base.Fixture):
    SUB_FIXTURES='cl'

    async def _up(self):
        cl = self._getSubFixtureValue('cl')
        args = ['ipython', '--simple-prompt']
        kwargs = {}
        kwargs['class'] = 'proc'
        kwargs['jobexts'] = ['repl','sh']
        kwargs['name'] = 'ipython'
        iref = await cl.xcall('job::run', args=args,kwargs=kwargs)
        await cl.callm(iref,'trackp', r'^(In\s\[\d+\]:\s)$')
        await cl.callm(iref,'syncp')
        self._iref = iref
        return self

    async def cmd(self, s):
        cl = self._getSubFixtureValue('cl')
        iref = self._iref
        x = await cl.callm(iref,'cmd', s)
        if 'Traceback' in x.out:
            raise IPythonCmdError(f'{s!r} failed')
        return x

    async def _down(self,ipy):
        iref = self._iref
        cl = self._getSubFixtureValue('cl')
        await cl.callm(iref, 'tx', b'\x04')
        await asyncio.sleep(0.1)
        await cl.callm(iref, 'tx', b'\r')
        await asyncio.sleep(0.1)


__all__ = [
    'IPythonCmdError',
    'IPythonFixture',
]
