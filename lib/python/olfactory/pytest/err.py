from olfactory import plugin
from olfactory.pytest import base


class raises:
    def __init__(self,klass,*, msg_contains=None,olfactory_fields=None):
        self.klass = klass
        self.msg_contains = msg_contains
        self.olfactory_fields = olfactory_fields

    def __enter__(self):
        pass

    def __exit__(self,*triplet):
        self.triplet = triplet
        if (triplet[0] is None) or (triplet[0] != self.klass):
            raise AssertionError(f'{self.klass.__name__!r} not raised')
        if self.msg_contains is not None:
            msg = str(triplet[1])
            if self.msg_contains not in str(triplet[1]):
                raise AssertionError(f'Bad exception message {msg!r}')
        if self.olfactory_fields is not None:
            if not hasattr(triplet[1],'olfactory_fields'):
                raise AssertionError(f"Exception has no attribute olfactory_fields")
            olfactory_fields = triplet[1].olfactory_fields
            for k,v0 in self.olfactory_fields.items():
                if k not in olfactory_fields:
                    raise AssertionError(f"olfactory_fields has no key {k!r}")
                v1 = olfactory_fields[k]
                if v1 != v0:
                    raise AssertionError(f"olfactory_fields[{k!r}] differs (found: {v1!r} expected: {v0!r})")
        if triplet[0]==self.klass:
            return True


__all__ = ['raises']
