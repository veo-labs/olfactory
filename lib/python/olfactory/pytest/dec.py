from olfactory import decorators
from olfactory.pytest import base


class DecFixture(base.Fixture):
    def __init__(self, *names, inner=False):
        super().__init__(*names)
        for k,v in decorators.DEFINITIONS.items():
            setattr(self,k,decorators.Decorator(k,v,decorate=self.decorate))
        self._keys = []
        self._inner = inner
        self._up_decorations = []

    def _decorate(self,name,item,**kwargs):
        m = decorators.Manager()
        k = m.decorate(name,item,**kwargs)
        self._keys.append(k)

    def decorate(self,name,item,**kwargs):
        if self._inner:
            self._decorate(name,item,**kwargs)
        else:
            self._up_decorations += [(name,item,kwargs)]

    async def _up(self):
        for name,item,kwargs in self._up_decorations:
            self._decorate(name,item,**kwargs)
        return self

    async def _down(self,dec):
        m = decorators.Manager()
        for key in self._keys:
            m.removeDecoration(key)
        self._keys.clear()


__all__ = ['DecFixture']
