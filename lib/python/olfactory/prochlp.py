import asyncio
import logging
import copy
from olfactory import loghlp


OUTPUT_BUFFER_MAX_SIZE = 2*1024*1024  # 2M
BLOCKSIZE = 200


class LineLogger:
    def __init__(self,logger,blocksize):
        self._buf = ''
        self._logger = logger
        self._blocksize = blocksize

    def add(self, bufp):
        buf = self._buf + bufp.decode()
        ln = ''
        for ch in buf:
            ln += ch
            b = False
            if len(ln) > 120:
                b = True
            elif ch in ('\n','\r'):
                b = True
            if b:
                self.log(ln)
                ln = ''
        self._buf = ln

    def log(self,ln):
        if ln=='':
            return
        ln = repr(ln)
        ln = ln[1:-1]
        self._logger.debug(f'  <  {ln}')

    def flush(self):
        self.log(self._buf)
        self._buf = ''


class ProcessRunner:
    def __init__(self, cmd, identity=None, loop=None, shell=None, logger=None, output_buffer_max_size=OUTPUT_BUFFER_MAX_SIZE,
            auto_relaunch=None, blocksize=BLOCKSIZE, **kwargs):
        if loop is None:
            loop = asyncio.get_event_loop()
        self._loop = loop
        self._cmd = cmd
        if shell is None:
            if isinstance(cmd,str):
                shell=True
            elif isinstance(cmd,list):
                shell=False
        self._shell = shell
        self._proc = None
        self._output = b''
        self._auto_relaunch = auto_relaunch
        self._bg_task = None
        self._blocksize = blocksize
        if logger is None:
            logger = logging.getLogger(__name__)
        if identity is not None:
            self._logger = loghlp.get_sub_logger(logger,identity)
        else:
            self._logger = logger
        self._identity = identity
        self._kwargs = kwargs
        self._truncating_reported = False
        self._output_buffer_max_size = output_buffer_max_size

    def __repr__(self):
        s = f'<{self.__class__.__name__} running={self.running}>'
        return s

    async def _backgroundTask(self):
        self._truncating_reported = False
        ln_logger = LineLogger(self._logger,self._blocksize)
        while True:
            stdout = self._proc.stdout
            buf = await stdout.read(self._blocksize)
            if len(buf)==0:
                self._logger.info(f'  x  EOF reached')
                ln_logger.flush()
                if self._auto_relaunch is not None:
                    self._logger.info(f'  x  auto relaunch process in {self._auto_relaunch}s')
                    await asyncio.sleep(self._auto_relaunch)
                    await self.launch(_bg_task=False)
                    continue
                else:
                    break
            ln_logger.add(buf)
            self._output += buf
            if not(self._truncating_reported) and (len(self._output) > self._output_buffer_max_size):
                self._logger.info(f'  x  max size ({self._output_buffer_max_size}) for output buffer reached so truncating from now')
                self._truncating_reported = True
            self._output = self._output[-self._output_buffer_max_size:]

    def _backgroundTaskDone(self,tsk):
        self._bg_task = None
        if not tsk.cancelled:
            return
        self.kill()

    async def launch(self,*,_bg_task=True):
        if self.running:
            raise AssertionError('already running')
        shell = self._shell
        loop = self._loop
        cmd = self._cmd
        if self._logger:
            if isinstance(cmd,list):
                self._logger.info(f'  x  launch process {" ".join(cmd)!r}')
            else:
                self._logger.info(f'  x  launch process {cmd!r}')
        self._output = b''
        kwargs = copy.copy(self._kwargs)
        kwargs['stdin'] = asyncio.subprocess.PIPE
        kwargs['stdout'] = asyncio.subprocess.PIPE
        kwargs['stderr'] = asyncio.subprocess.STDOUT
        kwargs['loop'] = loop
        if shell==True:
            f = asyncio.create_subprocess_shell
            args = cmd,
        elif shell==False:
            args = cmd
            f = asyncio.create_subprocess_exec
        else:
            raise AssertionError
        proc = await f(*args,**kwargs)
        self._logger.info(f'  x    pid: {proc.pid}')
        self._proc = proc
        if _bg_task:
            tsk = loop.create_task(self._backgroundTask())
            tsk.add_done_callback(self._backgroundTaskDone)
            self._bg_task = tsk
        return proc

    async def wait(self):
        returncode = await self._proc.wait()
        self._logger.info(f'  x  exited with return code {returncode}')
        return returncode

    async def run(self):
        await self.launch()
        return await self.wait()

    def sendSignal(self, signal):
        self._proc.send_signal(signal)

    def disableAutoRelaunch(self):
        self._auto_relaunch = None

    def terminate(self):
        try:
            self._proc.terminate()
        except ProcessLookupError:
            pass

    def kill(self):
        try:
            self._proc.kill()
        except ProcessLookupError:
            pass

    def cancelBackgroundTask(self):
        self._bg_task.cancel()

    @property
    def stdin(self):
        return self._proc.stdin

    @property
    def output(self):
        return self._output

    @property
    def returncode(self):
        return self._proc.returncode

    @property
    def running(self):
        if self._proc is not None:
            return self._proc.returncode is None
        else:
            return False
