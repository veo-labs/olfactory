def strip_lines(input_str, *, num_spaces=8):
    output_str = ''
    spaces = ' ' * num_spaces
    lineno = 1
    for ln in input_str.split('\n'):
        if ln=='':
            continue
        elif ln.startswith(spaces):
            output_str += ln[num_spaces:] + '\n'
        else:
            raise AssertionError('invalid line #%d: %r' % (lineno, ln))
        lineno += 1
    return output_str
