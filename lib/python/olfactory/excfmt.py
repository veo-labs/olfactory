import sys
import traceback


def install_excepthook():
    def _(et,ev,tb):
        f = sys.stderr
        traceback.print_tb(tb,file=f)
        if hasattr(ev,'olfactory_fields'):
            f.write(f"  Field 'olfactory_fields'\n")
            for k,v in ev.olfactory_fields.items():
                f.write(f'    {k} = {v!r}\n')
        traceback.print_exception(et,ev,file=f,tb=None)
    sys.excepthook = _


def format_multi(*, exc=None, triplet=None, with_tb=True, remove_eol=True):
    if triplet is None and exc is None:
        et, ev, tb = sys.exc_info()
    elif exc is not None:
        et, ev, tb = exc.__class__, exc, None
        with_tb=False
    else:
        et, ev, tb = triplet
    if with_tb:
        lines = traceback.format_exception(et, ev, tb)
    else:
        lines = traceback.format_exception_only(et, ev)
    if not remove_eol:
        return lines
    lines_out = []
    for ln in lines:
        lines_out += ln.split('\n')
    lines = lines_out
    lines_out = []
    for ln in lines:
        if ln=='':
            continue
        lines_out.append(ln)
    return lines_out


def format_single(*, exc=None, triplet=None):
    lines = format_multi(exc=exc, triplet=triplet, with_tb=False, remove_eol=True)
    if len(lines) == 0:
        return None
    else:
        return lines[-1]


def split(*,exc=None,triplet=None):
    line = format_single(exc=exc, triplet=triplet)
    if line==None:
        return None, None
    else:
        idx = line.find(':')
        if idx >= 1:
            name = line[:idx]
            msg = line[idx+2:]
        else:
            name = line
            msg = None
        if msg != None:
            for ch in '"',"'":
                if msg[0]==ch and msg[-1]==ch and msg.count(ch)==2:
                    msg=msg[1:-1]
                    break
        return name,msg
