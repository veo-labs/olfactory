import sys
import logging
import asyncio
import collections
import time
from olfactory import decorators, identifier, excdef, event, loghlp, excfmt
from olfactory.ocp import client
from olfactory.decorators import *


logger = logging.getLogger(__name__)


class Extension:
    def __init__(self, job, *args, **kwargs):
        self._manager = job.getManager()
        self._job = job

    def getJob(self):
        return self._job

    def getManager(self):
        return self._job.getManager()

    async def _starting(self):
        pass

    async def _stopping(self):
        pass


class Job(event.EventHandler):
    def __init__(self, manager, *args, loop, auto=None, autodelay=1, use_idmng, **kwargs):
        event.EventHandler.__init__(self,loop=loop)
        self._manager = manager
        self._name = kwargs['name']
        self._auto = auto
        self._autodelay = autodelay
        self._autorestart_handle = None
        self._status = 'null'
        self._loop = loop
        self._jobexts = collections.OrderedDict()
        s = self.getServer()
        s.setMasterLabel(self, f'job::{self._name}')
        self._initExtensions(loop=loop,*args,**kwargs)
        self._triplet = None
        self._wait_sts_futures = {}
        self._prologue = self._addPrologue('status-changed', self.__presentStatus)
        self.__logger = loghlp.get_sub_logger(logger,self._name)
        self._use_idmng = use_idmng
        self._initializeEventProcessing()
        self._start_time = None

    def enableAuto(self,auto,delay=1):
        self._auto = auto
        self._autodelay = delay

    def disableAuto(self):
        self._auto = None
        if self._autorestart_handle:
            self._autorestart_handle.cancel()
        self._autorestart_handle = None

    @decorators.ocp_method
    def destroy(self):
        self._requireStatus('stopped')
        self._setStatus('null')
        self._finalizeEventProcessing()
        self._manager._jobs.remove(self)
        self._removePrologue(self._prologue)

    def getManager(self):
        return self._manager

    def getServer(self):
        s = self._manager.getServer()
        return s

    def getMasterLabel(self):
        s = self.getServer()
        return s._getMasterLabel(self)

    def putLabel(self,label):
        s = self.getServer()
        s.putLabel(self,label)

    def getExtension(self,name):
        return self._jobexts[name]

    def _initExtensions(self, *args, **kwargs):
        jobexts = kwargs.get('jobexts',[])
        m = self._manager
        for name in jobexts:
            ext = m._instanciateExtension(name, self, *args, **kwargs)
            self._addHousing(ext)
            self._jobexts[name] = ext
            dm = decorators.Manager()
            for dec in dm.getDecorations('jobext_method', parent=ext.__class__):
                uf = dec.getItem()
                n = uf.__name__
                if hasattr(self,n):
                    raise AssertionError(f'Job {self._name} has already an attribute called {n}')
                bf = getattr(ext,n)
                setattr(self,n,bf)
                dm.decorate('ocp_method', uf)

    def _publishSoon(self, name, *args, **kwargs):
        s = self._manager.getServer()
        label = self.getMasterLabel()
        topic = f"{label}<{name}>"
        s.publishSoon(topic, *args, **kwargs)

    def _addPrologue(self, name, callable, **extra_kwargs):
        s = self._manager.getServer()
        label = self.getMasterLabel()
        topic = f"{label}<{name}>"
        return s.addPrologue(topic,callable,**extra_kwargs)

    def _removePrologue(self, pro):
        s = self._manager.getServer()
        s.removePrologue(pro)

    def _publishSoonOnSocket(self, name, socket, *args, **kwargs):
        s = self._manager.getServer()
        label = self.getMasterLabel()
        topic = f"{label}<{name}>"
        s.publishSoonOnSocket(topic, socket, *args, **kwargs)

    async def _publishOnSocket(self, name, socket, *args, **kwargs):
        s = self._manager.getServer()
        label = self.getMasterLabel()
        topic = f"{label}<{name}>"
        await s.publishOnSocket(topic, socket, *args, **kwargs)

    def _setStatus(self, new_status):
        s = self._manager.getServer()
        old_status = self._status
        if old_status == new_status:
            return
        self._status = new_status
        self.__logger.debug(f'{old_status} >>> {new_status}')
        self._publishSoon('status-changed', self.getMasterLabel(), old_status, new_status)
        for fut, tupl in list(self._wait_sts_futures.items()):
            old,new = tupl
            if fut.done():
                del self._wait_sts_futures[fut]
                continue
            if (old==None or old==old_status)  and  new==new_status:
                del self._wait_sts_futures[fut]
                fut.set_result(None)
        self._postEvent('STATUS', old_status, new_status)

    def getStatus(self):
        return self._status

    async def __presentStatus(self,socket):
        s = self._manager.getServer()
        await self._publishOnSocket('status-changed', socket, self.getMasterLabel(), 'unknown', self._status)

    @decorators.ocp_method
    def waitStatus(self, old_status, new_status=None, *, check_current=True):
        if new_status is None:
            new_status = old_status
            old_status = None
        fut = asyncio.Future()
        if check_current and (old_status is None):
            if new_status==self._status:
                fut.set_result(None)
                return fut
        self._wait_sts_futures[fut] = old_status, new_status
        return fut

    def _requireStatus(self, *args):
        if self._status in args:
            return
        raise excdef.StateError(f"Unexpected status {self._status!r}")

    def getName(self):
        return self._name

    def _setTriplet(self, *args):
        if self._triplet is not None:
            return
        if len(args)==3:
            self._triplet = args
        elif len(args)==1:
            a0 = args[0]
            if a0 is None:
                self._triplet = None
            elif isinstance(a0, BaseException):
                ev = a0
            else:
                ev = AssertionError(a0)
            et = ev.__class__
            self._triplet = et, ev, None

    def _resetTriplet(self):
        self._triplet = None

    def _getTriplet(self):
        return self._triplet

    def _checkTriplet(self):
        if self._triplet is None:
            return
        exc = self._triplet[1]
        self._resetTriplet()
        raise exc

    @decorators.ocp_method
    def getInfo(self):
        d = {}
        d['status'] = self._status
        d['master_label'] = self.getMasterLabel()
        d['name'] = self._name
        if self._triplet is not None:
            name,msg = excfmt.split(triplet=self._triplet)
            d['last_error_name'] = name
            d['last_error_msg'] = msg
        xd = self._getExtraInfo()
        d.update(xd)
        return d

    def _getExtraInfo(self):
        return {}

    @decorators.ocp_method
    async def start(self):
        await self._doStart(throw=True)

    async def _doStart(self,*,throw):
        self._start_time = time.monotonic()
        self._resetTriplet()
        self._requireStatus('stopped')
        self._setStatus('starting')
        try:
            for ext in self._jobexts.values():
                await ext._starting()
            await self._starting()
            self._setStatus('started')
        except:
            self._setStatus('stopped')
            triplet = sys.exc_info()
            loghlp.log_exception(self.__logger, title='Job starting failed')
            self._setTriplet(triplet)
            self._manageAuto()
            if throw:
                raise

    def _manageAuto(self):
        if self._auto=='restart':
            dt = time.monotonic() - self._start_time
            if dt < self._autodelay:
                delay = self._autodelay - dt
                self.__logger.debug(f'wait {delay:.01f}s before restarting')
                self._autorestart_handle = self._postEventLater(delay,'WANTSTART')
            else:
                self._postEvent('WANTSTART')
        elif self._auto=='destroy':
            self.destroy()

    async def _starting(self):
        pass

    async def _eventProcessingDone(self,triplet):
        s = self.getServer()
        m = self.getManager()
        s.resetMasterLabel(self)
        if self._use_idmng:
            m._idmng.release(self._name)
        del self._jobexts

    @decorators.ocp_method
    def terminate(self):
        self._requireStatus('started')
        self._postEvent('WANTSTOP')

    @decorators.ocp_method
    async def stop(self):
        self.terminate()
        await self.waitStatus('stopped')
        self._checkTriplet()

    async def _stopping(self):
        pass

    async def _ev_WANTSTOP(self):
        if self._status!='started':
            return
        self._setStatus('stopping')
        try:
            for ext in self._jobexts.values():
                await ext._stopping()
            await self._stopping()
            self._setStatus('stopped')
        except:
            self._setStatus('stopped')
            triplet = sys.exc_info()
            loghlp.log_exception(self.__logger, title='Job stopping failed')
            self._setTriplet(triplet)
            raise
        finally:
            self._manageAuto()

    async def _ev_WANTSTART(self):
        self._autorestart_handle = None
        await self._doStart(throw=False)


class Manager:
    def __init__(self, server):
        self._server = server
        self._loop = server.getLoop()
        self._idmng = identifier.IDManager()
        self._job_classes = {}
        self._ext_classes = {}
        self._exposures = None
        self._initExposures()
        self._initJobClasses()
        self._initExtensionClasses()
        self._jobs = set()

    def getServer(self):
        return self._server

    def _initExposures(self):
        s = self._server
        l = []
        l += [ s.expose('job::create',  self.createJob) ]
        l += [ s.expose('job::run',     self.runJob) ]
        l += [ s.expose('job::get',     self.getJob) ]
        l += [ s.expose('job::get_all', self.getAllJobs) ]
        l += [ s.expose('job::get_all_names', self.getAllNames) ]
        l += [ s.expose('job::get_all_status', self.getAllStatus) ]
        self._exposures = l

    def _initJobClasses(self):
        m = decorators.Manager()
        for dec in m.getDecorations('job_class'):
            klass = dec.getItem()
            classname = dec.getArg0()
            self._job_classes[classname] = klass
            m.decorate('ocp_class', klass)

    def getAllNames(self):
        s = self._server
        names = []
        for inst in s._getAllInstances():
            if isinstance(inst,Job):
                n = inst.getName()
                names.append(n)
        return names

    def getAllStatus(self):
        s = self._server
        d = {}
        for inst in s._getAllInstances():
            if isinstance(inst,Job):
                n = inst.getName()
                d[n] = inst.getStatus()
        return d

    def _initExtensionClasses(self):
        m = decorators.Manager()
        for dec in m.getDecorations('jobext_class'):
            klass = dec.getItem()
            classname = dec.getArg0()
            self._ext_classes[classname] = klass

    def createJob(self, *args, **kwargs):
        if 'class' in kwargs:
            classname = kwargs['class']
        elif 'klass' in kwargs:
            classname = kwargs['klass']
        else:
            raise AssertionError('job class name not specified')
        name = classname + '{}'
        if 'name' in kwargs:
            name = kwargs.pop('name')
        use_idmng = kwargs.pop('use_idmng', True)
        if use_idmng:
            name = self._idmng.acquire(name)
        kwargs['name'] = name
        if classname not in self._job_classes:
            raise excdef.NameError("No job class registered with name '{}'".format(classname))
        klass = self._job_classes[classname]
        job = klass(self, *args, loop=self._loop, use_idmng=use_idmng,**kwargs)
        job._setStatus('stopped')
        label = f"job::{name}"
        self._jobs.add(job)
        return job

    def _instanciateExtension(self, *args, **kwargs):
        name, job, *args = args
        klass = self._ext_classes[name]
        ext = klass(job, *args, **kwargs)
        return ext

    def getJob(self, name):
        if name==None:
            raise excdef.NameError('name is None')
        label = f"job::{name}"
        s = self._server
        job = s._getInstanceFromLabel(label)
        return job

    def getAllJobs(self,*,include_null=True):
        s = self._server
        jobs = []
        for inst in s._getAllInstances():
            if isinstance(inst,Job):
                if (not include_null) and (inst.getStatus()=='null'):
                    continue
                jobs.append(inst)
        return jobs

    def countJobWithStatus(self, status):
        count = 0
        for j in self.getAllJobs():
            if j.getStatus()==status:
                count += 1
        return count

    def getNumberOfJobs(self,*,include_null=True):
        jobs = self.getAllJobs(include_null=include_null)
        return len(jobs)

    async def runJob(self, *args, **kwargs):
        job = self.createJob(*args, **kwargs)
        await job.start()
        return job


@ct_action('job.create')
async def _(*,cl,ns,ctapp,**ignored):
    """Create a job using ARGS, KWARGS and JOBEXTS"""
    return await ctapp.doBasicAction(cl, ns, funcname='job::create', args=True, kwargs=True)


@ct_action('job.run')
async def _(*,cl,ns,ctapp,**ignored):
    """Create a job using ARGS and KWARGS and start it"""
    return await ctapp.doBasicAction(cl, ns, funcname='job::run', args=True, kwargs=True)


@ct_action('job.destroy')
async def _(*,cl,ns,ctapp,**ignored):
    """Destroy the job named JOBNAME"""
    return await ctapp.doBasicAction(cl, ns, funcname='destroy',iref='required')


@ct_action('job.start')
async def _(*,cl,ns,ctapp,**ignored):
    """Start the job named JOBNAME"""
    return await ctapp.doBasicAction(cl, ns, funcname='start',iref='required')


@ct_action('job.stop')
async def _(*,cl,ns,ctapp,**ignored):
    """Stop the job named JOBNAME"""
    return await ctapp.doBasicAction(cl, ns, funcname='stop',iref='required')


@ct_action('job.terminate')
async def _(*,cl,ns,ctapp,**ignored):
    """Terminate the job named JOBNAME"""
    return await ctapp.doBasicAction(cl, ns, funcname='terminate',iref='required')


@ct_action('job.info')
async def _(*,cl,ns,ctapp,**ignored):
    """Dump info from job named JOBNAME"""
    return await ctapp.doBasicAction(cl, ns, funcname='getInfo',iref='required')


@ct_action('job.list')
async def _(*,cl,ns,ctapp,**ignored):
    """List all jobs"""
    return await ctapp.doBasicAction(cl, ns, funcname='job::get_all')
