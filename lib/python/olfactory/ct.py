import sys
import asyncio
import argparse
import copy
import ast
import logging
import os
import re
import time
import functools
import pprint
import inspect
import websockets
import websockets.exceptions
import textwrap

from olfactory import config, loghlp, decorators, plugin, excdef, terminal, excfmt, callhlp
from olfactory.argphlp import *
from olfactory.ocp import client
from olfactory.decorators import *


def main():
    app = Application()
    app()


class ForceApplicationRestart(Exception):
    def __init__(self,extra_env=None):
        self.extra_env = extra_env


class CTActionHelpSection:
    def __init__(self,formatter,parent):
        self.formatter = formatter
        self.parent = parent

    def format_help(self):
        m = decorators.Manager()
        help = {}
        indent_size = -1
        for d in m.getDecorations('ct_action'):
            a0 = d.getArg0()
            doc = d.getItem().__doc__
            if doc is None:
                doc = ''
            help[a0] = doc
            indent_size = max(indent_size,len(a0))
        s = 'available ACTIONs:\n'
        width = self.formatter._width
        for a0 in sorted(help.keys()):
            doc = help[a0]
            fmt = '{:<%d}' % indent_size
            text = '  ' + fmt.format(a0) + '        ' + doc
            subsequent_indent = '  ' + fmt.format('') + '        '
            s += textwrap.fill(text,width=width,subsequent_indent=subsequent_indent)
            s += '\n'
        return s


class HelpFormatter(argparse.HelpFormatter):
    def start_ct_action_section(self):
        self._indent()
        section = CTActionHelpSection(self, self._current_section)
        self._add_item(section.format_help, [])
        self._current_section = section


class ArgumentParser(argparse.ArgumentParser):
    def __init__(self):
        super().__init__(formatter_class=HelpFormatter,
            usage='%(prog)s ACTION [ct options] [logging options] [config options] [-- ARGS]',
            epilog="arguments passed after '--' are as strings in ARGS")
        conf = config.Config()
        targets = {
            'args': [],
            'kwargs': {},
            'authparams': {},
        }
        printer = 'json'
        topics = []
        self._targets = targets
        agrp = self.add_argument_group('ct actions')
        agrp.add_argument(dest="action", default=None, nargs=1, type=str, help=argparse.SUPPRESS)
        ogrp = self.add_argument_group('ct options')
        ogrp.add_argument("-u",                     dest='url',             default=conf.getItem('connect_url'),
            action='store',                   help="server location specified as ws://host:port")
        ogrp.add_argument("-i",                     dest='identity',        default=None,
            action='store',                   help="client identity")
        ogrp.add_argument("-K",                     dest='keep_connected',  default=False,
            action='store_true',              help="reconnect to server when losing connection")
        ogrp.add_argument("-f",                     dest='funcname',        default=None,
            action='store',                   help="name of the exposed function or method")
        ogrp.add_argument("-c", metavar='COUNT',    dest='count',           default=None, type=int,
            action='store',                   help="exit if COUNT reached")
        ogrp.add_argument("-t",                     dest='topics',          default=topics,
            action='append',                  help="topic name (or topic pattern when subscribing)")
        ogrp.add_argument("-T",                     dest='topics',          default=topics,
            action='append_const', const='*',  help="shortcut for -t '*' which means subscribing to all")
        ogrp.add_argument("-l",                     dest='label',           default=None,
            action='store',                   help="label to identify the remote instances")
        ogrp.add_argument("-n",                     dest='jobname',         default=None,
            action='store',                   help="shortcut for -l jobs.JOBNAME")
        ogrp.add_argument("-s",                     dest='section',         default='olfactory',
            action='store',                   help="set config section")
        ogrp.add_argument("--kwa0",                 dest='kwa0',            default=False,
            action='store_true',              help="use KWRGS as first positional argument")
        ogrp.add_argument("--aa0",                 dest='aa0',            default=False,
            action='store_true',              help="use ARGS as first positional argument")
        ogrp.add_argument("-o",  metavar='KWARGS',  dest='_o',              default=MountDashOLikeOpt(targets, 'kwargs', name='-o'),
           action='append',                   help="update KWARGS with the following syntax key1=value1,key2=value2,key3,...")
        ogrp.add_argument("-x",  metavar='JOBEXTS', dest='_x',              default=CSVOpt(targets['kwargs'], 'jobexts', name='-x', ensure=[]),
            action='append',                  help="specify job extensions with the following syntax ext1,ext2,...")
        ogrp.add_argument("--cwd", metavar='KWARGS',dest='_cwd',            default=CWDOpt(targets['kwargs'], 'cwd', name='--cwd'),
            action='append_const', const=None,help="put current working dir in KWARGS['cwd']")
        ogrp.add_argument("-a",  metavar='ARGS',    dest='_a',              default=JSONExpressionOpt(targets, 'args', name='-a'),
            action='append',                  help="update ARGS with a json expression")
        ogrp.add_argument("-p", metavar='KWARGS',   dest='_p',              default=JSONPairOpt(targets, 'kwargs', name='-p'),
            action='append',                  help="update KWARGS with syntax key=<json-expr>")
        ogrp.add_argument("-pL", metavar='KWARGS',   dest='_pL',            default=LabelPairOpt(targets, 'kwargs', name='-pL'),
            action='append',                  help="update KWARGS with syntax key=<label>")
        ogrp.add_argument("-pN", metavar='KWARGS',   dest='_pN',            default=JobPairOpt(targets, 'kwargs', name='-pN'),
            action='append',                  help="update KWARGS with syntax key=<job>")
        ogrp.add_argument("-k", metavar='KWARGS',   dest='_k',              default=JSONDictOpt(targets, 'kwargs', name='-k'),
            action='append',                  help="update KWARGS with json dictionnary")
        ogrp.add_argument("-aS", metavar='ARGS',    dest='_aS',             default=PercentStringOpt(targets, 'args', name='-aS'),
            action='append',                  help="update ARGS with litteral python string")
        ogrp.add_argument("-aB", metavar='ARGS',    dest='_aB',             default=PercentBytesOpt(targets, 'args', name='-aB'),
            action='append',                  help="update ARGS with litteral python bytes")
        ogrp.add_argument("-aL", metavar='ARGS',    dest='_aL',             default=LabelOpt(targets, 'args', name='-aL'),
            action='append',                  help="update ARGS with label")
        ogrp.add_argument("-aN", metavar='ARGS',    dest='_aN',             default=JobOpt(targets, 'args', name='-aN'),
            action='append',                  help="update ARGS with job")
        ogrp.add_argument("-A",  metavar='AUTHPARAMS',  dest='_A',          default=MountDashOLikeOpt(targets, 'authparams', name='-A'),
           action='append',                   help="update AUTHPARAMS with the following syntax key1=value1,key2=value2,key3,...")
        ogrp.add_argument("--raw-printer",          dest='printer', default=printer, action='store_const', const="raw",  help="use raw printer")
        ogrp.add_argument("--json-printer",         dest='printer', default=printer, action='store_const', const="json", help="use json printer")
        ogrp.add_argument("--json-printer-indent", dest='json_printer_indent', default=None, type=int,
           action='store', help="json printer indentation")

    def _get_formatter(self):
        t = terminal.Terminal()
        try:
            width,height = t.getDimension()
        except OSError:
            width = 80
        return self.formatter_class(prog=self.prog,width=width)

    def format_help(self):
        formatter = self._get_formatter()

        # usage
        formatter.add_usage(self.usage, self._actions,
                            self._mutually_exclusive_groups)

        # description
        formatter.add_text(self.description)

        # ct actions section
        formatter.start_ct_action_section()
        formatter.end_section()

        # positionals, optionals and user-defined groups
        for action_group in self._action_groups:
            formatter.start_section(action_group.title)
            formatter.add_text(action_group.description)
            formatter.add_arguments(action_group._group_actions)
            formatter.end_section()

        # epilog
        formatter.add_text(self.epilog)

        # determine help from format above
        return formatter.format_help()

    def parse_argv(self, argv):
        largv = []
        rargv = []
        xargv = largv
        for a in argv[1:]:
            if a=='--':
                rargv = []
                xargv = rargv
                continue
            xargv.append(a)
        ns = super().parse_args(largv)
        ns.action = ns.action[0]
        ns.executable = argv[0]
        ns.authparams = self._targets['authparams']
        self._targets['args'] += rargv
        if ns.kwa0 and ns.aa0:
            raise AssertionError('Cannot mix --aa0 and --kwa0 options')
        elif ns.aa0:
            ns.args = [self._targets['args']]
            ns.kwargs = {}
        elif ns.kwa0:
            ns.args = [self._targets['kwargs']]
            ns.kwargs = {}
        else:
            ns.args = self._targets['args']
            ns.kwargs = self._targets['kwargs']
        return ns


class BaseSetup:
    def __init__(self, app):
        self._app = app
    def up(self,kwargs):
        pass
    def down(self,kwargs):
        pass


class LoopSetup(BaseSetup):
    NAME = 'loop_setup'
    def up(self, kwargs):
        loop = asyncio.get_event_loop()
        kwargs['loop']=loop


class ClientSetup(BaseSetup):
    NAME = 'client_setup'
    async def up(self,kwargs):
        loop = kwargs['loop']
        ns = kwargs['ns']
        if ns.identity is None:
            identity = f"{os.path.basename(ns.executable)}-{os.getpid()}"
            prefix = 'olfactory-'
            if identity.startswith(prefix):
                identity = identity[len(prefix):]
        else:
            identity = ns.identity
        cl = client.Client(ns.url, loop=loop, identity=identity)
        cl.setAuthParams(ns.authparams)
        await cl.connect()
        kwargs['cl'] = cl

    async def down(self,kwargs):
        cl = kwargs['cl']
        await cl.disconnect()


SETUP_CLASSES = [LoopSetup, ClientSetup]


class Application:
    def forceRestart(self,extra_env):
        raise ForceApplicationRestart(extra_env)

    def parseArgs(self):
        plgmng = plugin.Manager()
        p = ArgumentParser()
        loop = asyncio.get_event_loop()
        plgmng.provide(plgmng.Provision('argparser',p,annotation='tune'),loop=loop)
        loop.run_until_complete(plgmng.ensureTriggersExecutionFinished())
        ns = p.parse_argv(sys.argv)
        loghlp.install_from_ns(ns)
        return ns

    def getDecoratorFromAction(self, action):
        m = decorators.Manager()
        for d in m.getDecorations('ct_action'):
            if d.getArg0()==action:
                return d
        raise AssertionError('No such command %r' % action)

    def __call__(self):
        excfmt.install_excepthook()
        m = plugin.Manager()
        m.importAll()
        ns = self.parseArgs()
        if ns.printer=='json':
            self._print = JSONPrinter(ns)
        elif ns.printer=='raw':
            self._print = sys.stdout.write
        else:
            raise AssertionError
        f = functools.partial(self._reexec, ns)
        try:
            self._innerCall(ns)
        except OSError as exc:
            f(exc)
        except excdef.ConnectionClosed as exc:
            f(exc)
        except excdef.StateError as exc:
            f(exc)
        except excdef.AuthError as exc:
            f(exc)
        except excdef.PermError as exc:
            f(exc)
        except asyncio.TimeoutError as exc:
            f(exc)
        except ForceApplicationRestart as exc:
            f(exc)

    def _reexec(self,ns,exc):
        try:
            self._real_reexec(ns,exc)
        except KeyboardInterrupt:
            sys.stdout.write('\n\n')

    def _real_reexec(self, ns, exc):
        conf = config.Config()
        delay = conf.getItem('ct_reexec_delay')
        f = sys.stdout
        t = terminal.Terminal(f)
        em = t.fgcol('#f08080')
        tm = time.strftime('(%H:%M:%S)')
        msg = str(exc)
        cmd = sys.argv
        if isinstance(exc, ForceApplicationRestart):
            for k,v in exc.extra_env.items():
                os.putenv(k,v)
            os.execvp(cmd[0],cmd)
        excn, msg = excfmt.split(exc=exc)
        f.write('\r\n{em:+}{tm} - {excn}: {msg}{em:-}\r\n'.format(em=em,msg=msg,tm=tm,excn=excn))
        if not ns.keep_connected:
            f.write('\r\n')
            return
        f.write('                    {em:+}==> will try to reconnect in {em:-}'.format(em=em))
        f.flush()
        t1 = time.monotonic() + delay
        while True:
            dt = t1 - time.monotonic()
            if dt < 0:
                dt = 0
            f.write('{em:+}{dt:1.2f}s{em:-}'.format(em=em,dt=dt))
            f.flush()
            time.sleep(0.06)
            f.write('\b' * 5)
            if dt==0:
                break
        f.write('\r\n')
        f.flush()
        os.execvp(cmd[0],cmd)

    def _innerCall(self,ns):
        kwargs = {'ns': ns, 'loop': None, 'ctapp': self}
        dec = self.getDecoratorFromAction(ns.action)
        dec_kwargs = dec.getKwargs()
        setup = []
        for klass in SETUP_CLASSES:
            if not dec_kwargs[klass.NAME]:
                continue
            s = klass(self)
            setup.append(s)
        for s in setup:
            loop = kwargs['loop']
            if callhlp.iscoroutinefunction(s.up) and loop:
                loop.run_until_complete(s.up(kwargs))
            else:
                s.up(kwargs)
        cbl = dec.getItem()
        result = None
        try:
            if callhlp.iscoroutinefunction(cbl) and loop:
                result = loop.run_until_complete(cbl(**kwargs))
            else:
                result = cbl(**kwargs)
        except:
            raise
        else:
            if result != None:
                self._print(result)
        finally:
            for s in reversed(setup):
                if callhlp.iscoroutinefunction(s.down) and loop:
                    loop.run_until_complete(s.down(kwargs))
                else:
                    s.down(kwargs)

    def getIRef(self,ns):
        label = ns.label
        if label is None:
            if ns.jobname != None:
                label = 'job::%s' % ns.jobname
        if label is None:
            return None
        iref = client.IRef(label)
        return iref

    async def doBasicAction(self,cl, ns,*,push=False, funcname=None, iref=None, args=False, kwargs=False):
        if funcname is None:
            _funcname = ns.funcname
        else:
            _funcname = funcname
        if iref is None:
            _iref = None
        else:
            _iref = self.getIRef(ns)
            if _iref is None:
                if iref=='required':
                    raise AssertionError('label not specified')
        if push:
            _result = await cl.xpush(_funcname, iref=_iref, args=ns.args, kwargs=ns.kwargs)
        else:
            _result = await cl.xcall(_funcname, iref=_iref, args=ns.args, kwargs=ns.kwargs)
        return _result


@ct_action('ocp.call')
async def _(*,cl,ns,ctapp,**ignored):
    """Perform the following remote call FUNCNAME(*ARGS,**KWARGS) and display reply"""
    return await ctapp.doBasicAction(cl, ns, iref=True,args=True,kwargs=True)


@ct_action('ocp.push')
async def _(*,cl,ns,ctapp,**ignored):
    """Perform the following remote call FUNCNAME(*ARGS,**KWARGS) without reply"""
    return await ctapp.doBasicAction(cl, ns, push=True, iref=True,args=True,kwargs=True)


@ct_action('ocp.publish')
async def _(*,cl,ns,ctapp,**ignored):
    """Ask the server to send the following publication TOPIC(*ARGS,**KWARGS)"""
    for topic in ns.topics:
        await cl.xpublish(topic, args=ns.args, kwargs=ns.kwargs)


@ct_action('ocp.subscribe')
async def _(*,ctapp,cl,ns,loop,**ignored):
    """Suscribe to topic TOPIC and dump incoming publications which match"""
    cnt = 0
    def _cb(*,type,frame=None,fut=None):
        nonlocal cnt
        ts = time.strftime("%Y-%m-%d %H:%M:%S")
        if type=='publication':
            _,topic,args,kwargs=frame
            d = dict(T=ts, TYPE=type, topic=topic, args=args, kwargs=kwargs)
        elif type=='subscription_done':
            d = dict(T=ts, TYPE=type, subscription_id=fut.result())
        else:
            raise AssertionError
        ctapp._print(d)
        cnt += 1
        if ns.count is None:
            return
        if cnt >= ns.count:
            cl.alert()
    def pub_cb(frame):
        _cb(type='publication',frame=frame)
    def subdone_cb(fut):
        _cb(type='subscription_done',fut=fut)
    cl.bind('*', pub_cb, raw=True)
    tsks = []
    for topic in ns.topics:
        tsks += [loop.create_task(cl.subscribe(topic))]
    fut = asyncio.gather(*tsks)
    fut.add_done_callback(subdone_cb)
    await cl.watch()
    if not fut.done():
        fut.cancel()


@ct_action('dbg.ns', loop_setup=False, client_setup=False)
def _(*,ns,**ignored):
    """Dump container returned by arguments parser"""
    d = {}
    for n in dir(ns):
        if n.startswith('_'):
            continue
        d[n]=getattr(ns,n)
    return d


@ct_action('dbg.nsargs', loop_setup=False, client_setup=False)
def _(*,ns,**ignored):
    """Dump args and kwargs returned by arguments parser"""
    d = {}
    for k in 'args','kwargs':
        d[k]=getattr(ns,k)
    return d


@ct_action('dbg.conf', loop_setup=False, client_setup=False)
def _(*,ns,ctapp,**ignored):
    """Dump olfactory configuration after config files parsing"""
    conf = config.Config()
    section = conf.getSection(ns.section)
    d = section.asDictionary()
    d2 = {}
    for k in sorted(d.keys()):
        v = d[k]
        print(k,v)
        if isinstance(v,set):
            v = list(sorted(v))
        elif inspect.isfunction(v):
            continue
        d2[k] = v
    return d2
