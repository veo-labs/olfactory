import sys
import os
import json
from olfactory.ocp import client
from urllib.parse import quote_from_bytes, unquote_to_bytes, quote, unquote


class _JSONEncoder(json.JSONEncoder):
    def default(self,obj):
        if isinstance(obj,bytes):
            repr = quote_from_bytes(obj,safe="/ ")
            return {'_class': 'bytes', 'repr': repr}
        elif isinstance(obj,client.IRef):
            lbl = obj.getLabel()
            return {'_class': 'IRef', 'label': lbl}
        else:
            return obj


def json_dumps(s,**kwargs):
    return json.dumps(s,sort_keys=True,cls=_JSONEncoder,**kwargs)


def json_loads(s,**kwargs):
    def object_hook(d):
        if '_class' not in d:
            return d
        if d['_class']=='IRef':
            return client.IRef(d['label'])
        elif d['_class']=='bytes':
            return unquote_to_bytes(d['repr'])
        else:
            raise AssertionError('Invalid field _class=%r' % d['_class'])
    return json.loads(s,object_hook=object_hook,**kwargs)


class JSONPrinter:
    def __init__(self,ns):
        self._ns = ns

    def __call__(self, obj):
        ns = self._ns
        s = json_dumps(obj,indent=ns.json_printer_indent)
        sys.stdout.write(s+'\n')
        sys.stdout.flush()


class Opt:
    def __init__(self, parent, attrname, *, name=None, ensure=None):
        if isinstance(parent,dict):
            self._parent = parent
        else:
            self._parent = parent.__dict__
        self._attrname = attrname
        self._set = False
        self._name = name
        self._ensure = ensure

    def ensureTarget(self):
        if self._attrname not in self._parent:
            self._parent[self._attrname] = self._ensure
        return self.getTarget()

    def setTarget(self,value):
        self._parent[self._attrname] = value

    def getTarget(self,ensure=[]):
        return self._parent[self._attrname]

    def append(self, *args):
        for a in args:
            self.appendSingle(a)


class CSVOpt(Opt):
    def appendSingle(self, a):
        t = self.ensureTarget()
        t += a.split(',')


class CWDOpt(Opt):
    def append(self, *ignored):
        self.setTarget(os.getcwd())


class MountDashOLikeOpt(Opt):
    def appendSingle(self, val):
        d = {}
        for x in val.split(','):
            l = x.split('=')
            l[0]=l[0].strip()
            if len(l[0])==0:
                continue
            elif len(l)==1:
                if l[0].startswith('+'):
                    d[l[0][1:]] = True
                elif l[0].startswith('-'):
                    d[l[0][1:]] = False
                elif l[0].startswith('~'):
                    d[l[0][1:]] = None
                else:
                    d[l[0]] = True
            elif len(l)==2:
                d[l[0]] = l[1]
            else:
                raise AssertionError(f'option {self._name}: syntax error')
        t = self.ensureTarget()
        t.update(d)


class JSONExpressionOpt(Opt):
    def appendSingle(self, val):
        t = self.ensureTarget()
        e = json_loads(val)
        t += [ e ]


class JSONDictOpt(Opt):
    def appendSingle(self, val):
        t = self.ensureTarget()
        e = json_loads(val)
        t.update(e)


class PercentStringOpt(Opt):
    def appendSingle(self,val):
        t = self.ensureTarget()
        t += [ unquote(val) ]


class PercentBytesOpt(Opt):
    def appendSingle(self,val):
        t = self.ensureTarget()
        t += [ unquote_to_bytes(val) ]


class JSONPairOpt(Opt):
    def appendSingle(self,val):
        t = self.ensureTarget()
        idx = val.find('=')
        if idx < 0:
            raise AssertionError(f'option {self._name}: "=" required')
        k = val[:idx]
        expr = val[idx+1:]
        v = json.loads(expr)
        t[k]=v


class ValueOpt(Opt):
    def appendSingle(self, value):
        t = self.setTarget(value)


class LabelOpt(Opt):
    def appendSingle(self,val):
        t = self.ensureTarget()
        t += [ client.IRef(val) ]


class JobOpt(Opt):
    def appendSingle(self,val):
        t = self.ensureTarget()
        t += [ client.IRef(f'job::{val}') ]


class LabelPairOpt(Opt):
    FORMAT = '{}'
    def appendSingle(self,val):
        idx = val.find('=')
        if idx < 0:
            raise AssertionError(f'option {self._name}: "=" required')
        k = val[:idx]
        v = val[idx+1:]
        t = self.ensureTarget()
        t[k] =  client.IRef(self.FORMAT.format(v))


class JobPairOpt(LabelPairOpt):
    FORMAT = 'job::{}'
