def get_runtime():
    # __pragma__ ('skip')
    return 'cpython'
    # __pragma__ ('noskip')
    return 'transcrypt'


def ensure_runtime(*expected):
    guessed = get_runtime()
    if guessed not in expected:
        raise AssertionError('unexpected runtime: guessed={} expected={}'.format(guessed,expected))

