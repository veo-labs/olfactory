import os
import copy
import logging
import sys

from olfactory import config

logger = logging.getLogger(__name__)


DEFINITIONS = {
    'plg_trigger':      dict(reverse=False, priority=None),
    'ocp_codec':        {},
    'ocp_class':        {},
    'ocp_method':       {},
    'ocp_exception':    {},
    'job_class':        {},
    'jobext_class':     {},
    'jobext_method':    {},
    'ct_action':        dict(loop_setup=True, client_setup=True),
    }


_manager_state = None

class Manager:
    def __init__(self):
        global _manager_state
        if _manager_state is not None:
            self.__dict__ = _manager_state
            return
        self._decorations = {}
        _manager_state = self.__dict__

    def addDecoration(self, decoration):
        n = decoration.getName()
        item = decoration.getItem()
        key = (item, n)
        self._decorations[key] = decoration
        logger.debug("adding %r" % decoration)
        return key

    def removeDecoration(self, key):
        del self._decorations[key]

    def decorate(self,name,item,**kwargs):
        d = Decoration(name, item, **kwargs)
        return self.addDecoration(d)

    def getDecoration(self, item, name):
        key = (item, name)
        try:
            return self._decorations[key]
        except KeyError:
            return None

    def getDecorations(self, required_name, *, parent=None):
        if parent:
            children = set()
            for n in dir(parent):
                if n.startswith('__') and n.endswith('__'):
                    continue
                ch = getattr(parent,n)
                children.add(ch)
        l = []
        for key,dec in self._decorations.items():
            item,name = key
            if name==required_name:
                if parent==None:
                    l.append(dec)
                elif item in children:
                    l.append(dec)
        return l


def decorate(name, item, **kwargs):
    m = Manager()
    return m.decorate(name,item, **kwargs)


class Decoration:
    def __init__(self, name, item, **kwargs):
        self._name = name
        self._item = item
        self._kwargs = kwargs

    def __repr__(self):
        return '<Decoration %r kw=%r on %r>' % (self._name, self._kwargs, self._item.__qualname__)

    def getName(self):
        return self._name

    def getKwarg(self, key):
        return self._kwargs[key]

    def getArg0(self):
        return self.getKwarg('_arg0')

    def getObject(self):
        return self.getKwarg('obj')

    def getItem(self):
        return self._item

    def getKwargs(self):
        return self._kwargs


class Decorator:
    def __init__(self, name, defaults={}, *, decorate):
        self._name = name
        self._defaults = defaults
        self._decorate = decorate

    def decorate(self, item, **kwargs_p):
        kwargs = {}
        kwargs.update(self._defaults)
        kwargs.update(kwargs_p)
        self._decorate(self._name, item, **kwargs)

    def outer(self, arg0_or_item=None, **kwargs):
        if callable(arg0_or_item):
            self.decorate(arg0_or_item)
            return arg0_or_item
        def inner(item):
            self.decorate(item, _arg0=arg0_or_item, **kwargs)
            return item
        return inner

    __call__ = outer


__all__ = []
for k,v in DEFINITIONS.items():
    d = Decorator(k,v,decorate=decorate)
    globals()[k]=d
    __all__.append(k)
