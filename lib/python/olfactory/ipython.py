import os
import re
import shlex
import sys
import argparse
import copy
import subprocess
import time
import shlex
from olfactory import plugin, deleg, terminal, config, prochlp, loghlp, excdef
from olfactory.ocp import client


class JobsContainer:
    def __init__(self, prefix=''):
        self._prefix = prefix
        self._entries = {}

    def _cleanup(self):
        self._entries = {}

    def _getSortedEntriesKeys(self):
        l = list(self._entries.keys())
        l.sort()
        return l

    def __dir__(self):
        return self._getSortedEntriesKeys()

    def __repr__(self):
        l = self._getSortedEntriesKeys()
        return repr(l)

    def __getattr__(self,name):
        if name in self._entries:
            return self._entries[name]
        else:
            raise AttributeError(f'no such job {name}')

    def _escapeLabel(self, label):
        s = label
        if label.startswith(self._prefix):
            s = s[len(self._prefix):]
        s = s.replace(':','_')
        s = s.replace('.','_')
        s = s.replace('-','_')
        return s

    def _add(self, iref):
        label = iref.getLabel()
        key = self._escapeLabel(label)
        self._entries[key] = iref

    def _addMany(self, *irefs):
        for iref in irefs:
            self._add(iref)


class LowLevelClientCaller:
    def __init__(self, parent, name):
        self._parent = parent
        self._name = name

    def __call__(self,*args,**kwargs):
        cl = self._parent._client
        f = getattr(cl, self._name)
        return f(*args,**kwargs)


class ArgumentParser(argparse.ArgumentParser):
    def __init__(self):
        super().__init__(self,'.log_install')

    def error(self,msg):
        raise AssertionError(msg)


class OlfactoryExtension:
    def __init__(self,ip,*,name='olf'):
        self._ip = ip
        self._loop = deleg.get_event_loop()
        self._terminal = terminal.Terminal()
        self._name = name
        self._proc_runners = []
        self.jobs = JobsContainer('job::')
        identity = "ipython-%d-%s" % (os.getpid(), self._name)
        cl_ = client.Client(identity=identity,loop=self._loop)
        cl = deleg.Instance(cl_,loop=self._loop)
        self._client = cl
        ip.push({name: self})
        self._argparser = ArgumentParser()
        m_ = plugin.Manager()
        m = deleg.Instance(m_,loop=self._loop)
        m.provide(m_.Provision('argparser',self._argparser,annotation='tune'),loop=self._loop)
        m.ensureTriggersExecutionFinished()
        self.log_setopts()

    def clone(self, name):
        klass = self.__class__
        return klass(self._ip,name=name)

    def log_setopts(self,argsp=None):
        m = plugin.Manager()
        opts = loghlp.Options()
        opts.setStreamname('stdout')
        if argsp is not None:
            args = shlex.split(argsp)
            ns = self._argparser.parse_args(args)
        else:
            ns = self._argparser.parse_args([])
        loghlp.install_from_ns(ns)

    def log_clearrules(self):
        self.log_setopts('--lCd')

    def connect(self,url=None,authparams=None):
        cl = self._client
        try:
            self.disconnect()
        except excdef.StateError:
            pass
        conf = config.Config()
        if url is None:
            url =conf.getItem('connect_url')
        cl.connect(url,authparams=authparams)
        for name in dir(cl):
            if name.startswith('_'):
                continue
            if hasattr(self,name):
                continue
            caller = LowLevelClientCaller(self, name)
            setattr(self,name,caller)

    def disconnect(self):
        cl = self._client
        cl.disconnect()
        self.jobs._cleanup()

    def runProcess(self, cmd, **kwargs):
        pr_ = prochlp.ProcessRunner(cmd,shell=True, **kwargs)
        pr = deleg.Instance(pr_)
        pr.launch()
        self._proc_runners.append(pr)
        return pr

    def killAllProcessRunners(self):
        proc_runners = []
        for pr in self._proc_runners:
            if not pr.running:
                continue
            proc_runners.append(pr)
            pr.kill()
        self._proc_runners = proc_runners

    def rescanJobs(self,**kwargs):
        ip = self._ip
        cl = self._client
        self.jobs._cleanup()
        irefs = cl.call('job::get_all')
        self.jobs._addMany(*irefs)


def load_ipython_extension(ip):
    m = plugin.Manager()
    m.importAll()
    OlfactoryExtension(ip)
