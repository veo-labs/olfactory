import sys
import os
import logging
import inspect
import copy
from olfactory import interp, excdef


logger = logging.getLogger(__name__)

_NO_DEFAULT = object()


def _get_top_dir():
    topdir = os.path.dirname(__file__)
    topdir = os.path.join(topdir, '../../..')
    topdir = os.path.abspath(topdir)
    return topdir

def _dump_symbols(title):
    frm = inspect.currentframe()
    frm = frm.f_back
    print('* %s' % title)
    print('globals = %r' % frm.f_globals)
    print('locals = %r' % frm.f_locals)
    print()


class SectionFactory:
    def __init__(self,configp):
        self._config = configp

    def __call__(self,name):
        return Section(self._config,name)


class Section:
    def __init__(self,configp,name):
        if name in configp._sections:
            self.__dict__ = configp._sections[name]
            return
        self._name = name
        self._items = {}
        configp._sections[name] = self.__dict__

    def _getLocals(self):
        frm = inspect.currentframe()
        frm = frm.f_back.f_back
        return frm.f_locals

    def _getGlobals(self):
        frm = inspect.currentframe()
        frm = frm.f_back.f_back
        return frm.f_globals

    def __enter__(self):
        l = self._getLocals()
        g = self._getGlobals()
        g.update(l)
        l.clear()
        l.update(self._items)
        l['_current_section_name'] = self._name

    def __exit__(self,*args):
        l = self._getLocals()
        del l['_current_section_name']
        self._items.update(l)
        l.clear()

    def setItem(self, name, obj):
        self._items[name] = obj

    def getItem(self, name, default_value=_NO_DEFAULT):
        try:
            return self._items[name]
        except KeyError:
            if default_value==_NO_DEFAULT:
                raise
            else:
                return default_value

    def delItem(self,name):
        del self._items[name]

    def asDictionary(self):
        return dict(self._items)

    def _getRawItems(self):
        return self._items


def olfactory_config_section(orig_method):
    def new_method(self, *args, **kwargs):
        self._ensureScanned()
        name = orig_method.__name__
        section = self.getSection('olfactory')
        f = getattr(section,name)
        return f(*args,**kwargs)
    return new_method


class BaseConfig:
    def __init__(self,files):
        self._globals = {'__builtins__': {}}
        self._scanned = False
        self._scanning = False
        self._files = files
        self._sections = {}
        self._addGlobalBuiltin('Section', SectionFactory(self))
        self._addGlobalBuiltin('_dumpsymbols', _dump_symbols)
        self._addGlobalBuiltin('dict', dict)
        self._addGlobalBuiltin('set', set)
        self._addGlobalBuiltin('copy', copy.copy)
        self._addGlobalBuiltin('getenv', os.getenv)

    def _addGlobalBuiltin(self,name,obj):
        g = self._globals
        b = g['__builtins__']
        b[name]=obj

    def getTopDir(self):
        return _get_top_dir()

    def getSection(self,name):
        self._ensureScanned()
        return Section(self,name)

    def _ensureScanned(self):
        if self._scanned:
            return
        if self._scanning:
            raise Assertion('_ensureScanned() reentrance')
        self._scanning = True
        try:
            for fn in self._files:
                if fn.strip()=='':
                    continue
                try:
                    src = interp.Source(filename=fn,mode='exec')
                except FileNotFoundError:
                    continue
                g = copy.copy(self._globals)
                g['__file__'] = fn
                l = {}
                src.execute(g,l)
            self._scanned = True
        finally:
            self._scanning = False

    @olfactory_config_section
    def setItem(self, name, obj):
        pass

    @olfactory_config_section
    def getItem(self):
        pass

    @olfactory_config_section
    def delItem(self,name):
        pass

    @olfactory_config_section
    def getItems(self):
        pass

    @olfactory_config_section
    def asDictionary(self):
        pass


_config_state = None

class Config(BaseConfig):
    def __init__(self):
        global _config_state
        if _config_state is not None:
            self.__dict__ = _config_state
            return
        files = os.getenv('OLFACTORY_CONFIG_FILES','')
        files = files.split(':')
        files = ["%s/default-config.py" % _get_top_dir()]  +  files
        super().__init__(files)
        section = Section(self,'olfactory')
        section.setItem('topdir',_get_top_dir)
        _config_state = self.__dict__
        self._addGlobalBuiltin('abspath', os.path.abspath)
