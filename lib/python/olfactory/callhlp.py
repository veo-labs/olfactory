import inspect
import weakref
import functools


def make_weakref(obj):
    if inspect.ismethod(obj):
        return weakref.WeakMethod(obj)
    elif inspect.isfunction(obj):
        return weakref.ref(obj)
    elif inspect.isclass(obj):
        return weakref.ref(obj)
    elif isinstance(obj,functools.partial):
        return weakref.ref(obj)
    else:
        raise TypeError(f'cannot make weakref of {obj!r}')


async def ensure_coroutine(obj,*args,**kwargs):
    if iscoroutinefunction(obj):
        rval = await obj(*args,**kwargs)
    else:
        rval = obj(*args,**kwargs)
    return rval


def iscoroutinefunction(obj):
    if isinstance(obj,functools.partial):
        return inspect.iscoroutinefunction(obj.func)
    else:
        return inspect.iscoroutinefunction(obj)
