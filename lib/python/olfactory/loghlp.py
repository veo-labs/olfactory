from olfactory.runtime import *

ensure_runtime('cpython', 'transcrypt')

# __pragma__ ('skip')
import sys
import copy
import logging
import functools
import argparse
import optparse
import copy
import time
import faulthandler
from olfactory import oregexp, excfmt, terminal
from olfactory.stamp import *
# __pragma__ ('noskip')

if get_runtime()=='transcrypt':
    from olfactory.transcrypt import logging


def get_sub_logger(logger,name):
    loggername = "{}[{}]".format(logger.name, name)
    sublogger = logging.getLogger(loggername)
    return sublogger


# __pragma__ ('skip')
DECISION_DROP   = 0
DECISION_ACCEPT = 1
DECISION_PASS   = 2

# default values for rotating logs
ROTATE_MAXSIZE = 1024*1024
ROTATE_COUNT = 2


DATE_FORMAT = '%Y-%m-%d %H:%M:%S'


def get_group_and_add(parser):
    if has_stamp(parser,'group_and_add'):
        return get_stamp(parser,'group_and_add')
    if 'pytest' in parser.__class__.__module__:
        group = parser.getgroup('olfactory logging options')
        add = group.addoption
    else:
        group = parser.add_argument_group('olfactory logging options')
        add = group.add_argument
    set_stamp(parser,'group_and_add',(group,add))
    return group,add


def tune_argparser(parser):
    opts = Options()
    group,add = get_group_and_add(parser)
    if opts.install_filter:
        add("--lL",  type=str,  metavar='LOGLEVEL', dest="_loghlp_lL",  action='append',
            default=Appender('setLevel'), help="set log level to LOGLEVEL")

        add("--lLd", dest="_loghlp_lLd", action='append_const', default=Appender('setLevel'),
            const='debug', help="set log level to 'debug'")

        add("--lA",  type=str,  metavar="LOGRULES", dest="_loghlp_lA", action='append',
            default=Appender('addAcceptRule'), help="add accepting log rule")

        add("--lD",  type=str,  metavar="LOGRULES", dest="_loghlp_lD", action='append',
            default=Appender('addDropRule'), help="add dropping log rule")

        add("--lCa", dest="_loghlp_lCa", action='append_const', default=Appender('clearAndSetDefaultDecision'),
            const=DECISION_ACCEPT, help="clear LOGRULES and set default decision to accept")

        add("--lCd", "--lC", dest="_loghlp_lCd", action='append_const', default=Appender('clearAndSetDefaultDecision'),
            const=DECISION_DROP, help="clear LOGRULES and set default decision to drop")

        add("--lOi", metavar="LOGRULES", dest="_loghlp_lOi", action='append_const', default=Appender('setupOlfactoryOnly'),
            const='info', help="shortcut for '--lCd --lL info --lA olfactory.*'")

        add("--lOd", metavar="LOGRULES", dest="_loghlp_lOd", action='append_const', default=Appender('setupOlfactoryOnly'),
            const='debug', help="shortcut for '--lCd --lL debug --lA olfactory.*'")
    if opts.install_handler:
        add("--lF",  type=str,  metavar='LOGFILENAME', dest="_loghlp_lF", action='append',
            default=Appender('setFilename'), help="append logs to file LOGFILENAME")

        add("--lK", type=str,  metavar='CRASHFILENAME', dest="_loghlp_lK", action='append',
            default=Appender('setCrashFilename'), help="appends crash backtrace to file CRASHFILENAME")

        add("--lR", dest="_loghlp_lR", action='append_const', const=None, default=Appender('enableRotate'),
            help=f"enable log rotation of log file ('--lF' one ) with defaults (ROTATE_MAXSIZE={ROTATE_MAXSIZE} and ROTATE_COUNT={ROTATE_COUNT})")

        add("--lRs", metavar='ROTATE_MAXSIZE', dest="_loghlp_lRs", action='append', type=int,
            default=Appender('setRotateMaxSize'), help=f"enable log rotation of log file and set ROTATE_MAXSIZE (default is {ROTATE_MAXSIZE})")

        add("--lRc", metavar='ROTATE_COUNT', dest="_loghlp_lRc", action='append', type=int,
            default=Appender('setRotateCount'), help=f"enable log rotation of log file and set ROTATE_COUNT (default is {ROTATE_COUNT})")

        add("--lS",  type=str,  metavar='LOGSTREAMNAME', dest="_loghlp_lS", action='append',
            default=Appender('setStreamname'), help="send logs to stream LOGSTREAMNAME (can be stdout,stderr,null)")

        add("--lS0",  metavar='LOGSTREAMNAME', dest="_loghlp_lS", action='append_const',
            const='null', default=Appender('setStreamname'), help="send logs to null")

        add("--lS1",  metavar='LOGSTREAMNAME', dest="_loghlp_lS", action='append_const',
            const='stdout', default=Appender('setStreamname'), help="send logs to stdout")

        add("--lS2",  metavar='LOGSTREAMNAME', dest="_loghlp_lS", action='append_const',
            const='stderr', default=Appender('setStreamname'), help="send logs to stderr")

        add("--lN", dest="_loghlp_lN", action='append_const',
            const=False, default=Appender('setColor'), help="disable color")

        add("--lU",  type=str,  metavar='SYSLOGSOCKET', dest="_loghlp_lU", action='append',
            default=Appender('setSyslogSocket'), help="append logs to syslog using socket SYSLOGSOCKET")

        add("--lUf",  type=str,  metavar='SYSLOGFACILITY', dest="_loghlp_lUf", action='append',
            default=Appender('setSyslogFacility'), help="use SYSLOGFACILITY when appending logs to syslog")

    return group,add


_options_state = None


class Options:
    def __init__(self):
        global _options_state
        if _options_state is not None:
            self.__dict__ = _options_state
            return
        self._rules = []
        self._filename = None
        self._rotate_enabled = False
        self._rotate_maxsize = ROTATE_MAXSIZE
        self._rotate_count = ROTATE_COUNT
        self._streamname = 'unset'
        self._color = None
        self._default_decision = DECISION_DROP
        self._level = logging.INFO
        self.install_filter = True
        self.install_handler = True
        self._crash_filename = None
        self._syslogsock = None
        self._syslogfac = 'local0'
        self.altered_by_argparser = False
        _options_state = self.__dict__

    def __repr__(self):
        return f'<Options {self.__dict__!r}>'

    def getRules(self):
        return self._rules

    def getLevel(self):
        return self._level

    def setColor(self,b):
        self._color = b

    def getColor(self):
        return self._color

    def setFilename(self,fn):
        self._filename = fn

    def getFilename(self):
        return self._filename

    def getStreamname(self):
        return self._streamname

    def setStreamname(self, n):
        if n not in ('stderr', 'stdout', 'null','unset'):
            raise AssertionError('invalid stream name: %r' % n)
        self._streamname = n

    def setSyslogSocket(self,ss):
        self._syslogsock = ss

    def getSyslogSocket(self):
        return self._syslogsock

    def getDefaultDecision(self):
        return self._default_decision

    def setDefaultDecision(self,decision):
        self._default_decision = decision

    def setRules(self, rules):
        self._rules = rules

    def setLevel(self, level):
        if isinstance(level,str):
            level = level.upper()
        level = logging._checkLevel(level)
        self._level = level

    def setSyslogFacility(self,sf):
        self._syslogfac = sf

    def getSyslogFacility(self):
        return self._syslogfac

    def addAcceptRule(self, pattern):
        r = AcceptRule(pattern)
        self._rules.append(r)

    def addDropRule(self, pattern):
        r = DropRule(pattern)
        self._rules.append(r)

    def clearRules(self):
        self._rules = []

    def enableRotate(self,*,maxsize=None,count=None):
        self._rotate_enabled = True
        if maxsize is not None:
            self._rotate_maxsize = maxsize
        if count is not None:
            self._rotate_count = count

    def getRotateMaxSize(self):
        return self._rotate_maxsize

    def getRotateCount(self):
        return self._rotate_count

    def isRotateEnabled(self):
        return self._rotate_enabled

    def setCrashFilename(self,fn):
        self._crash_filename = fn

    def getCrashFilename(self):
        return self._crash_filename


class Appender:
    def __init__(self, name):
        self._opts = Options()
        self._name = name

    def append(self, const):
        f = getattr(self, "_%s" % self._name)
        f(const)
        opts = self._opts
        opts.altered_by_argparser = True

    def _setColor(self, b):
        opts = self._opts
        opts.setColor(b)

    def _setFilename(self, fn):
        opts = self._opts
        opts.setFilename(fn)

    def _setSyslogSocket(self, ss):
        opts = self._opts
        opts.setSyslogSocket(ss)

    def _enableRotate(self,ignored=None):
        opts = self._opts
        opts.enableRotate()

    def _setRotateMaxSize(self, maxsize):
        opts = self._opts
        opts.enableRotate(maxsize=maxsize)

    def _setRotateCount(self, count):
        opts = self._opts
        opts.enableRotate(count=count)

    def _setStreamname(self, sn):
        opts = self._opts
        opts.setStreamname(sn)

    def _setCrashFilename(self,fn):
        opts = self._opts
        opts.setCrashFilename(fn)

    def _setLevel(self, level):
        opts = self._opts
        opts.setLevel(level)

    def _setSyslogFacility(self, sf):
        opts = self._opts
        opts.setSyslogFacility(sf)

    def _addAcceptRule(self, const):
        opts = self._opts
        opts.addAcceptRule(const)

    def _addDropRule(self, const):
        opts = self._opts
        opts.addDropRule(const)

    def _clearAndSetDefaultDecision(self, const):
        opts = self._opts
        opts.clearRules()
        opts.setDefaultDecision(const)

    def _setupOlfactoryOnly(self, level):
        opts = self._opts
        opts.clearRules()
        opts.setLevel(level)
        opts.setDefaultDecision(DECISION_DROP)
        opts.addAcceptRule('olfactory.*')


def remove_all_logging_handlers():
    root = logging.getLogger()
    for h in root.handlers[:]:
        root.removeHandler(h)


def install_filter_from_ns(ns):
    opts = Options()
    if not opts.install_filter:
        return
    root = logging.getLogger()
    root.setLevel(opts.getLevel())
    f = Filter()
    if len(root.handlers)==0:
        raise AssertionError('No logging handlers')
    for h in root.handlers:
        h.addFilter(f)
    for r in opts.getRules():
        f.addRule(r)
    default_decision = opts.getDefaultDecision()
    f.setDefaultDecision(default_decision)
    return f


def install_from_ns(ns):
    remove_all_logging_handlers()
    install_handler_from_ns(ns)
    install_filter_from_ns(ns)


def _set_formatter(ns,h):
    dfs = DATE_FORMAT
    fs = '{asctime} {levelname} {name} {message}'
    opts = Options()
    color = opts.getColor()
    if color is None:
        color = False
        if h.__class__==StderrHandler or h.__class__==StdoutHandler:
            color = True
    if color:
        for x in 'asctime', 'name', 'message', 'levelname':
            fs = fs.replace('{%s}' % x, '{col_%s:+}{%s}{col_%s:-}' % (x,x,x) )
    fs = fs.replace('{levelname}', '{levelname:^9}')
    fs = fs.replace('{name}', '{name:40}')
    if color:
        t = terminal.Terminal(h.stream)
        fmt = ColorizedFormatter(fs, dfs, t)
    else:
        fmt = logging.Formatter(fs, dfs, '{')
    h.setFormatter(fmt)


class StderrHandler(logging.StreamHandler):
    def __init__(self):
        logging.Handler.__init__(self)
        self._stream = sys.stderr

    @property
    def stream(self):
        return self._stream


class StdoutHandler(logging.StreamHandler):
    def __init__(self):
        logging.Handler.__init__(self)
        self._stream = sys.stdout

    @property
    def stream(self):
        return self._stream


class ColorizedFormatter(logging.Formatter):
    def __init__(self, fmt, datefmt, terminal):
        super().__init__(fmt, datefmt,'{')
        self._terminal = terminal
        t = self._terminal
        cols = {}
        cols['CRITICAL'] = t.fgcol('#ff0000')
        cols['ERROR'] = t.fgcol('#c01020')
        cols['WARNING'] = t.fgcol('#c0d020')
        cols['INFO'] = t.fgcol('#00ff80')
        cols['DEBUG'] = t.fgcol('#d0ffd0')
        cols['col_asctime'] = t.fgcol('#40d0d0')
        cols['col_name'] = t.fgcol('#8080ff')
        cols['col_message'] = t.fgcol('#d0d0ff')
        self._colors = cols

    def formatMessage(self, record):
        d = copy.copy(record.__dict__)
        d.update(self._colors)
        d['col_levelname'] = self._colors[record.levelname]
        return self._fmt.format(**d)


def install_handler_from_ns(ns):
    opts = Options()
    if not opts.install_handler:
        return
    root = logging.getLogger()
    fn = opts.getFilename()
    handlers = []
    if fn is not None:
        rotate = opts.isRotateEnabled()
        if not rotate:
            h = logging.FileHandler(fn, 'a')
        else:
            maxsize = opts.getRotateMaxSize()
            cnt = opts.getRotateCount()
            h = logging.handlers.RotatingFileHandler(fn, mode='a', maxBytes=int(maxsize/cnt), backupCount=cnt-1)
        handlers.append(h)
    ss = opts.getSyslogSocket()
    if ss is not None:
        sf = opts.getSyslogFacility()
        h = logging.handlers.SysLogHandler(address=ss, facility=sf)
        handlers.append(h)
    sn = opts.getStreamname()
    if sn=='stderr':
        h = StderrHandler()
        handlers.append(h)
    elif sn=='stdout':
        h = StdoutHandler()
        handlers.append(h)
    elif sn=='null' or sn=='unset':
        h = logging.NullHandler()
        handlers.append(h)
    else:
        raise AssertionError()
    for h in handlers:
        root.addHandler(h)
        _set_formatter(ns,h)
    fn = opts.getCrashFilename()
    if fn is None:
        return
    f = open(fn,'a')
    f.write('\n\n\n')
    f.write(f"{'-'*80}\n")
    f.write(f"opened at {time.strftime(DATE_FORMAT)}\n")
    f.write(f"{'-'*80}\n")
    f.flush()
    faulthandler.enable(f,all_threads=True)


class PatternRule:
    def __init__(self, pattern):
        self._pattern = pattern
        self._regexp = oregexp.ORegExp(pattern)

    def _match(self, record):
        m = self._regexp.match(record.name)
        if not m:
            return False
        return True

    def __call__(self, record):
        m = self._match(record)
        if m:
            return self.MATCH_DECISION
        else:
            return self.NOMATCH_DECISION

    def __repr__(self):
        return '<%s pattern=%s>' % (self.__class__.__name__, self._pattern)


class AcceptRule(PatternRule):
    MATCH_DECISION = DECISION_ACCEPT
    NOMATCH_DECISION = DECISION_PASS

class DropRule(PatternRule):
    MATCH_DECISION = DECISION_DROP
    NOMATCH_DECISION = DECISION_PASS

class Filter:
    def __init__(self):
        self._rules = []
        self._default_decision = DECISION_DROP

    def addRule(self, rule):
        self._rules.append(rule)

    def setDefaultDecision(self, decision):
        self._default_decision = decision

    def filter(self, record):
        decision = self._default_decision
        for r in self._rules:
            ret = r(record)
            if ret==DECISION_ACCEPT:
                decision = DECISION_ACCEPT
            elif ret==DECISION_DROP:
                decision = DECISION_DROP
            elif ret==DECISION_PASS:
                pass
            else:
                raise AssertionError()
        if decision==DECISION_ACCEPT:
            rval = True
        elif decision==DECISION_DROP:
            rval = False
        else:
            raise AssertionError()
        return rval


NCOLS = 80

def log_exception(logger, *, level=logging.ERROR, title=None, triplet=None, with_tb=True):
    lines = excfmt.format_multi(triplet=triplet, with_tb=with_tb, remove_eol=True)
    log_lines(logger, lines, level=level, title=title)


def log_lines(logger, lines, *, level=logging.ERROR, title=None, truncate=None):
    if truncate is not None:
        lines2 = []
        for ln in lines:
            lines2.append(ln[:truncate])
        lines = lines2
    ncols = 40
    for ln in lines + [title]:
        ncols = max(ncols, len(ln) + 4)
    if title:
        logger.log(level,'=' * ncols)
        logger.log(level,title)
        logger.log(level,'-' * ncols)
    for ln in lines:
        logger.log(level,'  ' + ln)
    if title:
        logger.log(level,'=' * ncols)
        logger.log(level,'')
# __pragma__ ('noskip')
