import sys
import logging
import asyncio
import websockets
import websockets.exceptions
import msgpack
import weakref
import copy
import inspect

from olfactory import excfmt, excdef, oregexp, decorators, loghlp, urlhlp, plugin, callhlp
from olfactory.stamp import *
from olfactory.ocp.common import *

logger = logging.getLogger(__name__)

# ------------------------------------------------------------------------------
# Subscription
# ------------------------------------------------------------------------------

class Subscription:
    def __init__(self, server, *, socket, requestid, pattern):
        self.socket = socket
        self.pattern = pattern
        self.requestid = requestid
        self._server = server
        self._regexp = oregexp.ORegExp(pattern)

    async def processPrologues(self):
        for pro in self._server._getPrologues():
            if self.match(pro.topic):
                clbl = pro.wref_callable()
                if clbl is None:
                    continue
                await callhlp.ensure_coroutine(clbl,self.socket,*pro.args,**pro.kwargs)

    def match(self, topic):
        m = self._regexp.match(topic)
        if not m:
            return False
        h = self._server._perm_handler
        if h is not None:
            kwargs = {}
            kwargs['authinfo'] = get_stamp(self.socket,'authinfo',{})
            kwargs['socket'] = self.socket
            kwargs['frmtype'] = FRAME_TYPE_NAMES[FRAME_TYPE_PUBLISH]
            kwargs['label'] = None
            kwargs['name'] = topic
            h = h()
            if h is None:
                return False
            try:
                granted = h(**kwargs)
            except:
                granted = False
            if not granted:
                return False
        return True



# ------------------------------------------------------------------------------
# Prologue
# ------------------------------------------------------------------------------

class Prologue:
    def __init__(self, *, topic, callable, args=(), kwargs={}):
        self.topic = topic
        self.wref_callable = callhlp.make_weakref(callable)
        self.args = args
        self.kwargs = kwargs


# ------------------------------------------------------------------------------
# Publication
# ------------------------------------------------------------------------------

class Publication:
    def __init__(self, *, topic, socket=None, args=(), kwargs={}):
        self.topic = topic
        self.socket = socket
        self.args = args
        self.kwargs = kwargs


# ------------------------------------------------------------------------------
# Exposure
# ------------------------------------------------------------------------------

class Exposure:
    def __init__(self, server, funcname, callable, *, force_async=None, raw=False, log_exception=True):
        self._server = server
        self._wref_callable = callhlp.make_weakref(callable)
        self._funcname = funcname
        self._force_async = force_async
        self._raw = raw
        self._log_exception = log_exception
        if force_async is None:
            self._async = callhlp.iscoroutinefunction(callable)
        else:
            self._async = force_async

    async def _coro(self,frame,*,socket,send_reply,async_):
        _,requestid,_,_,args,kwargs = frame
        if self._raw:
            args = ()
            kwargs = {'frame': frame, 'socket': socket}
        rval = None
        f = self._wref_callable()
        triplet = None,None,None
        try:
            if f is None:
                raise excdef.NoLongerAvailableError('callable associated with exposure has been deleted')
            if self._async:
                rval = await f(*args,**kwargs)
            else:
                rval = f(*args,**kwargs)
        except:
            triplet = sys.exc_info()
            s = self._server
            et,ev,tb = triplet
            if (et is not asyncio.CancelledError)  and  self._log_exception:
                loghlp.log_exception(s._logger, triplet=triplet, level=logging.DEBUG, title='Exposure failed')
            if hasattr(ev,'olfactory_fields'):
                error_fields = copy.copy(ev.olfactory_fields)
                if 'serialized' in error_fields:
                    del error_fields['serialized']
            else:
                name, msg = excfmt.split(triplet=triplet)
                error_fields = dict(name=name,msg=msg)
            self._server._callErrorHandler(error_fields,*triplet)
        else:
            error_fields = {}
        if send_reply:
            frame_out = [ FRAME_TYPE_REPLY, requestid, rval, error_fields ]
            if socket.open:
                await self._server._send(socket,frame_out)
        async_tasks = get_stamp(socket, 'async_tasks')
        if requestid in async_tasks:
            del async_tasks[requestid]

    async def __call__(self, frame, *, socket, send_reply):
        async_tasks = get_stamp(socket, 'async_tasks')
        coro = self._coro(frame,socket=socket,send_reply=send_reply,async_=self._async)
        _,requestid,_,_,_,_ = frame
        if self._async:
            fut = asyncio.ensure_future(coro)
            async_tasks[requestid] = fut
        else:
            await coro


# ------------------------------------------------------------------------------
# Failure
# ------------------------------------------------------------------------------

class Failure(Exposure):
    def __init__(self, server, exc_class, *exc_args, force_async=False,**kwargs):
        super().__init__(server,None,self._raise,force_async=False, **kwargs)
        self._exc_class = exc_class
        self._exc_args = exc_args

    def _raise(self, *ignored, **ignored_):
        klass = self._exc_class
        args = self._exc_args
        raise klass(*args)


# ------------------------------------------------------------------------------
# SystemCalls
# ------------------------------------------------------------------------------

class SystemCalls:
    @decorators.ocp_class
    class Dummy:
        def __init__(self, name):
            self._name = name

        def __repr__(self):
            return '<Dummy %r>' % self._name

    def __init__(self, server):
        self._server = server

    def create_dummy_instance(self,name):
        return self.Dummy(name)

    def add_label(self,obj,label):
        self._server.putLabel(obj,label)

    def remove_label(self,obj):
        self._server.removeLabel(obj)

    def get_all_instances(self):
        return self._server._getAllInstances()

    def get_all_master_labels(self):
        return self._server._getAllMasterLabels()

    def get_all_labels(self):
        return self._server._getAllLabels()

    def get_instance_from_label(self, label):
        return self._server._getInstanceFromLabel(label)

    def _ensure_instance(self,label_or_obj):
        s = self._server
        if isinstance(label_or_obj, str):
            obj = s._getInstanceFromLabel(label_or_obj)
        else:
            obj = label_or_obj
        return obj

    def get_master_label(self, label_or_obj):
        s = self._server
        obj = self._ensure_instance(label_or_obj)
        label = s._getMasterLabel(obj)
        return label

    def return_args_and_kwargs(self,*args,**kwargs):
        return {'args': args, 'kwargs': kwargs}

    def get_instance_repr(self, label_or_obj):
        obj = self._ensure_instance(label_or_obj)
        return repr(obj)

    def get_attribute(self,obj,name):
        return getattr(obj,name)

    def get_identity(self):
        return self._server.getIdentity()

    async def delayed_div(self,delay,a,b):
        await asyncio.sleep(delay)
        return a/b

    def throw(self,msg):
        raise AssertionError(msg)

    async def delayed_throw(self,msg,*,delay):
        await asyncio.sleep(delay)
        raise AssertionError(msg)

    async def sleep(self, delay):
        delay = int(delay)
        await asyncio.sleep(delay)

    async def publish(self, topic, *args, **kwargs):
        s = self._server
        p = Publication(topic=topic,args=args,kwargs=kwargs)
        await s._publish(p)

    def printf(self,format,*args):
        print(format % args)

    def log(self, msg='', *, level='warning', name=None):
        if name is None:
            logger = self._server._logger
        else:
            logger = logging.getLogger(name)
        lvl = logging._nameToLevel[level.upper()]
        logger.log(lvl,msg)

    async def log_periodically(self, msg, *, period=1):
        i=0
        while True:
            logger = self._server._logger
            logger.debug("{} #{}".format(msg,i))
            await asyncio.sleep(period)
            i += 1


# ------------------------------------------------------------------------------
# RawSystemCalls
# ------------------------------------------------------------------------------

class RawSystemCalls:
    def __init__(self, server):
        self._server = server

    async def subscribe(self, **kwargs):
        s = self._server
        rval = await s._subscribe(**kwargs)
        return rval

    def unsubscribe(self, **kwargs):
        s = self._server
        return s._unsubscribe(**kwargs)

    def get_authinfo(self,*,socket,**ignored):
        authinfo = get_stamp(socket,'authinfo',{})
        return authinfo


# ------------------------------------------------------------------------------
# ServerSideIRef
# ------------------------------------------------------------------------------

class ServerSideIRef:
    def __init__(self,label):
        self._label = label

    def __getattribute__(self,name):
        if name in ('getLabel','__repr__','_label', '__class__'):
            return super().__getattribute__(name)
        else:
            raise excdef.LabelError("No instance referenced as %r" % self._label)

    def getLabel(self):
        return self._label

    def __repr__(self):
        return '<%s label=%r>' % (self.__class__.__name__,self._label)


class InvalidServerSideIRef(ServerSideIRef):
    pass


# ------------------------------------------------------------------------------
# Server
# ------------------------------------------------------------------------------

class Server(Node):
    """Server for ``ocp`` protocol.

    :param url: listening url of the server. The following format is currently
        supported: ``ws://host:port``. If ``host`` is ``*`` the server will
        listen on all ``IP`` interfaces.

    :param loop: ``asyncio`` loop. If ``None``, ``asyncio.get_event_loop()``
        is used.

    :param identity: string used to identify this server in logs on client
        and server side.

    Usage in ``python3``:

    .. code:: python3

        from olfactory.ocp import server
        (...)
        srv = server.Server('ws://*:9087',identity='shiva')
    """

    def __init__(self, url, *, identity='srv', loop=None):
        super().__init__(loop,identity)
        self._exposures = {}
        self._url = urlhlp.URL(url)
        self._prologues = []
        self._subscriptions = {}
        self._server = None
        self._obj_to_lbl = weakref.WeakKeyDictionary()
        self._lbl_to_obj = weakref.WeakValueDictionary()
        self._syscalls = None
        self._raw_syscalls = None
        self._initSystemCalls()
        self._pub_queue = asyncio.Queue(loop=loop)
        self._pub_fut = None
        self._initCodecs()
        self._auth_handler = None
        self._perm_handler = None
        self._error_handler = None
        self._disconnect_handlers = []
        self._subscribe_handlers = []
        self._unsubscribe_handlers = []
        self._frame_handlers = []
        m = plugin.Manager()
        m.provide(m.Provision('server',self,annotation='init',forcediff=True),loop=self._loop)

    def setAuthHandler(self, handler):
        if not callhlp.iscoroutinefunction(handler):
            raise AssertionError(f'{handler} must be declared async to be an auth handler')
        self._auth_handler = callhlp.make_weakref(handler)
        return handler

    def setPermHandler(self, handler):
        if callhlp.iscoroutinefunction(handler):
            raise AssertionError(f'{handler} must not be declared async to be a perm handler')
        self._perm_handler = callhlp.make_weakref(handler)
        return handler

    def setErrorHandler(self, handler):
        if callhlp.iscoroutinefunction(handler):
            raise AssertionError(f'{handler} must not be declared async to be an error handler')
        self._error_handler = callhlp.make_weakref(handler)
        return handler

    def addDisconnectHandler(self, handler):
        if callhlp.iscoroutinefunction(handler):
            raise AssertionError(f'{handler} must not be declared async to be a disconnect handler')
        wref_handler = callhlp.make_weakref(handler)
        self._disconnect_handlers.append(wref_handler)
        return handler

    def addSubscribeHandler(self, handler):
        if callhlp.iscoroutinefunction(handler):
            raise AssertionError(f'{handler} must not be declared async to be a subscribe handler')
        wref_handler = callhlp.make_weakref(handler)
        self._subscribe_handlers.append(wref_handler)
        return handler

    def addUnsubscribeHandler(self, handler):
        if callhlp.iscoroutinefunction(handler):
            raise AssertionError(f'{handler} must not be declared async to be an unsubscribe handler')
        wref_handler = callhlp.make_weakref(handler)
        self._unsubscribe_handlers.append(wref_handler)
        return handler

    def addFrameHandler(self, handler):
        if callhlp.iscoroutinefunction(handler):
            raise AssertionError(f'{handler} must not be declared async to be a frame handler')
        wref_handler = callhlp.make_weakref(handler)
        self._frame_handlers.append(wref_handler)
        return handler

    def removeFrameHandler(self, handler):
        wref_handler = callhlp.make_weakref(handler)
        frame_handlers = []
        for h in self._frame_handlers:
            if h==wref_handler:
                continue
            frame_handlers.append(wref_handler)
        self._frame_handlers = frame_handlers

    def _logFrame(self, *args):
        super()._logFrame(*args)
        for h in self._frame_handlers:
            h = h()
            try:
                if h is None:
                    raise excdef.NoLongerAvailableError('a frame handler has been deleted')
                h(*args)
            except:
                loghlp.log_exception(self._logger, title='Frame handler failed: {h!r}')

    def _initSystemCalls(self):
        sc = SystemCalls(self)
        self._syscalls = sc
        for n in dir(sc):
            if n.startswith('_'):
                continue
            a = getattr(sc,n)
            if not callable(a):
                continue
            self.expose(SYSCALL_PREFIX + n, a)
        rsc = RawSystemCalls(self)
        self._raw_syscalls = rsc
        for n in dir(rsc):
            if n.startswith('_'):
                continue
            a = getattr(rsc,n)
            if not callable(a):
                continue
            self.expose(SYSCALL_PREFIX + n, a, raw=True)

    async def _processHandShake(self, socket, url):
        h = self._auth_handler
        if h is None:
            trame_out = [FRAME_TYPE_HELLO, self._identity, None, {}]
            if socket.open:
                await self._send(socket, trame_out)
            return
        auth_kwargs = {}
        if 'authparams' in url.params:
            authparams = url.params['authparams']
            auth_kwargs['authparams'] = self._decodeAuthParams(authparams)
        else:
            auth_kwargs['authparams'] = {}
        auth_kwargs['socket'] = socket
        error_fields = {}
        returned_info = {}
        triplet = None,None,None
        h = h()
        try:
            if h is None:
                raise excdef.NoLongerAvailableError('auth handler has been deleted')
            info = await h(**auth_kwargs)
            if info is None:
                raise excdef.AuthError('authentification failed')
            elif isinstance(info,str):
                raise excdef.AuthError(info)
            elif isinstance(info,dict):
                for k,v in info.items():
                    if k.startswith('_'):
                        continue
                    returned_info[k] = v
                set_stamp(socket, 'authinfo', info)
            else:
                raise AssertionError('authentification handler returns %r' % info)
        except excdef.AuthError:
            triplet = sys.exc_info()
            name, msg = excfmt.split(triplet=triplet)
            error_fields['name'] = name
            error_fields['msg'] = msg
            raise
        except:
            triplet = sys.exc_info()
            loghlp.log_exception(self._logger, triplet=triplet, title='Internal error in authentification handler')
            error_fields['name'] = 'olfactory.excdef.AuthError'
            error_fields['msg'] = 'internal error'
            raise excdef.AuthError(error_fields['msg'])
        finally:
            self._callErrorHandler(error_fields,*triplet)
            returned_info = self._encodeAuthParams(returned_info)
            trame_out = [FRAME_TYPE_HELLO, self._identity, returned_info, error_fields]
            await self._send(socket, trame_out)

    def _callErrorHandler(self,error_fields,*triplet):
        if len(error_fields)==0:
            return
        h = self._error_handler
        if h is None:
            return
        h = h()
        try:
            if h is None:
                raise excdef.NoLongerAvailableError('error handler has been deleted')
            h(error_fields,*triplet)
        except:
            loghlp.log_exception(self._logger, title='Error handler failed')

    async def _processClient(self, socket, url_text):
        m = decorators.Manager()
        url = urlhlp.URL(url_text)
        params = url.params
        peer_identity = '?'
        if 'identity' in params:
            peer_identity = params['identity']
            set_stamp(socket, 'identity', peer_identity)
        set_stamp(socket, 'async_tasks', {})
        set_stamp(socket, 'handshake')
        self._logger.info("x [{}] connected".format(peer_identity))
        while True:
            try:
                if has_stamp(socket, 'handshake'):
                    await self._processHandShake(socket,url)
                    del_stamp(socket, 'handshake')
                await self._processFrame(socket,url)
            except websockets.exceptions.ConnectionClosed:
                break
            except excdef.AuthError:
                self._logger.info("x [{}] bad authentification".format(peer_identity))
                break
            except asyncio.CancelledError:
                break
            except:
                loghlp.log_exception(self._logger, title='Server._processClient() failed')
                break
        async_tasks = get_stamp(socket, 'async_tasks')
        for fut in async_tasks.values():
            fut.cancel()
        self._logger.info("x [{}] disconnected".format(peer_identity))
        dis_kwargs = {}
        dis_kwargs['authinfo'] = get_stamp(socket,'authinfo',{})
        dis_kwargs['socket'] = socket
        for h in self._disconnect_handlers:
            h = h()
            try:
                if h is None:
                    raise excdef.NoLongerAvailableError('a disconnect handler has been deleted')
                h(**dis_kwargs)
            except:
                loghlp.log_exception(self._logger, title='Disconnect handler failed: {h!r}')

    async def _processFrame(self, socket, url):
        frame = await self._recv(socket)
        if frame[0] == FRAME_TYPE_CALL:
            await self._processXReq(socket,url,frame, send_reply=True)
        elif frame[0] == FRAME_TYPE_PUSH:
            await self._processXReq(socket,url,frame, send_reply=False)
        elif frame[0] == FRAME_TYPE_ABORT:
            await self._processAbort(socket,url,frame)
        else:
            raise AssertionError("Invalid frame type: {}".format(frame[0]))

    async def _processAbort(self, socket, url, frame_in):
        _,requestid = frame_in
        async_tasks = get_stamp(socket, 'async_tasks')
        try:
            fut = async_tasks.pop(requestid)
        except KeyError:
            return
        fut.cancel()

    async def _processXReq(self, socket, url, frame_in, *, send_reply):
        frmtype,_,label,funcname,_,_ = frame_in
        async_tasks = get_stamp(socket, 'async_tasks')
        h = self._perm_handler
        perm_granted=True
        if h is not None:
            kwargs = {}
            kwargs['authinfo'] = get_stamp(socket,'authinfo',{})
            kwargs['socket'] = socket
            kwargs['frmtype'] = FRAME_TYPE_NAMES[frmtype]
            kwargs['label'] = label
            kwargs['name'] = funcname
            perm_granted = False
            perm_msg = 'permission denied'
            h = h()
            try:
                if h is None:
                    raise excdef.NoLongerAvailableError('perm handler has been deleted')
                perm_granted = h(**kwargs)
            except excdef.PermError:
                _, perm_msg = excfmt.split()
            except:
                perm_msg = 'internal error'
                loghlp.log_exception(self._logger, title='internal error in permission handler')
        if not perm_granted:
            exp = Failure(self, excdef.PermError, perm_msg, log_exception=False)
        elif label==None:
            try:
                exp = self._exposures[funcname]
            except KeyError:
                exp = Failure(self, excdef.NameError, "No function named '{}'".format(funcname))
        else:
            obj = self._getInstanceFromLabel(label)
            if isinstance(obj,ServerSideIRef):
                exp = Failure(self, excdef.LabelError, "No instance referenced as '{}'".format(label))
            else:
                exp = Failure(self, excdef.NameError, "No method named '{}'".format(funcname))
                if hasattr(obj,funcname):
                    m = decorators.Manager()
                    bf = getattr(obj,funcname)
                    uf = bf.__func__
                    d = m.getDecoration(uf, 'ocp_method')
                    if d is not None:
                        exp = Exposure(self, funcname, bf)
        await exp(frame_in, socket=socket, send_reply=send_reply)

    def addPrologue(self, topic, callable, **extra_kwargs):
        """Add a prologue which reacts on clients subscription on the specified topic.

        The object ``callable`` is called every time a matching ``SUBSCRIBE``  frame is
        received. The call is done with the client socket as first argument.

        :param topic: must be a string corresponding to the topic name. Note
            that shell-style wild-cards pattern does not operate here.

        :param callable: object to be call

        :param extra_kwargs: extra keyword arguments passed to ``callable``

        :return: an :py:class:`olfactory.ocp.server.Prologue` instance.
        """
        pro = Prologue(topic=topic,callable=callable,args=(),kwargs=extra_kwargs)
        self._prologues.append(pro)
        return pro

    def removePrologue(self, prologue):
        self._prologues.remove(prologue)

    def _getPrologues(self):
        return self._prologues

    async def _subscribe(self, *, frame, socket):
        _,requestid,_,_,args,kwargs = frame
        pattern, = args
        kwargs = {}
        kwargs['socket'] = socket
        kwargs['pattern'] = pattern
        kwargs['requestid'] = requestid
        for h in self._subscribe_handlers:
            h = h()
            try:
                if h is None:
                    raise excdef.NoLongerAvailableError('a subscribe handler has been deleted')
                h(**kwargs)
            except:
                loghlp.log_exception(self._logger, title='Subscribe handler failed: {h!r}')
        sus = Subscription(self,socket=socket,requestid=requestid,pattern=pattern)
        key = id(socket),requestid
        self._subscriptions[key] = sus
        await sus.processPrologues()
        return requestid

    def _unsubscribe(self, *, frame, socket):
        _,_,_,_,args,kwargs = frame
        requestid, = args
        key = id(socket),requestid
        try:
            sus = self._subscriptions[key]
        except KeyError:
            return False
        kwargs = {}
        kwargs['socket'] = socket
        kwargs['pattern'] = sus.pattern
        kwargs['requestid'] = sus.requestid
        for h in self._unsubscribe_handlers:
            h = h()
            try:
                if h is None:
                    raise excdef.NoLongerAvailableError('a subscribe handler has been deleted')
                h(**kwargs)
            except:
                loghlp.log_exception(self._logger, title='Unsubscribe handler failed: {h!r}')
        del self._subscriptions[key]
        return True

    async def _publish(self, p):
        sockets = set()
        if p.socket is None:
            for key,sus in list(self._subscriptions.items()):
                if sus.socket.open:
                    if sus.match(p.topic):
                        sockets.add(sus.socket)
                else:
                    del self._subscriptions[key]
        elif p.socket.open:
            sockets.add(p.socket)
        frame_out = [ FRAME_TYPE_PUBLISH, p.topic, p.args, p.kwargs ]
        for sk in sockets:
            try:
                await self._send(sk,frame_out)
            except websockets.exceptions.ConnectionClosed:
                pass

    async def listen(self):
        """Start listening and process client requests.

        This call must be done before any others.

        Usage in ``python3``:

        .. code:: python3

            await srv.listen()
        """
        if self._server is not None:
            raise excdef.StateError('Already listening')
        url = self._url
        if url.scheme != 'ws':
            raise AssertionError('Invalid scheme: {}://'.format(url.scheme))
        hostname = url.hostname
        if url.hostname=='*':
            hostname=''
        if url.port == None:
            raise AssertionError('Port not specified')
        fut = websockets.serve(self._processClient, hostname, url.port)
        self._server = await fut
        loop = self._loop
        fut = asyncio.ensure_future(self._publishing())
        fut.add_done_callback(self._publishingDone)
        self._pub_fut = fut
        m = plugin.Manager()
        m.provide(m.Provision('server', self, annotation='listen',forcediff=True),loop=loop)

    async def close(self):
        """Stop server processing.
        """
        if self._server==None:
            raise excdef.StateError('not listening')
        m = plugin.Manager()
        loop = self._loop
        exc = None
        try:
            m.provide(m.Provision('server', self, annotation='close',forcediff=True),loop=loop)
        except:
            raise
        finally:
            self._pub_queue.put_nowait(None)
            self._server.close()
            await self._server.wait_closed()
            await self._pub_fut
            self._server = None

    async def publish(self, topic, *args, **kwargs):
        """Publish on topic ``topic``.

        All the clients which have subscribed to the topic ``topic`` will receive the publication.

        :param topic: must be a string corresponding to the topic name. Note
            that shell-style wild-cards pattern does not operate here.

        :param args: positional arguments included in the publication

        :param kwargs: keyword arguments included in the publication
        """
        p = Publication(topic=topic,args=args,kwargs=kwargs)
        await self._publish(p)

    async def publishOnSocket(self, topic, socket, *args, **kwargs):
        """Publish on topic ``topic`` on the specified socket.

        Only the client connected to the socket ``socket`` and which has also
        subscribed to the topic will receive the publication.

        :param topic: must be a string corresponding to the topic name. Note
            that shell-style wild-cards pattern does not operate here.

        :param args: positional arguments included in the publication.

        :param kwargs: keyword arguments included in the publication.
        """
        p = Publication(topic=topic, socket=socket, args=args, kwargs=kwargs)
        await self._publish(p)

    def publishSoon(self, topic, *args, **kwargs):
        """Publish on topic ``topic`` without waiting.

        Parameters are the same than :py:meth:`publish` method.
        """
        p = Publication(topic=topic,args=args,kwargs=kwargs)
        self._pub_queue.put_nowait(p)

    def publishSoonOnSocket(self, topic, socket, *args, **kwargs):
        """Publish on topic ``topic`` on the specified socket without waiting.

        Parameters are the same than :py:meth:`publishOnSocket` method.
        """
        p = Publication(topic=topic, socket=socket, args=args, kwargs=kwargs)
        self._pub_queue.put_nowait(p)

    async def _publishing(self):
        while True:
            p = await self._pub_queue.get()
            if p==None:
                return
            await self._publish(p)

    def _publishingDone(self,fut):
        try:
            fut.result()
        except:
            loghlp.log_exception(self._logger, title='_publishing() failed !!!')

    def expose(self, funcname, callable, *, force_async=None, raw=False):
        """Expose to clients the callable ``callable`` as name ``funcname``.

        The object ``callable`` is called every time a matching ``CALL`` or ``PUSH`` frame is
        received. The call is done with the positional arguments and keyword arguments included
        in the request.

        :param funcname: string used on client side to identify the callable.

        :param callable: object to be called. If is an async function the call is scheduled
            soon in event loop. If is a class its constructor is called. Note that the exposed
            class must be decorated with :py:func:`olfactory.decorators.ocp_class`.

        :param raw: If ``True`` the frame object is used in the call instead of the positional
            arguments and keyword arguments.

        :return: an :py:class:`olfactory.ocp.server.Exposure` instance.
        """
        exp = Exposure(self, funcname, callable, force_async=force_async, raw=raw)
        self._exposures[funcname] = exp
        return exp

    def unexpose(self, exposure):
        """Remove an exposure made previously by :py:meth:`expose`.

        :param funcname: :py:class:`olfactory.ocp.server.Exposure` instance returned by
            :py:meth:`expose`.
        """
        self._exposures.remove(exposure)

    def _getInstanceFromLabel(self, label):
        if label in self._lbl_to_obj:
            return self._lbl_to_obj[label]
        else:
            return InvalidServerSideIRef(label)

    def putLabel(self, obj, label):
        """Give a label to an instance. Label is used internally by the
        protocol to identify an instance on the network. Note that several
        labels on the same instance can be made.

        :param obj: instance to labelize.

        :param label: the label (must be a string).
        """
        self._lbl_to_obj[label] = obj

    def removeLabel(self, obj, label):
        """Remove label previously added on instancce with :py:meth:`putLabel`.

        :param obj: instance.
        """
        if self._lbl_to_obj[label]==obj:
            del self._lbl_to_obj[label]

    def setMasterLabel(self, obj, label):
        """Set the master label for an instance. Addionally to the standard labeling made by
        :meth:`putLabel`, master labeling ensure that the specified label will be used for instances that will be
        returned from server in ``REPLY`` frame.

        :param obj: instance.

        :param label: the label (must be a string)
        """
        self._obj_to_lbl[obj] = label
        self.putLabel(obj, label)

    def resetMasterLabel(self, obj):
        """Reset master labeling made previously on the instance by :py:meth:`setMasterLabel`.

        :param obj: instance.
        """
        if obj not in self._obj_to_lbl:
            raise AssertionError('master label instance is not set')
        label = self._obj_to_lbl.pop(obj)
        self.removeLabel(obj, label)

    def _makeDefaultLabel(self,obj):
        x = id(obj)
        label = hex(x)
        return label

    def _getMasterLabel(self, obj):
        if obj not in self._obj_to_lbl:
            label = self._makeDefaultLabel(obj)
            self.setMasterLabel(obj,label)
        return self._obj_to_lbl[obj]

    def _getAllInstances(self):
        instances = set()
        for obj in self._obj_to_lbl.keys():
            instances.add(obj)
        return list(instances)

    def _getAllMasterLabels(self):
        labels = []
        for lbl in self._obj_to_lbl.values():
            labels.append(lbl)
        labels.sort()
        return labels

    def _getAllLabels(self):
        labels = []
        for lbl in self._lbl_to_obj.keys():
            labels.append(lbl)
        labels.sort()
        return labels

    def _packFrameCallback(self, obj, *, socket):
        if isinstance(obj,ServerSideIRef):
            label = obj.getLabel()
        else:
            if isinstance(obj,weakref.ref):
                obj = obj()
            found = False
            m = decorators.Manager()
            for dec in m.getDecorations('ocp_class'):
                klass = dec.getItem()
                if issubclass(obj.__class__, klass):
                    found = True
            if not found:
                return super()._packFrameCallback(obj,socket=socket)
            label = self._getMasterLabel(obj)
        data = label.encode('utf-8')
        return msgpack.ExtType(IREF_MSGPACK_CODE, data)

    def _unpackFrameCallback(self, code, data, *, socket):
        if code != IREF_MSGPACK_CODE:
            return super()._unpackFrameCallback(obj,socket=socket)
        label = data.decode('utf-8')
        return self._getInstanceFromLabel(label)
