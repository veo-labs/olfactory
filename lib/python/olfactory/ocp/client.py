from olfactory.runtime import *

ensure_runtime('cpython', 'transcrypt')

# __pragma__ ('skip')
import logging
import asyncio
import websockets
import websockets.exceptions
import msgpack
import sys
import functools
from olfactory import loghlp
from olfactory.transcrypt.stubs import __pragma__
# __pragma__ ('noskip')

from olfactory import excdef, oregexp, urlhlp
from olfactory.ocp.common import *
from olfactory.stamp import *

__pragma__ ('js', '{}',
'''
import * as websockets from './olfactory.transcrypt.websockets.js';
import * as asyncio from './olfactory.transcrypt.asyncio.js';
import * as logging from './olfactory.transcrypt.logging.js';
import * as helper from './olfactory.transcrypt.helper.js';
''')

logger = logging.getLogger(__name__)


# ------------------------------------------------------------------------------
# Frame types
# ------------------------------------------------------------------------------

_std_python_exceptions = [
    AssertionError,
    AttributeError,
    IndexError,
    KeyError,
    NotImplementedError,
    SyntaxError,
    TypeError,
    ValueError,
]

if get_runtime()=='cpython':
    _std_python_exceptions += [
        FloatingPointError,
        MemoryError,
        NameError,
        OSError,
        OverflowError,
        RecursionError,
        RuntimeError,
        UnboundLocalError,
        ZeroDivisionError,
        IOError,
        BlockingIOError,
        ChildProcessError,
        ConnectionError,
        BrokenPipeError,
        ConnectionAbortedError,
        ConnectionRefusedError,
        ConnectionResetError,
        FileExistsError,
        FileNotFoundError,
        InterruptedError,
        TimeoutError
     ]

_std_python_errors = {}
for exc in _std_python_exceptions:
    _std_python_errors[exc.__name__] = exc
_std_python_errors['concurrent.futures._base.CancelledError'] = asyncio.CancelledError


_custom_exceptions = {}
_custom_exceptions_computed = False


# ------------------------------------------------------------------------------
# IRef
# ------------------------------------------------------------------------------

# __pragma__ ('skip')
class IRef:
    """An handle used on client side which references an instance located
    on the server.

    :param label: a string which is used internally by the protocol to identify
        the instance
    """
    def __init__(self, label):
        self._label = label

    def __repr__(self):
        return f"<IRef label={self._label}>"

    def __hash__(self):
        return hash(self.getLabel())

    def getLabel(self):
        """Label getter.

        :return: the label
        """
        return self._label

    def __eq__(self,other):
        if not isinstance(other,self.__class__):
            return False
        else:
            return self.getLabel()==other.getLabel()


_IRefClass = IRef
# __pragma__ ('noskip')


__pragma__ ('js', '{}',
'''
export function _IRefClass(label) {
  this._label = label;
}
_IRefClass.prototype.getLabel = function() { return this._label; }

export var IRef = function (label) {
    return new _IRefClass (label);
};
''')

# ------------------------------------------------------------------------------
# IRefCodec
# ------------------------------------------------------------------------------

class IRefCodec:
    code = IREF_MSGPACK_CODE
    Class = _IRefClass

    def pack(self, iref):
        label = iref.getLabel()
        if get_runtime()=='transcrypt':
            data = __new__(TextEncoder("utf-8")).encode(label);
        else:
            data = label.encode('utf-8')
        return data

    def unpack(self, data):
        if get_runtime()=='transcrypt':
            label = __new__(TextDecoder("utf-8")).decode(data);
        else:
            label = data.decode('utf-8')
        iref = IRef(label)
        return iref


# ------------------------------------------------------------------------------
# WIDAllocator
# ------------------------------------------------------------------------------

class WIDAllocator:
    def __init__(self):
        self._d = {}

    def get(self, prefix):
        if prefix not in self._d:
            self._d[prefix] = 0
        rval = self._d[prefix]
        self._d[prefix] += 1
        return rval


# ------------------------------------------------------------------------------
# Waiter
# ------------------------------------------------------------------------------

class Waiter(asyncio.Future):
    def __init__(self, client, prefix):
        wid = client._wid_allocator.get(prefix)
        key = "{}{}".format(prefix,wid)
        if key in client._waiters:
            raise AssertionError("There is already a waiter indexed as {}".format(key))
        super().__init__()
        client._waiters[key] = self
        self.__client = client
        self.__key = key
        self.__wid = wid
        self.__prefix = prefix

    def getWID(self):
        return self.__wid

    def getClient(self):
        return self.__client

    def getKey(self):
        return self.__key

    def getPrefix(self):
        return self.__prefix


# ------------------------------------------------------------------------------
# REQWaiter
# ------------------------------------------------------------------------------

class REQWaiter(Waiter):
    def __init__(self, client):
        super().__init__(client, 'REQ')

    def cancel(self):
        key = self.getKey()
        client = self.getClient()
        q = client._abort_queue
        q.put_nowait(self.getWID())
        ret = super().cancel()
        return ret


# ------------------------------------------------------------------------------
# translate_error
# ------------------------------------------------------------------------------

def translate_error(error_fields,**kwargs):
    if get_runtime()=='transcrypt':
        kwargs, = helper.extract_arguments(js_arguments, ['**'])
        kwargs = dict(kwargs)
    global _custom_exceptions_computed
    if len(error_fields) == 0:
        return
    name = None
    msg = None
    if 'name' in error_fields:
        name = error_fields['name']
    if 'msg' in error_fields:
        msg = error_fields['msg']
    if get_runtime()=='transcrypt':
        error_fields = dict(error_fields)
    elif not _custom_exceptions_computed:
        m = decorators.Manager()
        d = _custom_exceptions
        for dec in m.getDecorations('ocp_exception'):
            klass = dec.getItem()
            n = f'{klass.__module__}.{klass.__name__}'
            d[n] = klass
        _custom_exceptions_computed = True
    if name in _std_python_errors:
        klass = _std_python_errors[name]
    elif name != None  and  name.startswith('olfactory.excdef.'):
        klass = excdef._by_names[name]
    elif name in _custom_exceptions:
        klass = _custom_exceptions[name]
    else:
        klass = excdef.UnexpectedError
    if msg is not None:
        exc = klass(msg)
    else:
        exc = klass()
    d = {}
    for k,v in error_fields.items():
        d[k] = v
    for k,v in kwargs.items():
        d[k] = v
    exc.olfactory_fields = d
    raise exc


# ------------------------------------------------------------------------------
# Client
# ------------------------------------------------------------------------------

class Client(Node):
    """Client for ``ocp`` protocol.

    :param url: url of the server. The following format is currently
        supported: ``ws://username:password@host:port``.

    :param loop: ``asyncio`` loop. If ``None``, ``asyncio.get_event_loop()``
        is used.

    :param identity: string used to identify this client in logs on client
        and server side.

    :param authparams: authentification parameters sent to the server during the connection
        phase. On server side they will be passed to the authentification handler (if
        defined). See :py:func:`olfactory.decorators.ocp_authhandler`
        for more details.

    **Usage in** ``python3``

    .. code:: python3

        from olfactory.ocp import client
        (...)
        cl = client.Client('ws://localhost:9087',identity='zorg')

    **Usage in** ``JavaScript``

    .. code:: javascript

        import {__kwargtrans__ as KW} from '/olfactory/org.transcrypt.__runtime__.js';
        import * as client from '/olfactory/olfactory.ocp.client.js';
        (...)
        var cl = client.Client('ws://localhost:9087', KW({identity: 'zorg'})));
    """
    def __init__(self,url=None,*,loop=None,identity='cli', authparams={}):
        if get_runtime()=='transcrypt':
            loop,identity,authparams = helper.extract_arguments(js_arguments,
                [('loop', loop), ('identity', identity), ('authparams', authparams)])
        super().__init__(loop,identity)
        self._bindings = set()
        self._waiters = {}
        self._url = url
        self._authparams = authparams
        self._socket = None
        self._bgproc_task = None
        self._bgproc_running = False
        self._wid_allocator = WIDAllocator()
        self._abort_queue = None
        self._authinfo = None
        if get_runtime()=='cpython':
            self._initCodecs()
        self.addCodec(IRefCodec())

    def _requireConnected(self):
        if not self._bgproc_running:
            raise excdef.StateError('not connected')

    def setAuthParams(self, authparams):
        self._authparams = authparams

    def _makeRealURL(self):
        if self._url is None:
            raise excdef.StateError('URL not set')
        url = urlhlp.URL(self._url)
        username=url.username
        password=url.password
        if username=='':
            username=None
        if password=='':
            password=None
        if (username is not None) and ('username' in self._authparams):
            raise excdef.StateError('username specified twice (in URL and authparams)')
        if (password is not None) and ('password' in self._authparams):
            raise excdef.StateError('password specified twice (in URL and authparams)')
        authparams = {}
        if username is not None:
            authparams['username']=username
        if password is not None:
            authparams['password']=password
        authparams.update(self._authparams)
        authparams = self._encodeAuthParams(authparams)
        authparams = urlhlp.quote_all(authparams)
        identity = urlhlp.quote(self._identity)
        real_url = "{}://{}:{}?identity={}&authparams={}".format(url.scheme, url.hostname, url.port, identity, authparams)
        return real_url

    def setURL(self, url):
        self._url = url

    async def connect(self,url=None,*,authparams=None):
        """Start connection between the client and the server.

        :param authparams: if not ``None`` set authentification parameters. See
        :py:meth:`__init__` constructor.

        This call must be done before any others.

        **Usage in** ``python3``

        .. code:: python3

            await cl.connect()

        **Usage in** ``JavaScript``

        .. code:: javascript

            await cl.connect()
        """
        if get_runtime()=='transcrypt':
            authparams, = helper.extract_arguments(js_arguments, [('authparams', authparams)])
        if self._bgproc_running:
            raise excdef.StateError('already connected')
        if url is not None:
            self.setURL(url)
        if authparams is not None:
            self.setAuthParams(authparams)
        real_url = self._makeRealURL()
        self._logger.info("will connect to {}".format(real_url))
        loop=self._loop
        socket = await websockets.connect(real_url, loop=loop)
        self._abort_queue = asyncio.Queue(loop=loop)
        frame = await self._recv(socket)
        if frame[0] == FRAME_TYPE_HELLO:
            _,identity,authinfo,error_fields = frame
            if len(error_fields) > 0:
                await socket.close()
            set_stamp(socket, 'identity', identity)
            if authinfo is not None:
                self._authinfo = self._decodeAuthParams(authinfo)
            translate_error(error_fields,serialized=True)
        else:
            await socket.close()
            raise AssertionError('HELLO frame expected')
        self._socket = socket
        self._bgproc_task = self._createTask(self._backgroundProcessing)
        self._bgproc_running = True

    def getAuthInfo(self):
        """Returns authentification information sent back by the server after handshaking as
        returned in the authenfication handler. See :py:func:`olfactory.decorators.ocp_authhandler`
        for more details.

        :return: authentification information as string dictionary.
        """
        return self._authinfo

    async def disconnect(self):
        """Stop connection between the client and the server.
        """
        self._requireConnected()
        try:
            if self._socket is not None:
                await self._socket.close()
            if self._bgproc_task:
                if get_runtime()=='cpython':
                    await self._bgproc_task
                elif get_runtime()=='transcrypt':
                    await self._bgproc_task.promise()
        finally:
            self._socket = None
            self._bgproc_task = None
            self._abort_queue = None

    async def call(self, funcname, *args, **kwargs):
        """Perform a remote call of the function exposed as ``funcname``.

        :return: the value returned by the exposed function on server side

        This is a wrapper around method :py:meth:`_request` which do the same things as:

        .. code:: python3

            return await self._request(funcname, None, args, kwargs, True, None)
        """
        if get_runtime()=='transcrypt':
            args, kwargs = helper.extract_arguments(js_arguments, ['2*', '**'])
        rval = await self._request(funcname, None, args, kwargs, True, None)
        return rval

    async def push(self, funcname, *args, **kwargs):
        """Perform a remote call of the function exposed as ``funcname``
        but don't request the returned value.

        This is a wrapper around method :py:meth:`_request` which do the same things as:

        .. code:: python3

            return await self._request(funcname, None, args, kwargs, False, None)
        """
        if get_runtime()=='transcrypt':
            args, kwargs = helper.extract_arguments(js_arguments, ['2*', '**'])
        rval = await self._request(funcname, None, args, kwargs, False, None)
        return rval

    async def callm(self, iref, funcname, *args, **kwargs):
        """Perform a remote call of the method ``funcname`` on instance ``iref``.

        :return: the value returned by the exposed method on server side

        This is a wrapper around method :py:meth:`_request` which do the same things as:

        .. code:: python3

            return await self._request(funcname, iref, args, kwargs, True, None)
        """
        if get_runtime()=='transcrypt':
            args, kwargs = helper.extract_arguments(js_arguments, ['3*', '**'])
        rval = await self._request(funcname, iref, args, kwargs, True, None)
        return rval

    async def pushm(self, iref, funcname, *args, **kwargs):
        """Perform a remote call of the method ``funcname`` on instance ``iref``
        but don't request the returned value.

        This is a wrapper around method :py:meth:`_request` which do the same things as:

        .. code:: python3

            return await self._request(funcname, iref, args, kwargs, False, None)
        """
        if get_runtime()=='transcrypt':
            args, kwargs = helper.extract_arguments(js_arguments, ['3*', '**'])
        rval = await self._request(funcname, iref, args, kwargs, False, None)
        return rval

    async def xcall(self, funcname, *, iref=None, args=[], kwargs={}, requestid_getter=None):
        """Perform a remote function or method call.

        :return: the value returned by the exposed function or method on server side

        This is a wrapper around method :py:meth:`_request` which do the same things as:

        .. code:: python3

            return await self._request(funcname, iref, args, kwargs, True, requestid_getter)
        """
        if get_runtime()=='transcrypt':
            iref, args, kwargs, requestid_getter = helper.extract_arguments(js_arguments,
                [('iref', iref), ('args', args), ('kwargs', kwargs), ('requestid_getter', requestid_getter)])
        rval = await self._request(funcname, iref, args, kwargs, True, requestid_getter)
        return rval

    async def xpush(self, funcname, *, iref=None, args=[], kwargs={}, requestid_getter=None):
        """Perform a remote function or method call but don't request the returned value.

        This is a wrapper around method :py:meth:`_request` which awaits and returns:

        .. code:: python3

            self._request(funcname, iref, args, kwargs, False, requestid_getter)
        """
        if get_runtime()=='transcrypt':
            iref, args, kwargs, requestid_getter = helper.extract_arguments(js_arguments,
                [('iref', iref), ('args', args), ('kwargs', kwargs), ('requestid_getter', requestid_getter)])
        rval = await self._request(funcname, iref, args, kwargs, False, requestid_getter)
        return rval

    async def _request(self, funcname, iref, args, kwargs, expect_reply, requestid_getter):
        """This is the base code to perform remote function or method call. Optionally
        the returned value can be requested and awaited.

        :param funcname: name of the function or the method exposed on server side

        :param iref: if ``None`` a function call is performed. Otherwise it must be
            an instance of :py:class:`IRef` and in this case a method call is performed.
            See :py:class:`IRef` for further details.

        :param args: positional arguments passed directly to the exposed function or method
            on server side

        :param kwargs: keyword arguments passed directly to the exposed function or method
          on server side

        :param expect_reply: if ``True`` request and await returned value or exception from server

        :param requestid_getter: if not ``None`` a callable must be provided. It is used
            to get the ``requestid`` for cancel a call. See :py:meth:`abort` for further details.

        :return: if ``expect_reply`` is ``True`` the value returned by the exposed function or method
            on server side, ``None`` otherwise

        :raises: if ``expect_reply`` is ``True`` the exception raised by the exposed function or method
            on server side

        A request can be canceled in two ways by using :py:meth:`abort` or by calling ``cancel()``
        on the returned future like object. In this case it works only in ``python3`` with :py:meth:`call`,
        :py:meth:`callm` and :py:meth:`xcall`.

        .. code:: python3

           fut = loop.create_task( cl.call('do_lot_of_work') )
           await asyncio.sleep(5)
           if not fut.done():
               fut.cancel()
        """
        self._requireConnected()
        if expect_reply:
            waiter = REQWaiter(self)
            requestid = waiter.getWID()
            frame_type = FRAME_TYPE_CALL
        else:
            frame_type = FRAME_TYPE_PUSH
            requestid = self._wid_allocator.get('REQ')
            waiter = None
        if requestid_getter is not None:
            requestid_getter(requestid)
        if iref:
            label = iref.getLabel()
        else:
            label = None
        frame_out = [ frame_type, requestid, label, funcname, args, kwargs ]
        socket = self._socket
        await self._send(socket,frame_out)
        if not waiter:
            return
        if get_runtime()=='cpython':
            frame_in = await waiter
        elif get_runtime()=='transcrypt':
            frame_in = await waiter.promise()
        if frame_in[0] != FRAME_TYPE_REPLY:
            raise AssertionError("Invalid frame type: " + str(frame_in[0]))
        _,_,rval,error_fields = frame_in
        translate_error(error_fields,serialized=True)
        return rval


    async def abort(self, requestid):
        """Cancel a request previously made by :py:meth:`xcall`, :py:meth:`xpush` or :py:meth:`_request`.

        :param requestid: integer identifying the request get from ``requestid_getter`` callback
            passed to :py:meth:`_request`

        **Usage in** ``python3``

        .. code:: python3

           requestid = []
           fut = loop.create_task(cl.xcall('do_lot_of_work', requestid_getter=requestid.append))
           await asyncio.sleep(2)
           if not fut.done():
               await cl.abort(requestid[0])

        **Usage in** ``JavaScript``

        .. code:: javascript

            var promise = cl.xcall('do_lot_of_work', KW({{'requestid_getter': _=>{{requestid=_;}} }}));
            await asyncio.sleep(2);
            await cl.abort(requestid);
        """
        self._requireConnected()
        frame_out = [ FRAME_TYPE_ABORT, requestid ]
        socket = self._socket
        await self._send(socket,frame_out)

    async def subscribe(self,topic):
        """Ask the server to subscribe to one or more topics

        :param topic: must be a string corresponding to the topic name. More topics can be
            specified by using a shell-style wild-cards pattern for this parameter. See example
            below and also :py:class:`olfactory.oregexp.ORegExp` for more details.

        :return: an integer corresponding to this subscription which can be used later with
            :py:meth:`unsubscribe`

        Usage in ``python3``:

        (*subscribe to topic* ``"key-pressed"``)

        .. code:: python3

           subscription_id = await cl.subscribe("key-pressed")

        (*subscribe to topics* beginning with ``"key-"``)

        .. code:: python3

            subscription_id = await cl.subscribe("key-*")
        """
        self._requireConnected()
        subscription_id = await self._request(SYSCALL_PREFIX + 'subscribe', None, [topic], {}, True, None)
        return subscription_id

    async def unsubscribe(self,subscription_id):
        """Ask the server to unsubscribe the subscription made previously
        by :py:meth:`subscribe`.

        :param subscription_id: If ``None`` all subscriptions made previously are
            removed.

        :return: the number of removed subscriptions
        """
        self._requireConnected()
        rval = await self._request(SYSCALL_PREFIX + 'unsubscribe', None, [subscription_id], {}, True, None)
        return rval

    def bind(self, topic, callback, **kwargs):
        """Bind publication sent by server and matching ``topic`` on ``callback``

        :param topic: must be a string corresponding to the topic name. More topics can be
            specified by using a shell-style wild-cards pattern for this parameter. See example
            below and also :py:class:`olfactory.oregexp.ORegExp` for more details.

        :param callable: a callable compatible with the signature of the publication

        :param extra_kwargs: extra keyword arguments passed to the callback

        :return: a :py:class:`Binding` instance which can be used later with
            :py:meth:`unbind`

        Usage in ``python3``:

        (*subscribe to topic* ``"key-pressed"``)

        .. code:: python3

            def on_key_pressed(key):
                (...)

            binding = cl.bind("key-pressed", on_key_pressed)
        """
        if get_runtime()=='transcrypt':
            kwargs, = helper.extract_arguments(js_arguments, ['**'])
        b = Binding(self._loop, topic, callback, **kwargs)
        self._bindings.add(b)
        return b

    def unbind(self, binding):
        """Unbind a binding made previously by :py:meth:`bind`.

        :param binding: :py:class:`Binding` return by :py:meth:`bind`
        """
        self._bindings.remove(binding)

    def _joinTopicAndArgs(self,topic,args):
        topic_and_args = []
        topic_and_args.append(topic)
        for a in args:
            topic_and_args.append(a)
        return topic_and_args

    async def publish(self, topic, *args, **kwargs):
        """Ask server to send a publication to all clients who subscribed
        to this ``topic``.

        This is a wrapper around method :py:meth:`xpublish`:

        .. code:: python3

            await self.xpublish(topic, args=args, kwargs=kwargs)
        """
        if get_runtime()=='transcrypt':
            args,kwargs = helper.extract_arguments(js_arguments, ['2*', '**'])
        self._requireConnected()
        topic_and_args = self._joinTopicAndArgs(topic,args)
        await self._request(SYSCALL_PREFIX + 'publish', None, topic_and_args, kwargs, False, None)

    async def xpublish(self, topic, *, args=[], kwargs={}):
        """Ask server to send a publication to all clients who subscribed
        to this ``topic``.

        :param topic: must be a string corresponding to the topic name. Note
            that shell-style wild-cards pattern does not operate here.

        :param args: positional arguments included in the publication

        :param kwargs: keyword arguments included in the publication
        """
        if get_runtime()=='transcrypt':
            args,kwargs = helper.extract_arguments(js_arguments,[('args', args), ('kwargs', kwargs)])
        self._requireConnected()
        topic_and_args = self._joinTopicAndArgs(topic,args)
        await self._request(SYSCALL_PREFIX + 'publish', None, topic_and_args, kwargs, False, None)

    async def watch(self):
        """Wait until :py:meth:`alert` or :py:meth:`alertWithException` is called from
        another task or until any network error related the client occurred.

        In case of network error the corresponding exception is raised.

        :return: the value passed to :py:meth:`alert`

        :raises: the exception passed to :py:meth:`alertWithException`
        """
        self._requireConnected()
        waiter = Waiter(self, 'WATCH')
        if get_runtime()=='cpython':
            result = await waiter
        elif get_runtime()=='transcrypt':
            result = await waiter.promise()
        return result

    def alert(self,result=None):
        """Force all tasks waiting on :py:meth:`watch` to return from with value ``result``.

        :param result: value returned by :py:meth:`watch`
        """
        self._requireConnected()
        for k in list(self._waiters.keys()):
            if not k.startswith('WATCH'):
                continue
            waiter = self._waiters.pop(k)
            waiter.set_result(result)

    def alertWithException(self,exc):
        """Force all tasks waiting on :py:meth:`watch` to raise the exception ``exc``.

        :param exc: exception raised from :py:meth:`watch`
        """
        self._requireConnected()
        for k in list(self._waiters.keys()):
            if not k.startswith('WATCH'):
                continue
            waiter = self._waiters.pop(k)
            waiter.set_exception(exc)

    def _createTask(self, async_func, *args):
        if get_runtime()=='cpython':
            return self._loop.create_task(async_func(*args))
        elif get_runtime()=='transcrypt':
            return asyncio.WrappedPromise(async_func(*args))

    async def _backgroundProcessing(self):
        r = self._createTask(self._recv, self._socket)
        q = self._createTask(self._abort_queue.get)
        while True:
            try:
                done,_ = await asyncio.wait([r, q], return_when=asyncio.FIRST_COMPLETED)
                if r in done:
                    frame = r.result()
                    r = self._createTask(self._recv, self._socket)
                    if frame[0]==FRAME_TYPE_PUBLISH:
                        for b in self._bindings:
                            b._onFrame(frame)
                    else:
                        requestid = frame[1]
                        k = "REQ{}".format(requestid)
                        try:
                            waiter = self._waiters.pop(k)
                        except KeyError:
                            self._logger.warning('Unhandled reply with requestid={}'.format(requestid))
                            continue
                        else:
                            if waiter.cancelled():
                                pass
                            else:
                                waiter.set_result(frame)
                elif q in done:
                    requestid = q.result()
                    q = self._createTask(self._abort_queue.get)
                    await self.abort(requestid)
            except:
                ev = None
                if get_runtime()=='cpython':
                    et,ev,tb = sys.exc_info()
                    if isinstance(ev, websockets.exceptions.ConnectionClosed):
                        ev = excdef.ConnectionClosed()
                    else:
                        loghlp.log_exception(self._logger, title='Client._backgroundProcessing()')
                elif get_runtime()=='transcrypt':
                    __pragma__('js', '{}', '''ev = __except0__;''')
                for w in self._waiters.values():
                    if w.cancelled() and w.getPrefix()=='REQ':
                        continue
                    w.set_exception(ev)
                self._waiters = {}
                if get_runtime()=='cpython':
                  if not q.cancelled():
                      q.cancel()
                  if not r.cancelled():
                      r.cancel()
                self._bgproc_running = False
                break


# ------------------------------------------------------------------------------
# Binding
# ------------------------------------------------------------------------------

class Binding:
    def __init__(self, loop, pattern, callback, *, raw=False, schedule=False, extra_kwargs={}):
        self._loop = loop
        self._pattern = pattern
        self._callback = callback
        self._regexp = oregexp.ORegExp(pattern)
        if get_runtime()=='transcrypt':
            raw, schedule, extra_kwargs = helper.extract_arguments(js_arguments,
                [('raw',raw), ('schedule',schedule), ('extra_kwargs', extra_kwargs)])
        self._raw = raw
        self._schedule = schedule
        self._extra_kwargs = extra_kwargs

    def _makeCall(self,args,kwargs_p):
        schedule = self._schedule
        kwargs = {}
        kwargs.update(self._extra_kwargs)
        kwargs.update(kwargs_p)
        if get_runtime()=='cpython':
            def _():
                try:
                    self._callback(*args, **kwargs)
                except:
                    loghlp.log_exception(logger, title='Binding callback failed')
            if schedule:
                self._loop.call_soon(_)
            else:
                _()
        else:
            def _():
                self._callback(*args, kwargs)
            if schedule:
                setTimeout(_)
            else:
                _()

    def _onFrame(self, frame):
        m = self._regexp.match(frame[1])
        if not m:
            return
        if self._raw:
            args=(frame,)
            kwargs = {}
        else:
            args = frame[2]
            kwargs = frame[3]
        self._makeCall(args,kwargs)
