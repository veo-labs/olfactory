from olfactory.runtime import *

ensure_runtime('cpython', 'transcrypt')

# __pragma__ ('skip')
import logging
import asyncio
import msgpack
import functools
import base64
from olfactory import excfmt, decorators
from olfactory.transcrypt.stubs import __pragma__
# __pragma__ ('noskip')

from olfactory.stamp import *
from olfactory import loghlp

__pragma__ ('js', '{}',
'''
import * as logging from './olfactory.transcrypt.logging.js';
import * as asyncio from './olfactory.transcrypt.asyncio.js';
import * as msgpack from './olfactory.transcrypt.msgpack.js';
''')


logger = logging.getLogger(__name__)


# ------------------------------------------------------------------------------
# codecs related stuff
# ------------------------------------------------------------------------------

IREF_MSGPACK_CODE = 92


# ------------------------------------------------------------------------------
# system calls stuff
# ------------------------------------------------------------------------------

SYSCALL_PREFIX      = '$'


# ------------------------------------------------------------------------------
# Frame types
# ------------------------------------------------------------------------------

FRAME_TYPE_HELLO      = 101

FRAME_TYPE_PUSH       = 201
FRAME_TYPE_CALL       = 202
FRAME_TYPE_REPLY      = 203
FRAME_TYPE_ABORT      = 204

FRAME_TYPE_PUBLISH    = 301


FRAME_TYPE_NAMES = {
    FRAME_TYPE_HELLO:   'HELLO',
    FRAME_TYPE_PUSH:    'PUSH',
    FRAME_TYPE_CALL:    'CALL',
    FRAME_TYPE_REPLY:   'REPLY',
    FRAME_TYPE_ABORT:   'ABORT',
    FRAME_TYPE_PUBLISH: 'PUBLISH'
}

LOG_NODE_NUMBER = False
NEXT_NODE_NUMBER = 0


# ------------------------------------------------------------------------------
# Node
# ------------------------------------------------------------------------------

class Node:
    def __init__(self, loop, identity):
        self._initLoop(loop)
        if get_runtime()=='cpython':
            self._codecs = []
        else:
            self._codecs = msgpack.createCodec()
        self._identity = identity
        if LOG_NODE_NUMBER:
            global NEXT_NODE_NUMBER
            identity = f'{identity}#{NEXT_NODE_NUMBER}'
            NEXT_NODE_NUMBER += 1
        self._logger = loghlp.get_sub_logger(logger,identity)

    def getIdentity(self):
        return self._identity

    def _initCodecs(self):
        m = decorators.Manager()
        for dec in m.getDecorations('ocp_codec'):
            klass = dec.getItem()
            codec = klass()
            self.addCodec(codec)

    def _initLoop(self, loop):
        if loop==None:
            loop=asyncio.get_event_loop()
        self._loop = loop

    def getLoop(self):
        return self._loop

    def addCodec(self, codec):
        if get_runtime()=='cpython':
            self._codecs.append(codec)
        else:
            self._codecs.addExtPacker(codec.code, codec.Class, codec.pack);
            self._codecs.addExtUnpacker(codec.code, codec.unpack);

    def _packFrame(self, socket, frame):
        if get_runtime()=='cpython':
            default = functools.partial(self._packFrameCallback, socket=socket)
            if not isinstance(frame,list):
                raise AssertionError("list expected")
            chunk = msgpack.packb(frame, use_bin_type=True, default=default)
        else:
            chunk = msgpack.encode(frame, self._codecs)
        return chunk

    def _encodeAuthParams(self, d):
        data = ''
        for k,v in d.items():
            if '\x00' in k:
                raise ValkeError("got unexpected key '{}'".format(k))
            if '\x00' in v:
                raise ValueError("got unexpected value '{}'".format(v))
            data += k
            data += '\x00'
            data += v
            data += '\x00'
        if len(d) > 0:
            data = data[:-1]
        return data

    def _decodeAuthParams(self, data):
        if len(data) > 0:
            l = data.split('\x00')
        else:
            l = []
        d = {}
        for i in range(0,len(l),2):
            k = l[i]
            v = l[i+1]
            d[k] = v
        return d

    def _unpackFrame(self, socket, chunk):
        if get_runtime()=='cpython':
            ext_hook = functools.partial(self._unpackFrameCallback, socket=socket)
            frame = msgpack.unpackb(chunk, use_list=True, ext_hook=ext_hook)
            if not isinstance(frame,list):
                raise AssertionError("list expected")
        else:
            frame = msgpack.decode(chunk, self._codecs)
        return frame

    def _packFrameCallback(self, obj, *, socket):
        for codec in self._codecs:
            if isinstance(obj,codec.Class):
                data = codec.pack(obj)
                return msgpack.ExtType(codec.code, data)
        raise AssertionError("_packFrameCallback() failed - while packing obj={}".format(repr(obj)))

    def _unpackFrameCallback(self, code, data, *, socket):
        for codec in self._codecs:
            if code == codec.code:
                data = codec.unpack(data)
                return data
        raise AssertionError('_unpackFrameCallback failed')

    async def _send(self, socket, frame):
        chunk = self._packFrame(socket, frame)
        self._logFrame(socket,frame, '>')
        await socket.send(chunk)

    async def _recv(self, socket):
        chunk = await socket.recv()
        frame = self._unpackFrame(socket, chunk)
        self._logFrame(socket,frame, '<')
        return frame

    def _logFrame(self, socket, frame, dir):
        peer_identity = get_stamp(socket,'identity','?')
        frame_type = frame[0]
        frame_type_name = FRAME_TYPE_NAMES[frame_type]
        f = self._logger.debug
        if frame_type==FRAME_TYPE_PUBLISH and dir=='<':
            f = self._logger.info
        f("{} [{}]  -  {} {}".format(dir,peer_identity, frame_type_name,frame))
