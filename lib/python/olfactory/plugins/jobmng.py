import asyncio
import logging
from olfactory.decorators import *
from olfactory import job

logger = logging.getLogger(__name__)

TERMINATION_TIMEOUT = 5
DESTRUCTION_TIMEOUT = 5

@plg_trigger
def _oninit(server:'init',plgmng):
    loop = server.getLoop()
    jobmng = job.Manager(server)
    plgmng.provide(plgmng.Provision('jobmng',jobmng),loop=loop)


@plg_trigger(reverse=True)
async def _onclose(server:'close', jobmng):
    m = jobmng
    if m.getNumberOfJobs() == 0:
        return

    for j in m.getAllJobs(include_null=False):
        j.disableAuto()
        if j.getStatus()=='started':
            j.terminate()

    for i in range(TERMINATION_TIMEOUT*2):
        logger.warning('waiting for jobs termination...')
        if m.getNumberOfJobs(include_null=False) == m.countJobWithStatus('stopped'):
            break
        await asyncio.sleep(0.5)

    for j in m.getAllJobs():
        if j.getStatus()=='stopped':
            j.destroy()

    for i in range(DESTRUCTION_TIMEOUT*2):
        logger.warning('waiting for jobs destruction...')
        if m.getNumberOfJobs(include_null=False)==0:
            break
        await asyncio.sleep(0.5)

    for j in m.getAllJobs(include_null=False):
        logger.error('invalid job status: %s %s' % (j.getName(),j.getStatus()))
