import logging
import asyncio

from olfactory import job, excdef
from olfactory.decorators import *

logger = logging.getLogger(__name__)


@jobext_class('sh')
class Extension(job.Extension):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @jobext_method
    async def shcmd(self, command, *, include_returncode=False, expected_returncode=0):
        job = self._job
        x0 = await job.cmd(command)
        if expected_returncode==None:
            return x0
        x1 = await job.cmd("echo $?")
        rcode = int(x1.out)
        if rcode != expected_returncode:
            raise excdef.UnexpectedReturnCodeError("Unexpected return code: %s (expected: %s)" % (rcode, expected_returncode))
        if include_returncode:
            return x0 + x1
        else:
            return x0
