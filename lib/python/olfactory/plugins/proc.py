import pty
import subprocess
import logging
import os
import asyncio
import struct
import signal

from olfactory import excdef, terminal, loghlp
from olfactory.decorators import *
from olfactory.plugins import channel

logger = logging.getLogger(__name__)


DEFAULT_NCOLS = 80
DEFAULT_NLINES = 24


@job_class('proc')
class Process(channel.FDChannel):
    def __init__(self, manager, *args, **kwargs):
        pty = bool(kwargs.get('pty', True))
        super().__init__(manager, *args, **kwargs)
        self._cmdargs = args
        self._cwd = kwargs.get('cwd', os.getcwd())
        self._env = kwargs.get('env', dict(os.environ))
        self._pass_fds = kwargs.get('pass_fds', None)
        self._expected_returncode = kwargs.get('expected_returncode', None)
        self._pty = pty
        self._popobj = None
        self._pid = None
        self._returncode = None
        self._master = None
        self._slave = None
        self._stdin_pipe = None
        self._stdout_pipe = None
        self.__logger = loghlp.get_sub_logger(logger,self._name)
        self._pending_signal = None

    def setCommand(self, cmdargs):
        self._cmdargs = cmdargs

    def _ev_DIMCH(self, ncols, nlines):
        if not self._pty:
            return
        if not self._master is None:
            terminal.set_dimension(self._master, ncols, nlines)
            self.__logger.info('PTY dimensioned to %dx%d' % (ncols,nlines))
        if not self._pid is None:
            self.sendSignal(signal.SIGWINCH)
            self.__logger.info('SIGWINCH sent to process #%d' % (self._pid))

    async def _starting(self):
        if len(self._cmdargs)==0:
            raise AssertionError('cmdargs option is empty')
        self._returncode = None
        popen_kwargs = {}
        if self._pty:
            self._master, self._slave = pty.openpty()
            popen_kwargs['stdin']=self._slave
            popen_kwargs['stdout']=self._slave
            terminal.set_dimension(self._master, self._ncols, self._nlines)
            self.__logger.debug('PTY dimensioned to %dx%d' % (self._ncols,self._nlines))
        else:
            self._stdin_pipe = os.pipe()
            self._stdout_pipe = os.pipe()
            popen_kwargs['stdin']=self._stdin_pipe[0]
            popen_kwargs['stdout']=self._stdout_pipe[1]
        popen_kwargs['stderr']=subprocess.STDOUT
        popen_kwargs['cwd']=self._cwd
        popen_kwargs['env']=self._env
        popen_kwargs['start_new_session']=True
        if self._pass_fds is not None:
            popen_kwargs['pass_fds']=self._pass_fds
        if self._pty:
            self._setTxFD(self._master)
            self._setRxFD(self._master)
        else:
            self._setTxFD(self._stdin_pipe[1])
            self._setRxFD(self._stdout_pipe[0])
        self._popobj = subprocess.Popen(self._cmdargs, **popen_kwargs)
        self._pid = self._popobj.pid
        srv = self.getManager().getServer()
        loop = self._loop
        w = asyncio.get_child_watcher()
        w.add_child_handler(self._pid, self._terminated)
        if self._pending_signal is not None:
            self.sendSignal(self._pending_signal)
            self._pending_signal = None

    def _terminated(self,pid,returncode):
        self._returncode = returncode
        loop = self._loop
        loop.call_soon_threadsafe(super().terminate)

    async def _stopping(self):
        await super()._stopping()
        w = asyncio.get_child_watcher()
        w.remove_child_handler(self._pid)
        self._unsetTxFD()
        self._unsetRxFD()
        if self._pty:
            self._robustClose(self._master)
            self._robustClose(self._slave)
        if self._expected_returncode is not None:
            if self._returncode != self._expected_returncode:
                msg = f'return code is {self._returncode} instead of expected {self._expected_returncode}'
                self._setTriplet(excdef.UnexpectedReturnCodeError(msg))

    def _getExtraInfo(self):
        d = {}
        d['pid'] = self._pid
        d['returncode'] = self._returncode
        return d

    @ocp_method
    def sendSignal(self, signame_or_signum):
        if isinstance(signame_or_signum,str):
            sig = signal.Signals[signame_or_signum]
        else:
            sig = signal.Signals(signame_or_signum)
        jobname = self.getName()
        if self._popobj is None:
            self._pending_signal = sig.value
            return
        logger.debug(f'send {sig.name} to process associated with job {jobname!r}')
        try:
            self._popobj.send_signal(sig.value)
        except ProcessLookupError:
            pass

    @ocp_method
    def terminate(self):
        self.sendSignal(signal.SIGTERM)

    @ocp_method
    def kill(self):
        self.sendSignal(signal.SIGKILL)

    @ocp_method
    def suspend(self):
        self.sendSignal(signal.SIGSTOP)

    @ocp_method
    def resume(self):
        self.sendSignal(signal.SIGCONT)


@ct_action('proc.signal')
async def _(*,cl,ns,ctapp,**ignored):
    """Send signal passed as ARGS[0] to process JOBNAME"""
    return await ctapp.doBasicAction(cl, ns, funcname='sendSignal',iref='required',args=True)


@ct_action('proc.kill')
async def _(*,cl,ns,ctapp,**ignored):
    return await ctapp.doBasicAction(cl, ns, funcname='kill',iref='required',args=False)


@ct_action('proc.suspend')
async def _(*,cl,ns,ctapp,**ignored):
    return await ctapp.doBasicAction(cl, ns, funcname='suspend',iref='required',args=False)


@ct_action('proc.resume')
async def _(*,cl,ns,ctapp,**ignored):
    return await ctapp.doBasicAction(cl, ns, funcname='resume',iref='required',args=False)
