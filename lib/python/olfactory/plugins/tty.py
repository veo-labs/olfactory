import logging
import asyncio
import string
import glob
import os

import serial as pyserial

from olfactory import excfmt
from olfactory.decorators import *
from olfactory.plugins import channel


O_FLAGS = os.O_RDWR | os.O_NOCTTY | os.O_NONBLOCK


@job_class('tty')
class TTYChannel(channel.FDChannel):
    def __init__(self, *args, path, **kwargs):
        super().__init__(*args, path, **kwargs)
        self._path = path
        self._fd = None

    async def _starting(self):
        paths = glob.glob(self._path)
        if len(paths) == 0:
            raise FileNotFoundError("No such path: %s" % self._path)
        elif len(paths) > 1:
            raise AssertionError("Pattern '%s' is matching more than one path" % self._path)
        self._openDevice(paths[0])

    def _openDevice(self, path):
        fd = os.open(path, O_FLAGS)
        self._setTxFD(fd)
        self._setRxFD(fd)
        self._fd = fd

    async def _stopping(self):
        await super()._stopping()
        self._unsetTxFD()
        self._unsetRxFD()
        self._robustClose(self._fd)


@job_class('serial')
class SerialChannel(TTYChannel):
    def __init__(self, *args, path, baudrate=115200, **kwargs):
        super().__init__(*args, path=path, baudrate=baudrate, **kwargs)
        self._baudrate = baudrate

    def _openDevice(self, path):
        serobj = pyserial.Serial(port=path, baudrate=self._baudrate)
        fd = os.open(path, O_FLAGS)
        serobj.close()
        self._setTxFD(fd)
        self._setRxFD(fd)
        self._fd = fd
