import logging
import asyncio
import time

from olfactory import job
from olfactory.decorators import *
from olfactory.plugins import channel

logger = logging.getLogger(__name__)


@job_class('testjob-empty')
class Empty(job.Job):
    async def _starting(self):
        logger.info("Hello from _starting()")
        await asyncio.sleep(1.5)
        logger.info("Hello from _starting() again after sleep")

    async def _stopping(self):
        logger.info("Hello from _stopping()")
        await asyncio.sleep(1.5)
        logger.info("Hello from _stopping() again after sleep")


@job_class('testjob-clock')
class Clock(channel.Channel):
    async def _starting(self):
        self._postEvent('TICK')

    def _ev_TICK(self):
        ln = "%s\r\n" % time.time()
        self.rx(ln)
        self._postEventLater(1,'TICK')
