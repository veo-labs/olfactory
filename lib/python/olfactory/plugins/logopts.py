from olfactory.decorators import *
from olfactory import loghlp


@plg_trigger
def _tune(argparser:'tune'):
    loghlp.tune_argparser(argparser)
    opts = loghlp.Options()
    opts.clearRules()
    opts.setLevel('info')
    opts.setDefaultDecision(loghlp.DECISION_DROP)
    opts.addAcceptRule('olfactory.*')
