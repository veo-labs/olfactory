import socket
from olfactory.plugins import channel

from olfactory.decorators import *

@job_class('udprdr')
class UDPReaderChannel(channel.FDChannel):
    def __init__(self, *args, addr='', port, blocksize=4096, **kwargs):
        super().__init__(*args, **kwargs)
        self._addr = addr
        self._port = int(port)
        self._sk = None
        self._blocksize = blocksize

    async def _starting(self):
        sk = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sk.bind((self._addr,self._port))
        self._sk = sk
        self._setRxFD(sk.fileno(), blocksize=self._blocksize)

    async def _stopping(self):
        self._sk.close()
        self._sk = None
        self._unsetRxFD()

    def _readRxFD(self):
        data,addr = self._sk.recvfrom(self._rxbs)
        self._postEvent("RX", data)
