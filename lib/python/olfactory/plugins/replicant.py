import logging
import asyncio
import string
import re
import shlex

from olfactory import excfmt,loghlp
from olfactory.decorators import *
from olfactory.plugins import channel

logger = logging.getLogger(__name__)

GREETS_MESSAGE=[
    'Replicant is a dummy read-eval-print-loop system for testing purpose.',
    ]


ASCIIART = """
                                         .        .  .   .  .     .


                                      .      .
                            . ....................
                         ............~~~::++:::~~~..
     ...             ........~............~::+=++:~~~~.
   .. .....         .......~.................~:++++++:~... .
  ....... .       .....~.~.....~.~~.~~~........~++:~~.....
   ......... .  . ....~~~~.~........~........~~:++:........
   .......  .........~~~~~:~~... .  ⨂.  ....~:+==:........~.
   ............ ......~~::++::~..........~:::++o=:...~.....
   ...~~...~~.........~~::++==+:~~....~:++++:++=o=~~.. ⨂.
    ...................~~::++==oo=++=+=o=++++++=o=:..... .
    ..................~~~~:+++==ooooooooo=+++++=oo+:~.....
      ...~~............~~~::::+=ooooooooo=::++=+===++::::.
        ..~~.........~.~~:~:::++==ooooo==::::++++===+=++:.
          ............~~~~::::+++==ooo===~~~....~:~::++:~.
          . .......~~~~~~~~~::+++=====oo=+:~~~.....~+++:.
          .. .....~.~~~~~:~:::++++==o=ooooo==+:~~.~+++:~.
          ... ......~~~~~~:~:::+++============+++:+++::.
           .........~~~~::~:::+++=++:::::~.....~~~:++:~.
          ...........~~~~~::::++++++:~............~:~~.
          .............~~~~::::+:+++++:::~~~..~~...~~.
          .............~~~~~~~::+:++++:::~~......~:~~
          ................~~~~::++++++==++=+++~~~~~~
         .....................~~::+====ooooo==++:~..
         .....~.................~~::+++====++++::..
        .....~~~~~..................~~~~:::~:::~.
        ....~.~~~~~~...........................
 . . . ......~~~~~~~~.....................
  . .......~~~~~:~:~~~.~.................
 ..........~~~~:::::~~~.~................... .
 ..~~~~~~~~~~~~::::~:~~~~~..........................
 .~~~~~~~~~:~:::::::~~~~~~..............~...............
 .~~~::::::::+:::::::::~~~~~.~........~~~~~~...............
..............................................................
"""


@job_class('replicant')
class Replicant(channel.Channel):
    def __init__(self,*args,**kwargs):
        super().__init__(*args, **kwargs)
        self.__logger = loghlp.get_sub_logger(logger,self._name)

    async def _starting(self):
        await super()._starting()
        self._postEvent('GREETS')
        self._cmd = ''
        self._cmd_fut = None
        self._cmd_run = False
        self._prompt = None
        self._returncode = 0

    async def _stopping(self):
        await super()._stopping()

    async def _ev_GREETS(self):
        self._prompt = self._name + '> '
        self._send("\r\n")
        for ln in GREETS_MESSAGE:
            self._send(f"{ln}\r\n")
        self._send("\r\n\r\n")
        self._sendPrompt()

    async def _ev_TX(self, data):
        if self._status != 'started':
            return
        fut = self._cmd_fut
        if fut != None:
            if data==b'\x03':
                if not fut.cancelled():
                    fut.cancel()
            elif data.startswith(b'\x1b'):
                self._send("received: %s\r\n" % repr(data))
            return
        self._cmd_run = False
        data = self._ensureString(data)
        for ch in data:
            self._recvChar(ch)
        if self._cmd_run:
            fut = asyncio.ensure_future(self._processCommand(), loop=self._loop)
            fut.add_done_callback(self._processCommandDone)
            self._cmd_fut = fut

    def _send(self,data):
        data = self._ensureBytes(data)
        self.rx(data)

    def _sendPrompt(self):
        self._send(self._prompt)

    def _processCommandDone(self,fut):
        try:
            fut.result()
        except:
            loghlp.log_exception(self.__logger, title='Replicant._processCommandDone(). THIS SHOULD NOT HAPPEN !!!')

    def _recvChar(self, ch):
        if ch=='\r' or ch=='\n':
            self._send('\r\n')
            self._cmd_run = True
        elif ch in string.printable:
            self._cmd += ch
            self._send(ch)
        elif ch=='\x03': # ctrl-C
            self._send('\r\n')
            self._cmd = ''
            self._cmd_run = True
        elif ch=='\x7f': # backspace
            if len(self._cmd)<1:
                return
            self._cmd = self._cmd[:-1]
            self._send('\x08\x1b[K')
        else:
            ch = repr(ch)[1:-1]
            self._cmd += ch
            self._send(ch)

    async def _processCommand(self):
        cmd = shlex.split(self._cmd)
        if (len(cmd) >= 2)  and  (cmd[-1].startswith('->')):
            self._returncode = int(cmd[-1][2:])
            cmd = cmd[:-1]
        if len(cmd)==0:
            f = self._emptyCommand
        else:
            try:
                f = getattr(self, "cmd_%s" % cmd[0])
            except:
                f = self._noSuchCommand
        try:
            await f(cmd)
        except asyncio.CancelledError:
            self._send("-- Cancelled --\r\n\r\n")
        except:
            text = excfmt.format_single()
            self._send("%s\r\n\r\n" % text)
        finally:
            self._cmd = ''
            self._cmd_fut = None
            if self._status!='stopped':
                self._sendPrompt()

    async def _noSuchCommand(self, cmd):
        self._send("Error: no such command '%s'\r\n\r\n" % cmd[0])

    async def _emptyCommand(self, cmd):
        pass

    async def cmd_echo(self, cmd):
        if (len(cmd) >= 2)  and  (cmd[1] == '$?'):
            self._send("%s\r\n" % self._returncode)
            self._returncode = 0
        else:
            d = '%s\r\n' % ' '.join(cmd[1:])
            self._send(d)

    async def cmd_echon(self, cmd):
        d = '%s' % ' '.join(cmd[1:])
        self._send(d)

    async def cmd_countdown(self, cmd):
        # n - count start
        try:
            n = int(cmd[1])
        except:
            n = 10
        # s - additional displayed string
        try:
            s = ' - %s' % cmd[2]
        except:
            s=''
        # d - delay between each decount
        try:
            d = float(cmd[3])
        except:
            d = 0.04
        for i in range(n-1,-1,-1):
            self._send('    %d%s\r\n' % (i,s))
            if i==0:
                self._send('\r\n')
                break
            await asyncio.sleep(d)

    async def cmd_exit(self, cmd):
        self._send("bye !!!\r\n")
        await self.stop()

    async def cmd_asciiart(self, cmd):
        text = ASCIIART.replace('\n','\r\n')
        self._send(text)

    async def cmd_demoDSR(self, cmd):
        try:
            delay = int(cmd[1])
        except:
            delay = 2
        self._send("request cursor position and wait %ds...\r\n\r\n" % delay)
        self._send(b'\x1b[6n')
        await asyncio.sleep(delay)
        self._send("\r\n")

    async def cmd_demoGOTOX(self, cmd):
        try:
            x = int(cmd[1])
        except:
            x = 60
        self._send("Go to x=%d\x1b[%dG" % (x,x))
        await asyncio.sleep(1)
        self._send("*** HELLO ***")
        await asyncio.sleep(1)
        self._send("\r\n\r\n")

    async def cmd_throw(self, cmd):
        text = cmd[1]
        raise AssertionError(text)

    async def cmd_calc(self, cmd):
        d = cmd[1]
        m = re.match('([0-9]|\+|\-|\*|\s|\(|\))+', d)
        if not m:
            raise ValueError("Unexpected chars")
        r = eval("""%s""" % d)
        self._send("%s\r\n\r\n" % r)

    async def cmd_sleep(self, cmd):
        delay = int(cmd[1])
        await asyncio.sleep(delay)

    async def cmd_setprompt(self, cmd):
        if len(cmd) < 2:
            self._prompt = self.getName()
        else:
            prompt = cmd[1]
            prompt = prompt.replace('$','\r\n')
            prompt = prompt.replace('~',' ')
            self._prompt = prompt

    @ocp_method
    def getAsciiart(self):
        return ASCIIART
