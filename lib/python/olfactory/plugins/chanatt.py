import os
import tty
import termios
import asyncio
import sys
import signal
import string
from olfactory.ocp import client
from olfactory import event, terminal, excdef
from olfactory.decorators import *

BLOCK_SIZE = 4096
TTY_RAW = True
PRINT_ESCAPE_SEQUENCE_FROM_PROCESS = False


def get_common_prefix(m):
    if len(m)==0:
        return None
    elif len(m)==1:
        return m[0]
    s1 = min(m)
    s2 = max(m)
    for i, c in enumerate(s1):
        if c != s2[i]:
            return s1[:i]
    return s1


class Switch:
    VALID_CHARS = string.ascii_letters + string.digits + string.punctuation

    def __init__(self,parent,prompt='Switch to channel: '):
        self._prompt = prompt
        self._parent = parent
        self._line = b''
        fpout = self._parent._fpout
        fpout.write("\r\n\r\n")
        self._writePrompt()
        self._parent._write_enabled = False

    def _writePrompt(self):
        t = self._parent._terminal
        white=t.fgcol('#ffffff')
        fpout = self._parent._fpout
        fpout.write(f"{white:+}{t.bold:+}{self._prompt}{t.bold:-}")
        fpout.flush()

    async def kb(self,data):
        fpout = self._parent._fpout
        t = self._parent._terminal
        white=t.fgcol('#ffffff')
        all_labels = self._parent._all_labels
        ctapp = self._parent._ctapp
        cl = self._parent._client
        if data==b'\r':
            self._parent._write_enabled = True
            label = self._line.decode().strip()
            if label=='':
                label=self._parent._label
            self._parent._switch_to_label = label
            self._parent._state='tx'
            cl.alert()
        elif data==b'\x7f': # backspace
            if len(self._line)==0:
                return
            fpout.write('\x08\x1b[K')
            fpout.flush()
            self._line = self._line[:-1]
        elif data==b'\t':
            l = []
            for lbl in all_labels:
                if lbl.startswith(self._line.decode()):
                    l.append(lbl)
            prefix = get_common_prefix(l)
            if prefix is not None:
                self._line = prefix.encode()
            if len(l)==1:
                fpout.write("\r")
            else:
                fpout.write("\r\n")
                fpout.write('    '.join(l))
                fpout.write("\r\n\r\n")
            self._writePrompt()
            fpout.write(self._line.decode())
            fpout.flush()
        elif data.decode() in self.VALID_CHARS:
            fpout.write(data.decode())
            fpout.flush()
            self._line += data


class CTAction(event.EventHandler):
    def __init__(self,cl,ns,ctapp,loop,**ignored):
        super().__init__(loop=loop)
        self._client = cl
        self._ns = ns
        self._ctapp = ctapp
        self._fdin = 0
        self._fpout = sys.stdout
        self._terminal = terminal.Terminal(sys.stdout)
        self._job = None
        self._label = None
        self._state = 'tx'
        self._all_labels = set()
        self._cmd = None
        self._switch_to_label = None
        self._write_enabled = True

    async def __call__(self,**kwargs):
        old_settings = termios.tcgetattr(self._fdin)
        if TTY_RAW:
            tty.setraw(self._fdin)
        try:
            await self._innerCall(**kwargs)
        except:
            raise
        finally:
            self._loop.remove_signal_handler(signal.SIGWINCH)
            self._finalizeEventProcessing()
            if TTY_RAW:
                termios.tcsetattr(self._fdin, termios.TCSADRAIN, old_settings)
            job = self._job
            label = self._label
            t = self._terminal
            self._fpout.write("\r\n\r\n{white:+}[{bold:+}{label}{bold:-} detached from]{white:-}\r\n".format(
                label=label,
                bold=t.bold,
                white=t.fgcol('#ffffff'))
            )

    async def _ev_KBLN(self, data):
        ln = data.decode()
        if ln.startswith('!') and ln.endswith('\n'):
            ln = ln=eval(f"""'{ln[1:-1]}'""")
            for ch in ln:
                await self._ev_KB(ch.encode())
        else:
            await self._ev_KB(data)

    async def _ev_START(self):
        job = self._job
        cl = self._client
        await cl.pushm(job, 'start')

    async def _ev_TERM(self):
        job = self._job
        cl = self._client
        await cl.pushm(job, 'terminate')

    async def _ev_DESTROY(self):
        job = self._job
        cl = self._client
        await cl.pushm(job, 'destroy')

    async def _ev_KB(self, data):
        job = self._job
        cl = self._client
        if self._state=='esc':
            self._state='tx'
            if data==b'd':
                cl.alert()
            elif data==b'f':
                self._postEvent('FIT', force=True)
            elif data==b'r':
                self._postEvent('START')
            elif data==b't':
                self._postEvent('TERM')
            elif data==b'x':
                self._postEvent('DESTROY')
            elif data==b's':
                self._cmd = Switch(self)
                self._state='cmd'
            else:
                await cl.pushm(job, 'tx', data)
        elif self._state=='tx':
            if data==b'\x01':
                self._state='esc'
            else:
                await cl.pushm(job, 'tx', data)
        elif self._state=='cmd':
            await self._cmd.kb(data)
        else:
            raise AssertionError

    async def _ev_FIT(self,*,force=False):
        job = self._job
        cl = self._client
        t = self._terminal
        ncols,nlines = t.getDimension()
        await cl.pushm(job, 'setdim', ncols, nlines, force=force)

    def _ev_PRINT(self, data):
        if not self._write_enabled:
            return
        fd = self._fpout.fileno()
        t = self._terminal
        if PRINT_ESCAPE_SEQUENCE_FROM_PROCESS and b'\x1b' in data:
            data2 = "{em:+}{data}{em:-}".format(
                data=repr(data)[2:-1],
                em=t.fgcol('#ff0000'))
            os.write(fd, data2.encode())
        # do not emit DSR on terminal which attach our channel
        data = data.replace(b'\x1b[6n',b'')
        os.write(fd,data)

    def _ev_STATUS(self, label, old_sts, new_sts):
        if new_sts=='null':
            if label in self._all_labels:
                self._all_labels.remove(label)
        else:
            self._all_labels.add(label)
        if label != self._label:
            return
        if not self._write_enabled:
            return
        t = self._terminal
        white = t.fgcol('#ffffff')
        text= f"\r\n[{t.bold:+}{label}{t.bold:-} {old_sts} >>> {new_sts}]\r\n"
        text = f"{white:+}{text}{white:-}"
        self._fpout.write(text)

    async def _innerCall(self,*,run=False):
        ns = self._ns
        loop = self._loop
        ctapp = self._ctapp
        cl = self._client
        env_label =  os.getenv('_OLFACTORY_CHANATT_LABEL', None)
        if env_label is not None:
            self._job = client.IRef(env_label)
        elif run:
            self._job = await cl.xcall('job_create', args=ns.args, kwargs=ns.kwargs)
        else:
            self._job = ctapp.getIRef(ns)
        job = self._job
        self._initializeEventProcessing()
        info = None
        self._label = job.getLabel()
        l = await cl.call('job::get_all_names')
        l = list(map(lambda x: f'job::{x}', l))
        if self._label not in l:
            t = self._terminal
            white = t.fgcol('#ffffff')
            text = f"\r\n{white:+}[{t.bold:+}{self._label}{t.bold:-} connected but currently no such job]{white:-}\r\n"
            self._fpout.write(text)
        if info is not None:
            self._label = info['master_label']
        else:
            self._label = job.getLabel()
        label = self._label
        def _(*args):
             self._postEvent('FIT')
        loop.add_signal_handler(signal.SIGWINCH, _)
        def _():
            data = os.read(self._fdin, BLOCK_SIZE)
            if TTY_RAW:
                self._postEvent('KB',data)
            else:
                self._postEvent('KBLN',data)
        loop.add_reader(self._fdin, _)
        def _(job, old_sts, new_sts):
            self._postEvent('STATUS', job, old_sts, new_sts)
        cl.bind(f'job::*<status-changed>', _)
        await cl.subscribe(f'job::*<status-changed>')
        def _(data):
            self._postEvent('PRINT',data)
        cl.bind(f'{label}<RX>', _)
        await cl.subscribe(f'{label}<RX>')
        if run:
            await cl.callm(job,'start')
        await cl.watch()
        if self._switch_to_label is not None:
            extra_env={'_OLFACTORY_CHANATT_LABEL': self._switch_to_label}
            ctapp.forceRestart(extra_env)


@ct_action('channel.attach')
async def _(**kwargs):
    """Attach current terminal to channel JOBNAME"""
    act = CTAction(**kwargs)
    await act()

@ct_action('channel.run')
async def _(**kwargs):
    """Create a new channel, start it and attach it to current terminal"""
    act = CTAction(**kwargs)
    await act(run=True)
