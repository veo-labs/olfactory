import re
import asyncio
import time

from olfactory import job
from olfactory.decorators import *

@jobext_class('qemu')
class Extension(job.Extension):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self._runsubtty = kwargs.get('runsubtty', True)

    async def _starting(self):
        self._subtty_info = {}
        self._t0 = time.monotonic()

    @jobext_method
    async def getSubTTYInfo(self):
        t1 = time.monotonic()
        dt = t1 - self._t0 + 1
        if dt > 0:
            await asyncio.sleep(dt)
        return self._subtty_info

    async def _runSubTTY(self, name, device):
        mng = self.getManager()
        await mng.runJob(klass='tty',name=name,path=device,jobexts=['repl', 'sh'])

    async def _ev_RX(self,data):
        data = data.decode()
        for ln in data.split('\n'):
            m = re.search(r'char device redirected to (\S+) \(label (\S+)\)', ln)
            if not m:
                continue
            name = '%s.%s' % (self._job.getName(), m.group(2))
            self._subtty_info[name] = m.group(1)
            if self._runsubtty:
                await self._runSubTTY(name, m.group(1))
