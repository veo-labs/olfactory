import time
import sys
import asyncio
import logging
import os

from olfactory import job, event, loghlp
from olfactory.decorators import *

logger = logging.getLogger(__name__)

DEFAULT_NCOLS=80
DEFAULT_NLINES=24


class Channel(job.Job):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._ncols = int(kwargs.get('ncols', DEFAULT_NCOLS))
        self._nlines = int(kwargs.get('nlines', DEFAULT_NLINES))
        self._post_dimension = False
        self._tee_housings = {}

    @ocp_method
    def setdim(self,ncols,nlines,*,force=False):
        changed = False
        if (ncols != self._ncols)  or  (nlines != self._nlines):
            changed = True
        if changed or force:
            self._ncols = ncols
            self._nlines = nlines
            self._postEvent('DIMCH', self._ncols, self._nlines)

    @ocp_method
    def getdim(self):
        return self._ncols, self._nlines

    def _ensureBytes(self, data):
        if isinstance(data, bytes):
            return data
        elif isinstance(data, str):
            return data.encode()
        else:
            raise TypeError("data must be bytes or str")

    def _ensureString(self, data):
        if isinstance(data, str):
            return data
        elif isinstance(data, bytes):
            return data.decode(errors="backslashreplace")
        else:
            raise TypeError("data must be bytes or str")

    @ocp_method
    def tx(self, data):
        data = self._ensureBytes(data)
        self._postEvent('TX', data)

    @ocp_method
    def rx(self, data):
        data = self._ensureBytes(data)
        self._postEvent('RX', data)

    def _makeTeeKwargs(self,kwargs_p):
        kwargs = dict(teeid=self._name,name=self._name,channel=self)
        kwargs.update(kwargs_p)
        teeid = kwargs['teeid']
        return teeid,kwargs

    @ocp_method
    def tee(self, **kwargs):
        teeid,kwargs = self._makeTeeKwargs(kwargs)
        th = TeeHousing(**kwargs)
        self._tee_housings[teeid] = th
        return teeid

    @ocp_method
    def endtee(self, **kwargs):
        teeid,kwargs = self._makeTeeKwargs(kwargs)
        th = self._tee_housings[teeid]
        th.close(**kwargs)
        del self._tee_housings[teeid]

    def _ev_RX(self,data):
        self._publishSoon('RX', data)

    def _getExtraInfo(self):
        d = super()._getExtraInfo()
        d['ncols'] = self._ncols
        d['nlines'] = self._nlines
        return d


class FDChannel(Channel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._txfd = None
        self._rxfd = None
        self._txbs = None
        self._rxbs = None

    def _ev_RX(self,data):
        self._publishSoon('RX', data)

    def _robustClose(self,fd):
        if fd is None:
            return
        try:
            os.close(fd)
        except OSError:
            pass

    def _setTxFD(self, fd, *, blocksize=1024):
        self._unsetTxFD()
        self._txfd = fd
        self._txbs = blocksize

    def _setRxFD(self, fd, *, blocksize=1024):
        self._unsetRxFD()
        self._rxfd = fd
        self._rxbs = blocksize
        loop = self._loop
        loop.add_reader(self._rxfd, self._readRxFD)

    def _readRxFD(self):
        eof = False
        try:
            data = os.read(self._rxfd, self._rxbs)
        except OSError:
            eof = True
        else:
            if len(data)==0:
                eof = True
        if eof:
            self._postEvent('WANTSTOP')
        else:
            self._postEvent("RX", data)

    def _unsetRxFD(self):
        if self._rxfd is None:
            return
        loop = self._loop
        loop.remove_reader(self._rxfd)
        self._rxfd = None

    def _unsetTxFD(self):
        if self._txfd is None:
            return
        self._txfd = None

    def _ev_TX(self,data):
        if self._txfd is None:
            return
        os.write(self._txfd, data)

    async def _stopping(self):
        await super()._stopping()


class TeeHousing:
    def __init__(self,*,channel,teeid,filename,name):
        self._fh = open(filename,'ab')
        self._filename = filename
        self._teeid = teeid
        self._channel = channel
        self._name = name
        channel._addHousing(self)
        logger.warning(f'tee file {filename!r} opened - teeid={teeid!r} / name={name!r}')
        self._write(f'\n\n\n* (re-)opened at {time.asctime()}\n')

    def close(self):
        self._fh.close()
        self._fh = None
        filename = self._filename
        teeid = self._teeid
        channel = self._channel
        name = self._name
        channel._removeHousing(self)
        logger.warning(f'tee file {filename!r} closed - teeid={teeid!r} / name={name!r}')

    def _ev_RX(self,data):
        self._write(data)

    def _write(self,data):
        if isinstance(data,str):
            data=data.encode()
        fh = self._fh
        fh.write(data)
        fh.flush()


@ct_action('channel.tx')
async def _(*,cl,ns,ctapp,**ignored):
    """Send data passed as ARGS to channel JOBNAME"""
    return await ctapp.doBasicAction(cl, ns, funcname='tx',iref='required',args=True)


@ct_action('channel.rx')
async def _(*,cl,ns,ctapp,**ignored):
    """Simulate data transmission passed as ARGS from channel JOBNAME"""
    return await ctapp.doBasicAction(cl, ns, funcname='rx',iref='required',args=True)


@ct_action('channel.tee')
async def _(*,cl,ns,ctapp,**ignored):
    """Enable Rx data saving on filesystem for channel JOBNAME"""
    return await ctapp.doBasicAction(cl, ns, funcname='tee',iref='required',args=False,kwargs=True)


@ct_action('channel.endtee')
async def _(*,cl,ns,ctapp,**ignored):
    """Disable Rx data saving on filesystem for channel JOBNAME"""
    return await ctapp.doBasicAction(cl, ns, funcname='endtee',iref='required',args=False,kwargs=True)


@ct_action('channel.setdim')
async def _(*,cl,ns,ctapp,**ignored):
    """Set virtual terminal dimension to ncols=ARGS[0] and nlines=ARGS[1] of repl channel JOBNAME"""
    return await ctapp.doBasicAction(cl, ns, funcname='setdim',iref='required',args=True)


@ct_action('channel.cat')
async def _(*,cl,ns,ctapp,**ignored):
    """Dump publications on topic TOPIC as string"""
    def _recv(frame):
        name = frame[1]
        args = frame[2]
        kwargs = frame[3]
        basemsg = 'Incompatible event: {} args={} kwargs={} - '.format(*frame[1:4])
        if (len(args) != 1):
            cl.alertWithException(AssertionError(basemsg + 'expected one positional argument'))
            return
        elif (len(kwargs) != 0):
            cl.alertWithException(AssertionError(basemsg + 'no keyword argument expected'))
            return
        datap = args[0]
        if isinstance(datap,bytes):
            data = datap.decode(errors='backslashreplace')
        elif isinstance(datap,str):
            data = datap
        else:
            cl.alertWithException(AssertionError('string or bytes expected as first positional argument'))
            return
        sys.stdout.write(data)
        sys.stdout.flush()
    if ns.topic is None:
        raise AssertionError('topic not defined')
    cl.bind(ns.topic, _recv, raw=True)
    await cl.subscribe(ns.topic)
    rval = await cl.watch()
