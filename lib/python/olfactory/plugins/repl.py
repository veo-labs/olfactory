import sys
import asyncio
import logging
import copy
import struct
import msgpack
import pyte
import re
import random
import weakref

from olfactory import job, event, terminal, excdef, loghlp
from olfactory.decorators import *

logger = logging.getLogger(__name__)

_220K = 220*1000
_10K = 10*1000

DEFAULT_NCOLS = 80
DEFAULT_NLINES = 24

TRUNCMETH_KEEPBOTTOM = 0
TRUNCMETH_REMOVEMIDDLE = 1

TRUNCATION_METHODS = {
    'keepbottom': TRUNCMETH_KEEPBOTTOM,
    'removemiddle': TRUNCMETH_REMOVEMIDDLE,
}


class Content:
    def __init__(self,tokens,*, check_truncation=True):
        self._tokens = tokens
        self._truncated = False
        self._check_truncation = check_truncation
        for k,v in tokens:
            if k=='truncated':
                self._truncated = True
                break

    def enableTruncationChecking(self):
        self._check_truncation = True

    def disableTruncationChecking(self):
        self._check_truncation = False

    def __repr__(self):
        return repr(self._tokens)

    def getTokens(self):
        return self._tokens

    def getKinds(self):
        l = []
        for k,v in self._tokens:
            l.append(k)
        return l

    def isTruncated(self):
        return self._truncated

    def __repr__(self):
        s = repr(self._tokens)
        return "<Content %s>" % s[1:-1]

    def _as(self,*,kind=None,replace_linefeed=None,separator=None,check_truncation=None):
        throw = False
        if self._truncated:
            if check_truncation is None:
                throw = self._check_truncation
            else:
                throw = check_truncation
        if throw:
            raise ValueError('content is truncated')
        s = ''
        for k,v in self._tokens:
            if (k==kind)  or  (kind is None):
                if replace_linefeed is not None:
                    v = v.replace('\n',replace_linefeed)
                s += v
        if separator is None:
            return s
        else:
            l = s.split(separator)
            return l

    @property
    def out(self): return self._as(kind='out')
    output = out

    @property
    def outlines(self): return self._as(kind='out', separator='\n')

    @property
    def text(self): return self._as()

    def __add__(self, content):
        left = self._tokens
        right = content._tokens
        idx = 0
        for i in range(3,0,-1):
            if left[-1][0] != 'prompt':
                continue
            if left[-i:]==right[:i]:
                idx=i
        self._tokens = left + right[idx:]
        self._truncated = self._truncated or content.isTruncated()
        return self

    def __radd__(self, other):
        if isinstance(other, list)  and  len(other)==0:
            pass
        else:
            raise TypeError()
        return self

    def _tbook_emit(self,tbk):
        text = ''
        previous_k = None
        for k,v in self._tokens:
            first = True
            for ln in v.split('\n'):
                if k==previous_k:
                    pass
                elif k=='prompt':
                    text += '{P}'
                elif k=='cmd':
                    text += '{C}'
                else:
                    text += '{O}'
                ln = ln.replace('{','{{')
                ln = ln.replace('}','}}')
                text += ln
                previous_k=k
                if not first:
                    text += '\n'
                first = False
        tbk.emitCode(text,lexer='olfactory-repl')


@ocp_codec
class ContentCodec:
    Class = Content
    code = 93
    MAX_LEN=0x7f

    def pack(self,content):
        tokens = []
        for k,v in content._tokens:
            tokens.append(k.encode())
            tokens.append(v.encode())
        data = b''
        for t0 in tokens:
            while True:
                t = t0[:self.MAX_LEN]
                t0 = t0[self.MAX_LEN:]
                stop = False
                if len(t0)==0:
                    data += struct.pack("B",len(t))
                    stop = True
                else:
                    data += struct.pack("B",len(t)+self.MAX_LEN+1)
                data += t
                if stop:
                    break
        return data

    def unpack(self, data):
        tokens = []
        t = b''
        while len(data)>0:
            l = data[0]
            data = data[1:]
            next = not(l & (self.MAX_LEN+1))
            l = l & self.MAX_LEN
            t += data[:l]
            data = data[l:]
            if next:
                tokens.append(t.decode())
                t=b''
        tokens2 = []
        i = 0
        while i < len(tokens):
            tokens2.append((tokens[i],tokens[i+1]))
            i += 2
        content = Content(tokens2)
        return content


class Stream(pyte.Stream):
    def __init__(self, *args, extension):
        super().__init__(*args)
        self._ext = extension


class Screen(pyte.DiffScreen):
    def __init__(self, *, extension, ncols, nlines):
        job = extension.getJob()
        pyte.DiffScreen.__init__(self,ncols,nlines)
        self._ext = extension

    def backspace(self):
        super().backspace()
        ext = self._ext
        ext.propagateToFollowers('removeChars',1)

    def cursor_to_column(self, column=None):
        ext = self._ext
        oldx = self.cursor.x
        super().cursor_to_column(column)
        newx = self.cursor.x
        dx = newx-oldx
        if dx > 0:
            ext.propagateToFollowers('addChars', ' ' * dx)
        elif dx < 0:
            ext.propagateToFollowers('removeChars', -dx)

    def draw(self, char):
        super().draw(char)
        ext = self._ext
        ext.propagateToFollowers('addChars', char)

    def tab(self):
        super().tab()
        ext = self._ext
        ext.propagateToFollowers('addChars', '\t')

    def linefeed(self):
        super().linefeed()
        ext = self._ext
        ext.propagateToFollowers('feedLine')

    def write_process_input(self, data):
        self._ext._job.tx(data)


class Follower:
    _KINDMAP = {'o': 'out', 'p': 'prompt', 't': 'truncated'}

    def __init__(self,ext,*,size=None,truncation_method='keepbottom',command=None,last_prompt_from='nowhere',logger):
        self._ext = ext
        f = last_prompt_from
        if f=='nowhere':
            self._chars = []
            self._kinds = []
        else:
            self._chars, self._kinds = f._getLastPrompt()
        self._size = size
        self._command = command
        scr = ext.getScreen()
        ext._wr_followers.append(weakref.ref(self))
        ptracker = ext.getPromptTracker()
        self._ptracker = ptracker
        self._logger = logger
        self._truncation_method = TRUNCATION_METHODS[truncation_method]

    def _getLastPrompt(self):
        s = self._kinds
        i = len(s) - 1
        idx = None
        while i >= 0:
            if i>0 and s[i]=='p' and s[i-1]=='o':
                idx = i
                break
            elif i==0 and s[i]=='p':
                break
                idx = 0
            i -= 1
        if idx is not None:
            return self._chars[idx:], self._kinds[idx:]
        else:
            return [],[]

    def addChars(self, chars):
        self._chars += list(chars)
        self._kinds += ['o'] * len(chars)
        self._truncateChars()

    def removeChars(self, num):
        self._chars = self._chars[:-num]
        self._kinds = self._kinds[:-num]
        self._truncateChars()

    def _truncateChars(self):
        s = self._size
        if s is None:
            return
        if len(self._chars) <= s:
            return
        chars = list(" \n(...)\n")
        kinds = ['t']*len(chars)
        if self._truncation_method==TRUNCMETH_KEEPBOTTOM:
            self._chars = chars + self._chars[-s:]
            self._kinds = kinds + self._kinds[-s:]
        elif self._truncation_method==TRUNCMETH_REMOVEMIDDLE:
            s = int(s/2)
            self._chars = self._chars[:s] + chars + self._chars[-s:]
            self._kinds = self._kinds[:s] + kinds + self._kinds[-s:]
        else:
            raise AssertionError()

    def feedLine(self):
        self._chars.append('\n')
        self._kinds.append('o')
        self._truncateChars()

    def markPrompt(self,value):
        l = len(value)
        end = ''.join(self._chars[-l:])
        if end != value:
            self._logger.warning('value (%r) differs from end (%r)' % (value,end))
            return
        i = 0
        while i < len(value):
            self._kinds[-i-1] = 'p'
            i += 1

    def makeContent(self):
        chars = self._chars
        kinds = self._kinds + ['$']
        tokens = []
        v = ''
        prev_prompt = False
        for i in range(len(chars)):
            v += chars[i]
            if kinds[i] == kinds[i+1]:
                continue
            k = self._KINDMAP[kinds[i]]
            if self._command is not None:
                prefix = "%s\n" % self._command
                if prev_prompt and k=='out' and v.startswith(prefix):
                    tokens += [('cmd',v[:len(prefix)])]
                    tokens += [('out',v[len(prefix):])]
                else:
                    tokens += [(k,v)]
            else:
                tokens += [(k,v)]
            prev_prompt=False
            v=''
            if k=='prompt':
                prev_prompt=True
        return Content(tokens, check_truncation=False)


class HistoricFollower(Follower):
    pass


class AwaitableFollower(Follower):
    def __init__(self, ext, **kwargs):
        super().__init__(ext,**kwargs)
        self._fut = asyncio.Future()

    async def wait(self):
        result = await self._fut
        return result

    def _setResult(self,result):
        if self._fut.done():
            return
        self._fut.set_result(result)


class SyncingFollower(AwaitableFollower):
    def __init__(self, ext, *, last_prompt_from=None, backtracking,**kwargs):
        if last_prompt_from is None:
            if backtracking:
                last_prompt_from=ext._historic
            else:
                last_prompt_from='nowhere'
        super().__init__(ext,last_prompt_from=last_prompt_from,**kwargs)
        self._backtracking = backtracking

    def markPrompt(self,value):
        super().markPrompt(value)
        content = self.makeContent()
        self._setResult(content)

    async def wait(self):
        if self._backtracking:
            if (len(self._kinds) > 0)  and  self._kinds[-1].endswith('p'):
                self._fut.cancel()
                result = self.makeContent()
                return result
        result = await super().wait()
        return result


class ExpectingFollower(AwaitableFollower):
    def __init__(self, ext, pattern, *, primitive='search', escape=False, **kwargs):
        super().__init__(ext,**kwargs)
        if escape:
            pattern = re.escape(pattern)
        self._regexp = re.compile(pattern)
        self._primitive = getattr(self._regexp, primitive)

    def addChars(self, chars):
        super().addChars(chars)
        self._check()

    def removeChars(self, num):
        super().removeChars(num)
        self._check()

    def feedLine(self):
        super().feedLine()
        self._check()

    def markPrompt(self,*ignored):
        return

    def _check(self):
        chars = ''.join(self._chars)
        m = self._primitive(chars)
        if not m:
            return
        content = self.makeContent()
        self._setResult(content)


class PromptTracker:
    STATE_INIT     = 0
    STATE_GUESS   = 1
    STATE_TRACK   = 2
    STATE_STANDBY  = 3
    STATE_RESET    = 4

    PRIMARY_DELAY  = 2.0
    USUAL_DELAY    = 0.1
    MAX_RELEVANT_NLINES = 5

    _STATE_NAMES = {}
    for k,v in list(locals().items()):
        if k.startswith('STATE_'):
            _STATE_NAMES[v] = k[6:].lower()

    def __init__(self,ext, *, logger):
        super().__init__()
        self._ext = ext
        self._th = None
        self._primary_delay = self.PRIMARY_DELAY
        self._usual_delay = self.USUAL_DELAY
        self._state = self.STATE_INIT
        self._value = None
        self._start = None
        self._end = None
        self._regexp = None
        self._revelant_nlines = None
        self._logger = logger

    def getStateName(self,state=None):
        if state==None:
            state=self._state
        try:
            return self._STATE_NAMES[state]
        except:
            return "<%d>" % state

    def getSeenPrompt(self, relevant_nlines):
        nin = relevant_nlines
        if nin is None:
            nin = self.MAX_RELEVANT_NLINES
        scr = self._ext.getScreen()
        ret = False
        lines = []
        for i in range(nin):
            ln = ''
            for ch in scr.buffer[scr.cursor.y-nin+i+1].values():
                ln += ch.data
            if i==(nin-1):
                ln = ln[:scr.cursor.x]
            lines.append(ln)
        if relevant_nlines is None:
            seen = []
            b = False
            for ln in reversed(lines):
                seen = [ln] + seen
                if ln != '':
                    b = True
                    break
            if not b:
                nout = None
            else:
                nout = len(seen)
        else:
            seen = lines
            nout = len(seen)
        seen = '\n'.join(seen)
        return nout, seen

    def onActivity(self):
        if self._th != None:
            self._th.cancel()
            self._th = None
        s = self._state
        delay=None
        if s in (self.STATE_INIT, self.STATE_GUESS, self.STATE_RESET):
            delay = self._primary_delay
            self._setState(self.STATE_GUESS, delay=delay)
        elif s in (self.STATE_STANDBY,self.STATE_TRACK):
            delay = self._usual_delay
            self._setState(self.STATE_TRACK, delay=delay)
        else:
            raise excdef.StateError("invalid REPL state: %r" % self.getStateName())

        loop = self._ext.getJob().getLoop()
        self._th = loop.call_later(delay, self.onInactivity)

    def _setState(self,state,force_log=False,**kwargs):
        if (state != self._state)  or  force_log:
            new = self.getStateName(state)
            old = self.getStateName(self._state)
            self._logger.debug("prompt tracker state changed %r >>> %r  -  %r" % (old,new,kwargs))
        self._state = state

    def makePattern(self,seen):
        pattern = "^.*(%s)$" % re.escape(seen)
        return pattern

    def onInactivity(self):
        s = self._state
        if s==self.STATE_GUESS:
            n,seen = self.getSeenPrompt(relevant_nlines=None)
            if n==None:
                self._logger.debug(f"prompt tracker is still guessing  (seen={seen!r})")
                return
            pattern = self.makePattern(seen)
            self._regexp = re.compile(pattern)
            self._value = seen
            self._relevant_nlines = n
            self._setState(self.STATE_STANDBY, nlines=self._relevant_nlines, value=self._value, pattern=pattern)
        elif s==self.STATE_TRACK:
            n,seen = self.getSeenPrompt(relevant_nlines=self._relevant_nlines)
            m = self._regexp.match(seen)
            if not m:
                self._logger.debug(f"prompt tracker is still tracking (seen={seen!r})")
                return
            self._value = seen[m.start(1):]
            self._setState(self.STATE_STANDBY, value=self._value)
        else:
            raise excdef.StateError("invalid REPL state %r" % self.getStateName())
        ext = self._ext
        ext.propagateToFollowers('markPrompt', self._value)

    def track(self, pattern, *, nlines=1):
        self._regexp = re.compile(pattern)
        self._relevant_nlines = nlines
        self._setState(self.STATE_TRACK, nlines=self._relevant_nlines, value=self._value, pattern=pattern)

    def reset(self, *, delay=None, primary_delay=None, usual_delay=None):
        s = self._state
        if self._th != None:
            self._th.cancel()
            self._th = None
        self._value = None
        if delay != None:
            self._primary_delay = delay
            self._usual_delay = delay
        if usual_delay != None:
            self._usual_delay = usual_delay
        if primary_delay != None:
            self._primary_delay = primary_delay
        self._relevant_nlines = None
        self._setState(self.STATE_RESET, force_log=True, usual_delay=self._usual_delay,
                       primary_delay=self._primary_delay)

    def isStateOneOf(self,*states):
        if self._state in states:
            return True
        else:
            return False

    def getValue(self):
        return self._value


@jobext_class('repl')
class Extension(job.Extension):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        ncols = int(kwargs.get('ncols', DEFAULT_NCOLS))
        nlines = int(kwargs.get('nlines', DEFAULT_NLINES))
        job = self._job
        self._wr_followers = []
        self._stream = Stream(extension=self)
        self._screen = Screen(extension=self, ncols=ncols, nlines=nlines)
        self._stream.attach(self._screen)
        self._logger = loghlp.get_sub_logger(logger,job.getName())
        self._ptracker = PromptTracker(self, logger=self._logger)
        self._historic = HistoricFollower(self, size=_10K, logger=self._logger)
        job._addPrologue('RX',self.__presentHistoric)

    def propagateToFollowers(self,name,*args,**kwargs):
        new_wr_followers = []
        for wr_flwr in self._wr_followers:
            flwr = wr_flwr()
            if flwr is None:
                continue
            new_wr_followers.append(wr_flwr)
            if not hasattr(flwr,name):
                continue
            f = getattr(flwr,name)
            f(*args,**kwargs)
        self._wr_followers = new_wr_followers

    def _ev_RX(self,datap):
        data = self._job._ensureString(datap)
        self._stream.feed(data)
        self._ptracker.onActivity()

    def _ev_DIMCH(self, ncols, nlines):
        scr = self._screen
        scr.resize(nlines,ncols)
        self._logger.info('screen dimensioned to %dx%d' % (ncols,nlines))

    def getScreen(self):
        return self._screen

    def getPromptTracker(self):
        return self._ptracker

    @jobext_method
    def hist(self):
        return self._historic.makeContent()

    async def __presentHistoric(self,socket):
        if self._historic == None:
            return
        x = self._historic.makeContent()
        text = x.text.replace('\n','\r\n')
        job = self._job
        await job._publishOnSocket('RX',socket,text.encode())

    @jobext_method
    async def cmd(self,command,*,size=_220K,truncation_method='keepbottom'):
        if not self._ptracker.isStateOneOf(PromptTracker.STATE_STANDBY, PromptTracker.STATE_RESET):
            raise excdef.StateError('channel %r not ready to receive commands (state: %r)' % (self._job.getName(),
                self._ptracker.getStateName()))
        self._job.tx("%s\r" % command)
        f = SyncingFollower(self,command=command,last_prompt_from=self._historic, backtracking=False, logger=self._logger,
            size=size,truncation_method=truncation_method)
        content = await f.wait()
        return content

    @jobext_method
    async def syncp(self,*, backtracking=True, size=_220K, tx=None):
        f = SyncingFollower(self,backtracking=backtracking, size=size, logger=self._logger)
        if tx is not None:
            self._job.tx(tx)
        content = await f.wait()
        return content

    @jobext_method
    async def expect(self,pattern,*,size=_220K,primitive='search',escape=False):
        f = ExpectingFollower(self, pattern, size=size, primitive=primitive, escape=escape, logger=self._logger)
        content = await f.wait()
        return content

    @jobext_method
    async def getp(self):
        return self._ptracker.getValue()

    @jobext_method
    def trackp(self, pattern, *, nlines=1):
        self._ptracker.track(pattern, nlines=nlines)

    @jobext_method
    def easytrackp(self, seen):
        pattern = self._ptracker.makePattern(seen)
        self._ptracker.track(pattern, nlines=1)

    @jobext_method
    async def resetp(self,**kwargs):
        return self._ptracker.reset(**kwargs)


CONTENT_COLORS = {
    'default': '#80ffff',
    'truncated': '#ffff00',
    'prompt': '#ff4040',
    'cmd': '#ffffff',
}

def display_content(ns,content):
    fp = sys.stdout
    t = terminal.Terminal(fp)
    fp.write("\r\n")

    for k,v in content.getTokens():
        try:
            col = CONTENT_COLORS[k]
        except:
            col = CONTENT_COLORS['default']
        em = t.fgcol(col)
        fp.write("{em:+}{}{em:-}".format(v,em=em))
    fp.write("\r\n\r\n")


@ct_action('channel.hist')
async def _(*,cl,ns,ctapp,**ignored):
    """Dump historic of repl channel JOBNAME"""
    content = await ctapp.doBasicAction(cl, ns, funcname='hist',iref='required')
    display_content(ns,content)

@ct_action('channel.syncp')
async def _(*,cl,ns,ctapp,**ignored):
    """Wait prompt on repl channel JOBNAME"""
    content = await ctapp.doBasicAction(cl, ns, funcname='syncp',iref='required',kwargs=True)
    display_content(ns,content)

@ct_action('channel.cmd')
async def _(*,cl,ns,ctapp,**ignored):
    """Send command passed in ARGS[0] to repl channel JOBNAME"""
    content = await ctapp.doBasicAction(cl, ns, funcname='cmd',iref='required',args=True,kwargs=True)
    display_content(ns,content)

@ct_action('channel.expect')
async def _(*,cl,ns,ctapp,**ignored):
    """Wait for pattern passed in ARGS[0] from repl channel JOBNAME"""
    content = await ctapp.doBasicAction(cl, ns, funcname='expect',iref='required',args=True,kwargs=True)
    display_content(ns,content)

@ct_action('channel.getp')
async def _(*,cl,ns,ctapp,**ignored):
    """Dump current prompt of repl channel JOBNAME"""
    value = await ctapp.doBasicAction(cl, ns, funcname='getp',iref='required')
    return value

@ct_action('channel.resetp')
async def _(*,cl,ns,ctapp,**ignored):
    """Reset prompt of repl channel JOBNAME"""
    await ctapp.doBasicAction(cl, ns, funcname='resetp',iref='required')
