import logging
import asyncio
import os
import base64

from olfactory import job, excdef
from olfactory.decorators import *

logger = logging.getLogger(__name__)


@jobext_class('ssh')
class Extension(job.Extension):
    def __init__(self, *args, passwd=None, **kwargs):
        super().__init__(*args, passwd=passwd, **kwargs)
        self._passwd = passwd
        self._num_passwd_call = 0
        self._max_passwd_call = 5

    async def _starting(self):
        self._num_passwd_call = 0
        self._max_passwd_call = 5
        if self._passwd==None:
            return
        job = self.getJob()
        if not 'SSH_ASKPASS' in job._env:
            job._env['SSH_ASKPASS'] = 'olfactory-ssh-askpass'
        job._env['_OLFACTORY_JOBNAME'] = job.getName()

    @jobext_method
    def getPassword(self):
        self._num_passwd_call += 1
        if self._num_passwd_call > self._max_passwd_call:
            raise AssertionError('getPassword() called more than %d times' % self._max_passwd_call)
        return self._passwd
