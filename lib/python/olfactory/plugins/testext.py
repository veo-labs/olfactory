import logging

from olfactory import job, event
from olfactory.decorators import *

logger = logging.getLogger(__name__)


@jobext_class('testext-chcase')
class CaseChanger(job.Extension):
    async def _starting(self):
        self._mode = None

    def _ev_RX(self,data):
        job = self.getJob()
        if self._mode=='U':
            data=data.upper()
        elif self._mode=='L':
            data=data.lower()
        ev = event.Event('RX',data)
        return ev

    @jobext_method
    def lower(self):
        self._mode = 'L'

    @jobext_method
    def upper(self):
        self._mode = 'U'

    @jobext_method
    def keep(self):
        self._mode = None
