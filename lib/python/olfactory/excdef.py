from olfactory.runtime import *

ensure_runtime('cpython', 'transcrypt')


_MODNAME = 'olfactory.excdef'


class BaseError(Exception):
    def __init__(self,msg=None):
        if msg is None:
            Exception.__init__(self)
        else:
            Exception.__init__(self,msg)
        d = {}
        if get_runtime()=='transcrypt':
            modname = _MODNAME
        else:
            modname = self.__class__.__module__
        d['name'] = "{}.{}".format(modname,self.__class__.__name__)
        d['msg'] = msg
        d['serialized'] = False
        self.olfactory_fields = d


class NameError(BaseError):
    pass

class UnexpectedError(BaseError):
    pass

class StateError(BaseError):
    pass

class ConnectionClosed(BaseError):
    pass

class LabelError(BaseError):
    pass

class UnexpectedReturnCodeError(BaseError):
    pass

class AuthError(BaseError):
    pass

class PermError(BaseError):
    pass

class NoLongerAvailableError(BaseError):
    pass


_errors = [
    NameError,
    UnexpectedError,
    StateError,
    ConnectionClosed,
    LabelError,
    UnexpectedReturnCodeError,
    AuthError,
    PermError,
    NoLongerAvailableError,
]

# __pragma__('skip')
__all__ = []
for e in _errors:
    __all__.append(e.__name__)
# __pragma__('noskip')

_by_names = {}
for e in _errors:
    _by_names["{}.{}".format(_MODNAME,e.__name__)] = e
