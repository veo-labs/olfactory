import asyncio
import threading
import time
import logging
import inspect
from olfactory.stamp import *
from olfactory import callhlp


TICK = 0.5


def new_event_loop():
    loop = asyncio.new_event_loop()
    def check():
        mth = threading.main_thread()
        if not mth.is_alive():
            loop.stop()
        else:
            loop.call_later(TICK, check)

    def run():
        loop.call_later(TICK, check)
        loop.run_forever()

    set_stamp(loop, 'deleg')
    asyncio.set_event_loop(loop)
    asyncio.get_child_watcher()
    th = threading.Thread(target=run)
    th.start()
    return loop


def get_event_loop():
    loop = asyncio.get_event_loop()
    if has_stamp(loop, 'deleg'):
        return loop
    else:
        if loop.is_running():
            raise AssertionError('Current loop is running')
        loop = new_event_loop()
        return loop


class Instance:
    def __init__(self, instance, *, loop=None):
        self._instance = instance
        self._attr_proxies = {}
        if loop is None:
            loop = get_event_loop()
        self._loop = loop
        for n in dir(instance):
            tattr = getattr(type(self._instance),n,None)
            iattr = getattr(self._instance,n,None)
            if n.startswith('_'):
                continue
            if callable(iattr)  and  hasattr(iattr,'__self__')  and  getattr(iattr,'__self__')==self._instance:
                m = Method(name=n, loop=loop, instance=instance)
                setattr(self, n, m)
            else:
                p = AttrProxy(name=n, instance=instance)
                self._attr_proxies[n] = p

    def __dir__(self):
        entries = set()
        entries.update(self.__dict__.keys())
        entries.update(self._attr_proxies)
        return entries

    def __getattribute__(self, name):
        attr_proxies = super().__getattribute__('_attr_proxies')
        if name in attr_proxies:
            p = attr_proxies[name]
            call = Call(blocking=True, loop=self._loop,func=p.get)
            return call()
        else:
            return super().__getattribute__(name)


class Function:
    def __init__(self, func, *, loop=None):
        if loop is None:
            loop = get_event_loop()
        self._loop = loop
        self._func = func

    def _makeCall(self, *, blocking):
        f = self._func
        call = Call(blocking=blocking, loop=self._loop, func=f)
        return call

    def __call__(self, *args, **kwargs):
        call = self._makeCall(blocking=True)
        return call(*args, **kwargs)

    def opts(self, **kwargs):
        call = self._makeCall(**kwargs)
        return call


class Method(Function):
    def __init__(self, *, loop, name, instance):
        func = getattr(instance, name)
        super().__init__(func, loop=loop)


class AttrProxy:
    def __init__(self, *, name, instance):
        self._instance = instance
        self._name = name

    def get(self):
        return getattr(self._instance,self._name)


class Call:
    def __init__(self, *, blocking, loop, func):
        self._loop = loop
        self._blocking = blocking
        self._func = func

    def __call__(self, *args, **kwargs):
        coro = callhlp.ensure_coroutine(self._func, *args, **kwargs)
        loop = self._loop
        fut = asyncio.run_coroutine_threadsafe(coro, loop)
        if self._blocking==False:
            return fut
        elif self._blocking==True:
            return fut.result()
        elif isinstance(self._blocking, int):
            t0 = time.monotonic()
            while True:
                if fut.done():
                    return fut.result()
                time.sleep(TICK)
                t1 = time.monotonic()
                dt = t1 - t0
                if dt > self._blocking:
                    break
            return fut
        else:
            raise ValueError('_blocking=%r' % self._blocking)
