import sys
import types
import copy
import os
import termios
import string
import struct
import collections

from fcntl import ioctl
from termios import TIOCGWINSZ, TIOCSWINSZ


def set_dimension(fd,sx,sy):
    b = struct.pack('hhhh', sy, sx, 0, 0)
    ioctl(fd, TIOCSWINSZ, b)

def get_dimension(fd):
    sy,sx,_,_ = struct.unpack('hhhh', ioctl(fd, TIOCGWINSZ, '\000' * 8))
    return sx,sy


class Style:
    ATTR_NAME = None
    SPEC_TO_OPERATION = {
        '': '_getEnable',
        '!': '_getDisable',
        'i': '_getInit',
        'f': '_getFini',
        '+': '_getBegin',
        '-': '_getEnd'
        }

    def __init__(self, terminal, default_parameters, args=[], kwargs={}):
        self._terminal = terminal
        if default_parameters:
            self._setDefaultParameters()
        else:
            self._setParameters(*args, **kwargs)

    def __format__(self, spec):
        t = self._terminal
        try:
            n = self.SPEC_TO_OPERATION[spec]
        except KeyError:
            raise VelueError('Invalid format spec: %s' % repr(spec))

        f = getattr(self._terminal, n)
        return f(self)

    def _setParameters(self, *args, **kwargs):
        pass

    def _setDefaultParameters(self):
        pass

    def __call__(self, *args, **kwargs):
        klass = self.__class__
        style = klass(terminal=self._terminal, default_parameters=False, args=args, kwargs=kwargs)
        return style


    @classmethod
    def _getInit(klass):
        return ''

    @classmethod
    def _getFini(klass):
        return ''

    def _getEnable(self):
        return ''

    def _getDisable(self):
        return ''

    def __or__(self, a2):
        a1 = self
        if isinstance(a1, BasicStyle) and isinstance(a2, BasicStyle):
            ca = CompoundStyle()
            ca.add(a1)
            ca.add(a2)
            return ca

        elif isinstance(a1, BasicStyle) and isinstance(a2, CompoundStyle):
            a2.add(a1)
            return a2

        elif isinstance(a1, CompoundStyle) and isinstance(a2, BasicStyle):
            a1.add(a2)
            return a1

        elif isinstance(a1, CompoundStyle) and isinstance(a2, CompoundStyle):
            for a in a2._attributes:
                a1.add(a)
            return a1

        else:
            raise TypeError()


class BasicStyle(Style):
    def getItems(self):
        return [self]

class CompoundStyle(Style):
    def __init__(self):
        self._instances = []
        self._classes = {}
        self._terminal = None

    def add(self, style):
        if self._terminal==None:
            self._terminal = style._terminal
        elif self._terminal != style._terminal:
            raise AssertionError()

        klass = style.__class__
        if not klass in FINAL_STYLE_CLASSES.values():
            raise TypeError("Unexpected instance type: %s" % repr(klass))
        if klass in self._classes:
            raise TypeError("Try to combine two styles of the same class '%s'" % repr(klass))
        self._instances.append(style)
        self._classes[klass] = True

    def getItems(self):
        return copy.copy(self._instances)









class ColorStyle(BasicStyle):
    def _setParameters(self, color, *, brightness=1.0):
        self._color = color
        self._brightness = brightness

    @classmethod
    def _getFini(klass):
        return '\x1b[%dm' % klass.RESET_SGR_NUM

    def _getEnable(self):
        x = self._color
        if not type(x) is str:
            raise TypeError()

        elif x.startswith('#'):
            x = x[1:]
            x = int(x,16)
            return self._makeRgbStyle(x)

        else:
            raise ValueError()

        if idx < 16:
            raise AssertionError()

    def _computeBrightness(self, r, g, b):
        r = int(r * self._brightness) & 0xff
        g = int(g * self._brightness) & 0xff
        b = int(b * self._brightness) & 0xff
        return r,g,b


    def _makeRgbStyle(self, x):
        r = ((x & 0xff0000) >> 16)
        g = ((x & 0xff00)   >> 8)
        b = (x & 0xff)
        r,g,b = self._computeBrightness(r,g,b)
        return '\x1b[%d;2;%d;%d;%dm' % (self.APPLY_SGR_NUM,r,g,b)


class ForegroundColorStyle(ColorStyle):
    ATTR_NAME = 'fgcol'
    APPLY_SGR_NUM = 38
    RESET_SGR_NUM = 39

class BackgroundColorStyle(ColorStyle):
    ATTR_NAME = 'bgcol'
    APPLY_SGR_NUM = 48
    RESET_SGR_NUM = 49


class BoldStyle(BasicStyle):
    ATTR_NAME = 'bold'

    def _getEnable(self):
        return '\x1b[1m'

    def _getDisable(self):
        return '\x1b[22m'


class UnderlinedStyle(BasicStyle):
    ATTR_NAME = 'underlined'

    def _getEnable(self):
        return '\x1b[4m'

    def _getDisable(self):
        return '\x1b[24m'



def read_percent(value, of):
    if type(value) is str and value.endswith('%'):
        value = (float(value[:-1]) / 100.0) * of
        return int(value)
    else:
        return value



class MoveStyle(BasicStyle):
    ATTR_NAME = 'move'

    def _setParameters(self, x, y):
        self._x = x
        self._y = y

    def _getEnable(self):
        sx, sy = self._terminal.getDimension()
        newx = None
        newy = None

        if self._x != None:
            newx = read_percent(self._x, sx)
        if self._y != None:
            newy = read_percent(self._y, sy)

        if (newx == None) and (newy == None):
            return ''
        elif (newy == None):
            return '\x1b[%dG' % newx
        else:
            return '\x1b[%d;%df' % (newy, newx)



class LocationStyle(BasicStyle):

    ATTR_NAME = 'location'

    _oldx = None
    _oldy = None

    def _getEnable(self):
        return '\x1b[s'

    def _getDisable(self):
        return '\x1b[u'



class ScrollStyle(BasicStyle):
    def _setParameters(self, y=None):
        self._y = y

    def _getEnableDisable(self, chars):
        y = self._y
        if y==0 or y==None:
            return ''
        elif y > 0:
            return '\x1b[%d%c' % (y, chars[0])
        elif y < 0:
            return '\x1b[%d%c' % (-y, chars[1])
        else:
            return ''

    def _getEnable(self):
        return self._getEnableDisable('ST')

    def _getDisable(self):
        return self._getEnableDisable('TS')


class EraseLineStyle(BasicStyle):

    ATTR_NAME = 'eraseln'

    _dx=None

    def _setParameters(self, dx=None):
        self._dx = dx

    def _getEnable(self):
        dx = self._dx
        if dx==None:
            return '\x1b[K'
        else:
            return '\x1b[%dK' % self._dx

    def _getDisable(self):
        return ''


class EraseDisplayStyle(BasicStyle):

    ATTR_NAME = 'erasedisp'

    def _getEnable(self):
        return '\x1b[2J'

    def _getDisable(self):
        return ''



def found_final_style_classes(objects):
    d = {}
    for obj in objects:
        if type(obj) is type  and  issubclass(obj, Style)  and  getattr(obj, 'ATTR_NAME'):
            d[obj.ATTR_NAME]=obj
    return d

FINAL_STYLE_CLASSES = found_final_style_classes(globals().values())



STYLE_OPERATION_NAMES = 'enable', 'disable', 'init', 'fini'


class StyleOperationAggregator:
    def __init__(self, terminal, operation, write):
        self._terminal = terminal
        self._operation = operation
        self._write = write

    def __call__(self, stylep):
        s = ''
        for style in stylep.getItems():
            f = getattr(style, self._operation)
            s += f()
        if self._write:
            self._terminal._stream.write(s)
            self._terminal._stream.flush()
            return stylep
        else:
            return s



class Terminal:
    def __init__(self, stream=None):
        if stream==None:
            self._stream = sys.__stdout__
        else:
            self._stream = stream
        self._stacks = {}
        self._applied_styles = collections.OrderedDict()
        self._fd = self._stream.fileno()

        for nw in STYLE_OPERATION_NAMES:
            ng = "_get%s%s" % (nw[0].upper(), nw[1:])
            aw = StyleOperationAggregator(self, ng, write=True)
            setattr(self, nw, aw)
            ag = StyleOperationAggregator(self, ng, write=False)
            setattr(self, ng, ag)


    def __getattribute__(self, name):
        try:
            klass = FINAL_STYLE_CLASSES[name]
        except KeyError:
            return super(Terminal, self).__getattribute__(name)
        else:
            return klass(default_parameters=True, terminal=self)

    def getDimension(self):
        return get_dimension(self._fd)

    def setDimension(self,x,y):
        return set_dimension(self._fd,x,y)

    def _getEnd(self, stylep):
        s = ''
        for ccur in stylep.getItems():
            s += ccur._getDisable()
            klass = ccur.__class__
            try:
                self._stacks[klass].pop()
            except IndexError:
                continue
            try:
                cnew = self._stacks[klass][-1]
            except IndexError:
                s += klass._getFini()
            else:
                s += cnew._getEnable()
        try:
            del self._applied_styles[stylep]
        except KeyError:
            pass
        return s

    def _getBegin(self, stylep):
        s = ''
        for style in stylep.getItems():
            klass = style.__class__
            if not klass in self._stacks:
                s += klass._getInit()
                self._stacks[klass] = []
            self._stacks[klass].append(style)
            s += style._getEnable()
        self._applied_styles[stylep] = True
        return s

    def begin(self, style):
        s = self._getBegin(style)
        self._stream.write(s)
        self._stream.flush()
        return style

    def end(self, style):
        s = self._getEnd(style)
        self._stream.write(s)
        self._stream.flush()
        return style

    def pop(self, count=1):
        r = []
        for i in range(count):
            style = self._applied_styles.popitem()[0]
            r.append(style)
            self.end(style)
        return r
