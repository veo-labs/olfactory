from olfactory.runtime import *

ensure_runtime('cpython','transcrypt')

__all__ = [
    'set_stamp',
    'del_stamp',
    'get_stamp',
    'has_stamp',
    'is_stamped',
    'get_all_stamps'
    ]


_PREFIX = '__olfactory_stamp_'


def set_stamp(obj, key, value=None):
    setattr(obj, _PREFIX+key, value)


def del_stamp(obj, key):
    delattr(obj, _PREFIX+key)


if get_runtime()=='cpython':
    NO_DEFAULT = object()
elif get_runtime()=='transcrypt':
    NO_DEFAULT = __new__ (Object())

def get_stamp(obj, key, default=NO_DEFAULT):
    if hasattr(obj,_PREFIX+key):
        return getattr(obj, _PREFIX+key)
    else:
        if default==NO_DEFAULT:
            raise AttributeError()
        else:
            return default


def has_stamp(obj, key):
    return hasattr(obj, _PREFIX+key)


def is_stamped(obj):
    for n in dir(obj):
        if n.startswith(_PREFIX):
            return True
    return False


def get_all_stamps(obj):
    d = {}
    for n in dir(obj):
        if n.startswith(_PREFIX):
            k = n[len(_PREFIX):]
            v = get_stamp(obj, k)
            d[k]=v
    return d

