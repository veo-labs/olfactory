import asyncio
import logging
import sys
import argparse
import os
import daemonize
import pwd
import grp

from olfactory import config, loghlp, job, plugin, decorators
from olfactory.ocp import server

logger = logging.getLogger(__name__)

# http://patorjk.com/software/taag/#p=display&f=Doom&t=KbInterrupt
KEYBINT_TEXT = r"""
 _   __           _    _____      _   
| | / /          | |  |_   _|    | |  
| |/ /  ___ _   _| |__  | | _ __ | |_ 
|    \ / _ \ | | | '_ \ | || '_ \| __|
| |\  \  __/ |_| | |_) || || | | | |_ 
\_| \_/\___|\__, |_.__/\___/_| |_|\__|
             __/ |                    
            |___/                     
"""


# http://patorjk.com/software/taag/#p=display&f=Doom&t=SigTerm
SIGTERM_TEXT = r"""
 _____ _     _____                   
/  ___(_)   |_   _|                  
\ `--. _  __ _| | ___ _ __ _ __ ___  
 `--. \ |/ _` | |/ _ \ '__| '_ ` _ \ 
/\__/ / | (_| | |  __/ |  | | | | | |
\____/|_|\__, \_/\___|_|  |_| |_| |_|
          __/ |                      
         |___/                       
"""


def get_user_name():
    uid = os.getuid()
    return pwd.getpwuid(uid)[0]

def get_group_name():
    gid = os.getgid()
    return grp.getgrgid(gid)[0]


class ArgumentParser(argparse.ArgumentParser):
    def __init__(self, daemon):
        argparse.ArgumentParser.__init__(self)
        conf = config.Config()
        url = conf.getItem('listen_url')
        self.add_argument("-u", dest='url', default=url, type=str, metavar='URL', help="set listening url to URL")
        self.add_argument('-D', dest='daemonize', default=False, action='store_true', help="daemonize process")
        self.add_argument('-p', dest='pidfile', default=None, type=str, help="set pid file location")
        self.add_argument('--identity', default=None, type=str, help="set server identity")
        self.add_argument('--debug-asyncio-loop', default=False, action="store_true", help="set the debug mode of the asyncio event loop")

    def parse_args(self, argv):
        conf = config.Config()
        ns = super().parse_args(argv)
        if ns.pidfile is None:
            default_pidfile = '/tmp/olfactory-daemon-%s.pid' % get_user_name()
            ns.pidfile = conf.getItem('daemon_pidfile', default_pidfile)
        return ns


class SigTermReceived(Exception):
    pass


class Daemonizer(daemonize.Daemonize):
    def __init__(self, loop, *args, **kwargs):
        super().__init__(*args,**kwargs)
        self.__loop = loop

    def sigterm(self, signum, frame):
        self.logger.warning("Caught signal #%d" % signum)
        raise SigTermReceived

    def exit(self):
        self.__loop.stop()


class Daemon:
    def __init__(self):
        self._argparser = None
        self._ns = None

    def getArgParser(self):
        return self._argparser

    @property
    def options(self):
        return self._ns

    def _logLines(self, text):
        lines = text.split('\n')
        for ln in lines:
            logger.info(ln)

    def _getIdentity(self):
        ns = self._ns
        if ns.identity is None:
            identity = "%s-%d" % (os.path.basename(sys.argv[0]),os.getpid())
            prefix = 'olfactory-'
            if identity.startswith(prefix):
                identity = identity[len(prefix):]
        else:
            identity = ns.identity
        return identity

    def _dropRootPrivileges(self):
        if os.getuid() != 0:
            return
        conf = config.Config()
        user = conf.getItem('daemon_user',None)
        if user is None:
            return
        group = conf.getItem('daemon_group',None)
        if group is None:
            return
        uinfo = pwd.getpwnam(user)
        ginfo = grp.getgrnam(group)
        os.setgid(uinfo.pw_gid)
        os.setgroups(ginfo.gr_mem)
        os.setuid(uinfo.pw_uid)

    def _setUMask(self):
        conf = config.Config()
        umsk = conf.getItem('daemon_umask',None)
        if umsk is not None:
            os.umask(umsk)

    def __call__(self):
        self._dropRootPrivileges()
        self._setUMask()
        plgmng = plugin.Manager()
        plgmng.importAll()
        self._argparser = ArgumentParser(self)
        loop = asyncio.get_event_loop()
        opts = loghlp.Options()
        opts.setStreamname('stdout')
        self._loop = loop
        plgmng.provide(
            plgmng.Provision('argparser',self._argparser,annotation='tune'),
            plgmng.Provision('daemon',self),
        loop=self._loop)
        loop.run_until_complete(plgmng.ensureTriggersExecutionFinished())
        ns = self._argparser.parse_args(sys.argv[1:])
        loghlp.install_from_ns(ns)
        self._ns = ns
        kwargs = {}
        if ns.pidfile is None:
            raise Assertion('ns.pidfile is None')
        if os.path.exists(ns.pidfile):
            content = open(ns.pidfile).read()
            logger.warning('The PID file %r containing %r already exists and will be overwritten' % (ns.pidfile, content))
            os.remove(ns.pidfile)
        kwargs['pid'] = ns.pidfile
        kwargs['action'] = self._run
        kwargs['app'] = os.path.basename(sys.argv[0])
        kwargs['logger'] = logger
        kwargs['auto_close_fds'] = False
        kwargs['chdir'] = os.getcwd()
        if ns.daemonize:
            kwargs['foreground'] = False
        else:
            kwargs['foreground'] = True
        kwargs['app'] = self._getIdentity()
        loop = asyncio.get_event_loop()
        d = Daemonizer(loop,**kwargs)
        d.start()
        raise AssertionError('daemonize start() failed')

    def _run(self):
        ns = self._ns
        identity = self._getIdentity()
        logger.info("PID is %d" % os.getpid())
        logger.info('UID is %d (%s)' % (os.getuid(), get_user_name()))
        logger.info('GID is %d (%s)' % (os.getgid(), get_group_name()))
        if len(sys.argv) > 1:
            logger.info(f'launched with following args:')
        for a in sys.argv[1:]:
            logger.info(f'  {a}')
        logger.info('using pidfile %s' % ns.pidfile)
        logger.info('identity is %r' % identity)
        logger.info('will listen on %r' % ns.url)
        plgmng = plugin.Manager()
        loop = asyncio.get_event_loop()
        self._loop = loop
        s = server.Server(ns.url,loop=loop,identity=identity)
        loop.run_until_complete(plgmng.ensureTriggersExecutionFinished())
        loop.run_until_complete(s.listen())
        loop.run_until_complete(plgmng.ensureTriggersExecutionFinished())
        n = plgmng.getNumberOfTriggersInFailure()
        if n:
            logger.error('daemon cannot be started because of triggers in failure')
            return
        logger.warn('daemon started')
        self._server = s
        loop = self._loop
        loop.set_debug(ns.debug_asyncio_loop)
        try:
            loop.run_forever()
        except KeyboardInterrupt:
            self._logLines(KEYBINT_TEXT)
        except SigTermReceived:
            self._logLines(SIGTERM_TEXT)
        except:
            loghlp.log_exception(logger)
        finally:
            self._stop()

    def _stop(self):
        ns = self._ns
        logger.warning("stopping daemon")
        s = self._server
        loop = self._loop
        loop.run_until_complete(s.close())
        m = plugin.Manager()
        loop.run_until_complete(m.ensureTriggersExecutionFinished())
        if ns.pidfile is not None:
            try:
                os.remove(ns.pidfile)
            except BaseException as ev:
                logger.warning("Cannot remove PID file %r (%s)" % (ns.pidfile, str(ev)))
            else:
                logger.info("PID file %r removed" % ns.pidfile)
        logger.warning("daemon stopped")
        m = plugin.Manager()
        m.provide(m.Provision('daemon',self,annotation='exit',forcediff=True), loop=loop)
        loop.run_until_complete(m.ensureTriggersExecutionFinished())
        sys.exit(0)
