# __pragma__ ('skip')
from olfactory.transcrypt.stubs import __pragma__
from urllib.parse import urlparse, parse_qsl
from urllib.parse import quote as _quote

class URL:
    def __init__(self,text):
        self._text = text
        self._uobj = urlparse(text)

    @property
    def scheme(self):
        return self._uobj.scheme

    @property
    def netloc(self):
        return self._uobj.netloc

    @property
    def hostname(self):
        return self._uobj.hostname

    @property
    def username(self):
        return self._uobj.username

    @property
    def password(self):
        return self._uobj.password

    @property
    def port(self):
        return self._uobj.port

    @property
    def params(self):
        l = parse_qsl(self._uobj.query)
        d = {}
        for k,v in l:
            if k in d:
                raise ValueError('duplicate parameter: ' + k)
            d[k] = v
        return d


def quote(s):
    return _quote(s)


def quote_all(sin):
    sout = ''
    for b in sin.encode():
        sout += "%%%02x" % b
    return sout
# __pragma__ ('noskip')


__pragma__ ('js', '{}',
'''
class _URL {
    constructor(text) {
        var U = window['URL'];
        this._text = text;
        this._uobj = new U(text);
        this._params = {};
    }

    get scheme() {
        var p = this._uobj.protocol;
        if (p.endsWith(':')) {
            p = p.slice(0, -1);
        }
        return p;
    }

    get netloc() {
        return this._uobj.host;
    }

    get hostname() {
        return this._uobj.hostname;
    }

    get port() {
        return this._uobj.port;
    }

    get username() {
        var u = this._uobj.username;
        if(u==='') {
            return null;
        }
        else {
            return u;
        }
    }

    get password() {
        var p = this._uobj.password;
        if(p==='') {
            return null;
        }
        else {
            return p;
        }
    }
}

export function URL(text) {
    return new _URL(text);
}

export function quote(s) {
    return encodeURIComponent(s);
}

export function quote_all(sin) {
    var sout = '';
    var e = new TextEncoder("utf-8");
    var arr = e.encode(sin);
    for(var i=0; i<arr.length; i++) {
        var n = arr[i].toString(16);
        if (arr[i] < 0) {
            throw new Error("arr[i] < 0")
        }
        else if (arr[i] < 16) {
            sout += '%0' + n;
        }
        else if (arr[i] < 256) {
            sout += '%' + n;
        }
        else {
            throw new Error("arr[i] > 256")
        }
    }
    return sout;
}
''')
