import ast


class Source:
    def __init__(self, content=None, *, filename, mode='exec'):
        if content is None:
            with open(filename) as f:
                content = f.read()
        self._content = content
        self._filename = filename
        self._mode = mode
        self._tree = ast.parse(self._content, filename=self._filename, mode=mode)

    def getLineContent(self, lineno):
        lines = self._content.split('\n')
        return lines[lineno-1]

    def execute(self, globals=None, locals=None):
        tree = self._tree
        code = compile(tree, filename=self._filename, mode=self._mode)
        if self._mode=='exec':
            rval = exec(code, globals, locals)
        elif self._mode=='eval':
            rval = eval(code, globals, locals)
        return rval

    def getTree(self):
        return self._tree

    def getFilename(self):
        return self._filename

    def getContent(self):
        return self._content
