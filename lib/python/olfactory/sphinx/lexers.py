import re

from pygments.token import Generic, Text
from pygments.lexer import Lexer


class ReplLexer(Lexer):
    name = 'REPL'

    syntax = {
        '{P}': Generic.Prompt,
        '{O}': Generic.Output,
        '{C}': Generic.Strong,
        '{{': '{',
        '}}': '}',
        }

    def __init__(self,**options):
        Lexer.__init__(self, **options)
        pattern = []
        for k in self.syntax.keys():
            pattern.append("(?:%s)" % re.escape(k))
        pattern = ' | '.join(pattern)
        pattern = "(%s)" % pattern
        self._regexp = re.compile(pattern, re.X)

    def get_tokens(self, text):
        t = Generic.Output
        tokens = re.split(self._regexp, text)
        kind = Generic.Output
        for x in tokens:
            if x in self.syntax:
                obj = self.syntax[x]
                if isinstance(obj,str):
                    yield kind,obj
                else:
                    kind=obj
            else:
                yield kind,x


def setup(app):
    app.add_lexer('olfactory-repl', ReplLexer)
