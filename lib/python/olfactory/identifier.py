from olfactory import excdef

class IDManager:
    def __init__(self):
        self._next = {}
        self._acquired = {}
        self._released = {}

    def _requirePattern(self, pattern):
        if pattern.count("{}") > 1:
            raise ValueError()
        if pattern in self._next:
            return
        self._next[pattern] = 0
        self._released[pattern] = []

    def acquire(self, pattern="{}"):
        self._requirePattern(pattern)
        if len(self._released[pattern])==0:
            ident = pattern.replace("{}", str(self._next[pattern]))
            if ident in self._acquired:
                raise excdef.NameError("%r already acquired" % ident)
            self._next[pattern] += 1
        else:
            ident = self._released[pattern].pop(0)
        self._acquired[ident] = pattern
        return ident

    def release(self, ident):
        if not ident in self._acquired:
            raise ValueError()
        pattern = self._acquired.pop(ident)
        self._released[pattern].append(ident)
        self._released[pattern].sort()
