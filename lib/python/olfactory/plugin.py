import importlib
import logging
import asyncio
import sys
import inspect
from olfactory import config, decorators, loghlp, callhlp


logger = logging.getLogger(__name__)

_manager_state = None


class Provision:
    def __init__(self, name, value=None, *, forcediff=False, annotation=None):
        self.name = name
        self.value = value
        self.annotation = annotation
        self.forcediff = forcediff
        self.new = True

    def __repr__(self):
        return f'<{self.__class__.__name__} {self.name}={self.value!r} ann={self.annotation!r} forcediff={self.forcediff!r} new={self.new!r}>'


class Trigger:
    def __init__(self, decoration):
        self._decoration = decoration

    def __repr__(self):
        f = self._decoration.getItem()
        return f'<{self.__class__.__name__} {f.__name__}@{f.__module__}>'

    def isReady(self):
        d = self._decoration
        f = d.getItem()
        s = inspect.signature(f)
        m = Manager()
        num_diff = 0
        for p in s.parameters.values():
            if (p.kind != inspect.Parameter.KEYWORD_ONLY) and (p.kind != inspect.Parameter.POSITIONAL_OR_KEYWORD):
                return False
            if p.name=='plgmng':
                continue
            if p.name not in m._provisions:
                return False
            stk = m._provisions[p.name]
            if len(stk)==0:
                return False
            newn = int(stk[-1].new)
            if len(stk)==1:
                num_diff += newn
            elif stk[-1].forcediff:
                num_diff += newn
            elif (len(stk) > 2)  and  (stk[-1].value != stk[-2].value):
                num_diff += newn
            if p.default != inspect._empty:
                if stk[-1].value != p.default:
                    return False
            if p.annotation != inspect._empty:
                if not isinstance(p.annotation,str):
                    return False
                if stk[-1].annotation != p.annotation:
                    return False
        if num_diff==0:
            return False
        else:
            return True

    def isReversed(self):
        if self._decoration.getKwarg('reverse'):
            return True
        else:
            return False

    def getPriority(self):
        d = self._decoration
        priority = d.getKwarg('priority')
        if priority is not None:
            return priority
        f = d.getItem()
        m = Manager()
        return m._getFunctionPriority(f)

    def _getKeywordArguments(self):
        d = self._decoration
        f = d.getItem()
        s = inspect.signature(f)
        m = Manager()
        kwargs = {}
        for p in s.parameters.values():
            if p.name=='plgmng':
                kwargs[p.name] = m
            else:
                kwargs[p.name] = m._provisions[p.name][-1].value
        return kwargs

    async def __call__(self):
        f = self._decoration.getItem()
        try:
            kwargs = self._getKeywordArguments()
            await callhlp.ensure_coroutine(f,**kwargs)
        except:
            loghlp.log_exception(logger, level=logging.ERROR, title=f'{self!r} failed')
            return False
        else:
            return True


class Manager:
    Provision = Provision

    def __init__(self):
        global _manager_state
        if _manager_state is not None:
            self.__dict__ = _manager_state
            return
        conf = config.Config()
        self._plugins = conf.getItem('plugins')
        self._imported = False
        _manager_state = self.__dict__
        self._provisions = {}
        self._provisions_toadd = []
        self._exec_triggers_task = None
        self._priorities = {}
        self._max_priority = 1000
        self._num_triggers_in_failure = 0

    def clearAll(self):
        self._plugins = set()

    def add(self, plugin):
        self._plugins.add(plugin)

    def _getModulePriority(self, name):
        if name is None:
            return self._max_priority
        try:
            return self._priorities[name]
        except KeyError:
            if name is None:
                return 1000
            elif name.startswith('olfactory.plugins.'):
                return 500
            elif name.startswith('olfactory.'):
                return 300
            else:
                return self._max_priority

    def _getFunctionPriority(self, f):
        name = f.__module__
        return self._getModulePriority(name)

    def importAll(self):
        if self._imported:
            raise AssertionError('plugins already imported')
        priority = self._max_priority
        for name in self._plugins:
            #print('import plugin %r (priority: %d)' % (name,priority))
            mod = importlib.import_module(name)
            priority += 1
            self._priorities[name] = priority
        self._max_priority = priority
        self._imported = True

    def _makePrioList(self, lin, reverse=False):
        l = sorted(lin, key=lambda obj: obj.getPriority())
        if reverse:
            return list(reversed(l))
        else:
            return list(l)

    def _addProvisions(self,provisions):
        for pr in provisions:
            if pr.name not in self._provisions:
                self._provisions[pr.name] = []
            stk = self._provisions[pr.name]
            stk += [pr]
            stk = stk[-2:]
            self._provisions[pr.name] = stk

    def _markProvisionsAsOld(self):
        for stk in self._provisions.values():
            for pr in stk:
                pr.new = False

    def clearProvisions(self):
        self._provisions.clear()

    async def _executeTriggers(self):
        while True:
            provisions = self._provisions_toadd
            self._provisions_toadd = []
            if len(provisions)==0:
                break
            self._addProvisions(provisions)
            triggers = self._prepareTriggers()
            for tr in triggers:
                ok = await tr()
                if not ok:
                    self._num_triggers_in_failure += 1
            self._markProvisionsAsOld()

    async def ensureTriggersExecutionFinished(self):
        if self._exec_triggers_task is None:
            return
        await self._exec_triggers_task

    def getNumberOfTriggersInFailure(self):
        return self._num_triggers_in_failure

    def provide(self,*provisions,loop):
        self._provisions_toadd += provisions
        if self._exec_triggers_task is not None:
            if loop != self._exec_triggers_task.get_loop():
                raise AssertionError('loop differs')
        else:
            tsk = loop.create_task(self._executeTriggers())
            def _(tsk):
                self._exec_triggers_task = None
                tsk.result()
            tsk.add_done_callback(_)
            self._exec_triggers_task = tsk

    def _prepareTriggers(self):
        triggers = []
        rev_triggers = []
        m = decorators.Manager()
        for dec in m.getDecorations('plg_trigger'):
            tr = Trigger(dec)
            if not tr.isReady():
                continue
            if tr.isReversed():
                rev_triggers.append(tr)
            else:
                triggers.append(tr)
        triggers = self._makePrioList(triggers)
        rev_triggers = self._makePrioList(rev_triggers, reverse=True)
        triggers = triggers + rev_triggers
        return triggers
