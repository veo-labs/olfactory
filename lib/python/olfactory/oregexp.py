from olfactory.runtime import *

ensure_runtime('cpython', 'transcrypt')

# __pragma__ ('skip')
import re
# __pragma__ ('noskip')

ESCAPED = "[ ] . - # $"
NON_ESCAPED = ": _ < > @ abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ 0123456789"

ESCAPED = ESCAPED.replace(' ','')
NON_ESCAPED = NON_ESCAPED.replace(' ','')

def translate(pattern):
    res = ''
    for ch in pattern:
        if ch=='*':
            res += '.*'
        elif ch=='+':
            res += '.+'
        elif ch in ESCAPED:
            res += '\\' + ch
        elif ch in NON_ESCAPED:
            res += ch
        else:
            raise SyntaxError('invalid oregexp pattern: %r' % pattern)
    res = '^' + res + '$'
    return res


class ORegExp:
    def __init__(self, pattern):
        self._orig_pattern = pattern
        self._translated_pattern = translate(pattern)
        if get_runtime()=='cpython':
            self._obj = re.compile(self._translated_pattern)
        else:
            self._obj = __new__ (RegExp(self._translated_pattern))

    def match(self, s):
        if get_runtime()=='cpython':
            m = self._obj.match(s)
        else:
            m = s.match(self._obj)
        if m:
            return True
        else:
            return False
