from olfactory import excdef

__pragma__ ('noanno')

__pragma__ ('js', '{}', __include__ ('../js/thirdparty/websocket-client.js'))


class Socket:
    def __init__(self):
        self._handle = __new__ (WebSocketClient())
        self._handle._tuneCloseEvent = self._tuneCloseEvent

    async def connect(self,url):
        await self._handle.connect(url)

    async def close(self):
        await self._handle.disconnect()

    async def send(self,data):
        self._handle.send(data)

    async def recv(self):
        buf = await self._handle.receive()
        data = __new__(Uint8Array(buf))
        return data

    def _tuneCloseEvent(self,event):
        __pragma__ ('js', '{}', '''if (!event instanceof CloseEvent) return event;''')
        exc = excdef.ConnectionClosed(event.reason)
        return exc


async def connect(url):
    socket = Socket()
    await socket.connect(url)
    return socket

# fake attributes to pass angular building machinery
class exceptions:
    ConnectionClosed = None
