class NullLogger:
    def __init__(self,name):
        self.name = name

    def info(self,s):
        pass

    def debug(self,s):
        pass

    def error(self,s):
        pass


class ConsoleLogger:
    def __init__(self,name):
        self.name = name

    def _format_msg(self, s):
        msg = "{}: {}".format(self.name, s)
        return msg

    def info(self,s):
        msg = self._format_msg(s)
        console.info(msg)

    def debug(self,s):
        msg = self._format_msg(s)
        console.debug(msg)

    def error(self,s):
        msg = self._format_msg(s)
        console.error(msg)


def getLogger(name):
    return NullLogger(name)

