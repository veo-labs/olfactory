__pragma__ ('noanno')

__pragma__ ('js', '{}', __include__ ('../js/thirdparty/msgpack.js'))

_msgpack = window['msgpack']
del window['msgpack']

def encode(obj, codec):
    return _msgpack.encode(obj,{'codec': codec})

def decode(data, codec):
    return _msgpack.decode(data,{'codec': codec})

def createCodec():
    codec = _msgpack.createCodec();
    return codec

# fake attributes to pass angular building machinery
ExtType = None
packb = None
unpackb = None
