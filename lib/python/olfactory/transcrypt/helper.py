def extract_arguments(args,signature):
    args = list(args)
    l = len(args)
    if '__kwargtrans__' not in args[l-1]:
        kwargs = {}
    else:
        kwargs = args[l-1]
        del kwargs['__kwargtrans__']
        del kwargs['constructor']
        args = args[:l-1]
    rval = []
    for k in signature:
        if isinstance(k,str):
            default = False
            v = None
        else:
            default = True
            k,v = k
        if k=='**':
            rval.append(kwargs)
        elif k.endswith('*'):
            idx = int(k[:-1])
            rval.append(args[idx:])
        elif k in kwargs:
            rval.append(kwargs[k])
        elif default:
            rval.append(v)
        else:
            raise ValueError('Keyword argument is missing: ' + k)
    rval = tuple(rval)
    return tuple(rval)
