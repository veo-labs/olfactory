from olfactory.transcrypt import helper


class CancelledError(Exception):
    pass

class Loop:
    def __init__(self):
        pass

_loop = None

def get_event_loop(self):
    global _loop
    if _loop == None:
        _loop = Loop()
    return _loop


class BaseFuture:
    def __init__(self):
        self._state = 'PENDING'
        self._result = None
        self._promise = None
        self._exception = None

    def _require_state(self, state):
        if self._state != state:
            raise Error('Invalid future state ({})'.format(self._state))

    def _set_result(self, result):
        self._require_state('PENDING')
        self._result = result
        self._state = 'FINISHED'

    def _set_exception(self, exception):
        self._require_state('PENDING')
        self._exception = exception
        self._state = 'FINISHED'

    def cancelled(self):
        return self._state == 'CANCELLED'

    def done(self):
        return self._state != 'PENDING'

    def promise(self):
        return self._promise

    def result(self):
        if self._state == 'CANCELLED':
            raise Error('Future is cancelled')
        if self._state != 'FINISHED':
            raise Error('Result is not ready')
        if self._exception is not None:
            raise self._exception
        return self._result

    def exception(self):
        if self._state == 'CANCELLED':
            raise Error('Future is cancelled')
        if self._state != 'FINISHED':
            raise Error('Exception is not set')
        return self._exception


class WrappedPromise(BaseFuture):
    def __init__(self, orig_promise):
        super().__init__(self)
        self._promise = orig_promise.then(self._resolve,self._reject)

    def _resolve(self, result):
        self._set_result(result)
        return result

    def _reject(self, exc):
        self._set_exception(exc)
        raise exc


def create_timeout_promise(delay):
    def _(resolve):
        setTimeout(resolve,delay * 1000.0)
    p = __new__ (Promise(_))
    return p


def create_timeout_future(delay):
    fut = Future()
    def _():
        fut.set_result(None)
    setTimeout(_, delay * 1000.0)
    return fut


async def sleep(delay):
    p = create_timeout_promise(delay)
    await p


class Future(BaseFuture):
    def __init__(self):
        super().__init__(self)
        def _(resolve,reject):
            self._resolve = resolve
            self._reject = reject
        self._promise = __new__ (Promise(_))

    def set_result(self, result):
        self._resolve(result)
        self._set_result(result)

    def set_exception(self, exc):
        self._reject(exc)
        self._set_exception(exc)

    def cancel(self):
        if self._state != 'PENDING':
            return False
        exc = Error('CancelledError')
        self.set_exception(exc)
        return True


FIRST_COMPLETED = 0
ALL_COMPLETED   = 1

async def wait(promises,*,return_when):
    return_when, = helper.extract_arguments(js_arguments,['return_when'])
    r = await _wait(promises,return_when)
    return r

async def _wait(futures,return_when):
    done = []
    pending = []
    promises = []
    def prepare_done_and_pending(value):
        for fut in futures:
            if fut._state=='FINISHED':
                done.append(fut)
            else:
                pending.append(fut)
    for fut in futures:
        promises.append(fut.promise())
    if return_when==FIRST_COMPLETED:
        p = Promise.race(promises)
    elif return_when==ALL_COMPLETED:
        p = Promise.all(promises)
    else:
        raise Error('unexpected value for return_when ({})'.format(return_when))
    f = prepare_done_and_pending
    await p.then(f,f)
    return done, pending


def run_promise_until_complete(promise):
    result = None
    exc = None
    def resolve(_):
        result = _
    def reject(_):
        exc = _
    promise.then(resolve, reject)
    Promise.race([promise])
    if exc != None:
        raise exc
    return result


class Queue:
    def __init__(self, *, loop=None):
        self._getters = []
        self._values = []

    def put_nowait(self, item):
        self._values.append(item)
        while len(self._getters) > 0:
            g = self._getters.pop(0)
            if not g.done():
                g.set_result(None)
                break

    async def get(self):
        while len(self._values) == 0:
            getter = Future()
            self._getters.append(getter)
            await getter.promise()
        return self._values.pop(0)

