import sys
import asyncio
import logging
import weakref

from olfactory import loghlp, excdef, callhlp

logger = logging.getLogger(__name__)


class Event:
    def __init__(self,*args,**kwargs):
        self.name = args[0]
        self.args = args[1:]
        self.kwargs = kwargs

    def __repr__(self):
        return '<Event %s %r %r>' % (self.name,self.args,self.kwargs)


class EventHandler:
    BREAK = object()

    def __init__(self, *, loop):
        self._loop = loop
        self.__queue = None
        self.__housings = []
        self.__finalizing = False

    def getLoop(self):
        return self._loop

    def _addHousing(self, obj):
        wref = weakref.ref(obj)
        self.__housings.append(wref)

    def _removeHousing(self, obj):
        wref = weakref.ref(obj)
        idx = self.__housings.index(wref)
        self.__housings.pop(idx)

    def _initializeEventProcessing(self):
        loop = self._loop
        fut = asyncio.ensure_future(self.__realEventProcessing())
        fut.add_done_callback(self.__realEventProcessingDone)
        self.__queue = asyncio.Queue(loop=loop)
        self.__finishing = False

    def _postEvent(self,*args,**kwargs):
        if self.__queue==None:
            return
        evt = Event(*args,**kwargs)
        self.__queue.put_nowait(evt)

    def _postEventLater(self,*args,**kwargs):
        delay = args[0]
        args = args[1:]
        evt = Event(*args,**kwargs)
        loop = self._loop
        th = loop.call_later(delay, self.__queue.put_nowait, evt)
        return th

    def _finalizeEventProcessing(self):
        if self.__finalizing:
            return
        self.__finalizing = True
        self._postEvent('FINALIZE')

    async def _eventProcessingDone(self,triplet):
        pass

    async def __realEventProcessing(self):
        triplet = None
        try:
            await self.__eventProcessing()
        except:
            loghlp.log_exception(logger, title='__realEventProcessing() failed.')
            triplet = sys.exc_info()
        finally:
            self.__queue = None
            await self._eventProcessingDone(triplet)

    def __realEventProcessingDone(self,fut):
        try:
            fut.result()
        except:
            loghlp.log_exception(logger, title='__realEventProcessingDone() failed. THIS SHOULD NOT HAPPEN !!!')

    async def __callEvent(self,evt):
        housings = []
        for wref in self.__housings:
            obj = wref()
            if not obj:
                continue
            housings.append(wref)
        self.__housings = housings
        wrefs = self.__housings + [weakref.ref(self)]
        for wref in wrefs:
            name = "_ev_%s" % evt.name
            obj = wref()
            try:
                f = getattr(obj,name)
            except AttributeError:
                continue
            res = await callhlp.ensure_coroutine(f,*evt.args,**evt.kwargs)
            if res==self.BREAK:
                return
            elif isinstance(res,Event):
                evt = res

    async def __eventProcessing(self):
        while True:
            evt = await self.__queue.get()
            if evt.name=='FINALIZE':
                return
            await self.__callEvent(evt)
