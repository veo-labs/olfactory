Getting started with ``olfactory``
==================================

Setup environment
~~~~~~~~~~~~~~~~~

.. code-block:: shell-session

    $ python3.7  -m venv ./venv
    $ . env.sh
    $ pip install --upgrade pip
    $ pip install --upgrade setuptools
    $ pip install --upgrade wheel
    $ pip install -r requirements.txt

This must be done once. After that the environment can be set easily
with:

.. code-block:: shell-session

    $ . env.sh


Build JavaScript bindings
~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: shell-session

    $ cd $OLFACTORY_TOPDIR
    $ scons js


Build sphinx documentation
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: shell-session

    $ cd $OLFACTORY_TOPDIR
    $ scons docs


This command puts generated HTML files in ``$OLFACTORY_TOPDIR/build/sphinx/html``.


Run the tests
~~~~~~~~~~~~~

.. code-block:: shell-session

    $ cd $OLFACTORY_TOPDIR
    $ pytest tests/*.py
