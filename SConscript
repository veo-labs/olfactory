import sys
import os
import re
import hashlib
import subprocess
import shutil
import glob
import npmdownloader
import tarfile
import jsbeautifier

TOPDIR = os.path.abspath('./')

JS_THIRDPARTY = [
    dict(pkgname='websocket-async', pkgver='1.3.0',  members={'package/lib/websocket-client.js': 'websocket-client.js'}, beautify=False),
    dict(pkgname='msgpack-lite',    pkgver='0.1.26', members={'package/dist/msgpack.min.js': 'msgpack.js'},              beautify=True),
]


def main():
    env = create_environment()
    t = []
    t += setup_sphinx(env)
    t += setup_thirdparty_js(env)
    t += setup_js(env)
    t += setup_testmaterials(env)
    Alias('olfactory_all', t)


def setup_testmaterials(env):
    t = []
    bn = 'qemu-rpi-kernel-master'
    cmd = f"cd tests/qemu && rm -fr {bn} {bn}.zip && "
    cmd += f"wget https://github.com/dhruvvyas90/qemu-rpi-kernel/archive/master.zip -O {bn}.zip; unzip -o {bn}.zip"
    t1 = env.Command([f'{bn}.zip'], [], cmd)
    AlwaysBuild(t1)
    t += t1
    bn = 'raspbian_lite_latest'
    cmd = f"cd tests/qemu && rm -fr {bn} {bn}.zip *araspbian-*-lite.img && "
    cmd += f"wget https://downloads.raspberrypi.org/{bn} -O {bn}.zip; unzip -o {bn}.zip"
    t2 = env.Command([f'{bn}.zip'], [], cmd)
    AlwaysBuild(t2)
    t += t2
    Alias('olfactory_testmaterials', t)
    return t


def setup_sphinx(env):
    sourcedir = '%s/docs/sphinx' % TOPDIR
    builddir = '%s/build/sphinx' % TOPDIR
    index_txt = "%s/index.txt" % sourcedir
    index_html = "%s/html/index.html" % builddir
    t = env.Command(index_html, index_txt, "sphinx-build -M html %s %s" % (sourcedir, builddir))
    AlwaysBuild(t)
    Alias('olfactory_sphinx', t)
    return t



class JSPDepsGrabber:
    def __init__(self, *, pkgname, pkgver, members, beautify):
        self._pkgname = pkgname
        self._pkgver = pkgver
        self._members = members
        self._beautify = beautify
        self._builddir = f'{TOPDIR}/lib/js/thirdparty'
        self._pkgdir =  f'{TOPDIR}/lib/js/thirdparty/.packages'

    def getTargetNames(self):
        names = []
        bdir = self._builddir
        for m in self._members:
            names += [self._getTargerName(m)]
        return names

    def _getTargerName(self,m):
        bn = self._members[m]
        bdir = self._builddir
        return f'{bdir}/{bn}'

    def __call__(self,target,source,env):
        bdir = self._builddir
        pkgdir = self._pkgdir
        pkgname = self._pkgname
        if not os.path.exists(pkgdir):
            os.makedirs(pkgdir)
        d = npmdownloader.NpmPackageDownloader(pkgdir)
        d.download(self._pkgname, version=self._pkgver)
        archive = glob.glob(f'{pkgdir}/*/{pkgname}*.tgz')
        if len(archive) != 1:
            raise AssertionError
        archive = archive[0]
        t = tarfile.open(archive)
        for m in self._members.keys():
            content = t.extractfile(m).read()
            content = content.decode()
            if self._beautify:
                content = jsbeautifier.beautify(content)
            f = open(self._getTargerName(m), 'w')
            f.write(content)
            f.close()


def setup_js(env):
    js_install_dir = env['JSINSTDIR']
    targets = []
    trans_t = env.Command(['%s/olfactory.transcrypt.entrypoint.js' % js_install_dir],
        ['%s/lib/python/olfactory/transcrypt/entrypoint.py' % TOPDIR],
        transcrypt_build)
    targets.append(trans_t)
    AlwaysBuild(targets)
    Alias('olfactory_js', targets)
    return targets


def setup_thirdparty_js(env):
    targets = []
    for kwargs in JS_THIRDPARTY:
        g = JSPDepsGrabber(**kwargs)
        t = env.Command(g.getTargetNames(), [], g)
        targets.append(t)
    Alias('thirdparty_js', targets)
    return targets


def execute(cmd,**kwargs):
    print("Execute: %s" % cmd)
    tp = subprocess.run(cmd, **kwargs)
    if tp.returncode != 0:
        if hasattr(tp,'stdout'):
            print(tp.stdout.decode())
        raise AssertionError('cmd %r failed (returncode=%d)' % (tp.args,tp.returncode))
    return tp


TRANSCRYPT_OUTPUT_PARSING = [
    ('Saving source', 'map'),
    ('Saving target', 'js'),
    ('Parsing',       'py'),
    ('Generating',    'js2'),
]


def transcrypt_execute(*, python_lib_dir, cwd, entrypoint, sourcemap, minification, transcrypt):
    targetdir = "%s/__target__" % cwd
    if os.path.exists(targetdir):
        execute('rm -fr %s' % targetdir, shell=True)
    cmd = '%s -v -b ' % transcrypt
    if sourcemap:
        cmd += '-m '
    if not minification:
        cmd += '-n '
    cmd += f'-e 7 -xp {python_lib_dir}${TOPDIR}/build {entrypoint}'
    tp = execute(cmd, capture_output=True, shell=True, cwd=cwd)
    stdout = tp.stdout.decode()
    d = {}
    for pattern,key in TRANSCRYPT_OUTPUT_PARSING:
        d[key] = set()
    for ln in stdout.split('\n'):
        m = re.match(r"^(.+): (\S+)$", ln)
        if not m:
            continue
        if m:
            for pattern,key in TRANSCRYPT_OUTPUT_PARSING:
                if pattern in m.group(1):
                    d[key].add(m.group(2))
                    break
    error = False
    if 'error' in stdout.lower():
        error = True
    if d['js2'] != d['js']:
        error = True
    if len(d['py']) != len(d['js']):
        error = True
    if error:
        print(stdout)
        raise AssertionError('cmd %r failed' % tp.args)
    return d


def transcrypt_make_execute_kwargs(target,source,env):
    python_lib_dir = env['PYLIBDIR']
    src0 = os.path.abspath(str(source[0]))
    if not src0.startswith(python_lib_dir):
        raise AssertionError('Source (%r) not in PYLIBDIR (%r)' % (src0,python_lib_dir))
    entrypoint = src0[len(python_lib_dir):]
    entrypoint = entrypoint.replace('/','.')
    if entrypoint.startswith('.'):
        entrypoint=entrypoint[1:]
    if entrypoint.endswith('.py'):
        entrypoint=entrypoint[:-3]
    kwargs = {}
    kwargs['cwd'] =  '%s/build' % TOPDIR
    kwargs['entrypoint'] = entrypoint
    kwargs['python_lib_dir'] = python_lib_dir
    kwargs['sourcemap'] = env['SOURCEMAP']
    kwargs['minification'] = env['MINIFICATION']
    kwargs['transcrypt'] = env['TRANSCRYPT']
    return kwargs


def remove_timestamp(filename):
    lines = open(filename).readlines()
    if lines[0].startswith('//'):
        lines.pop(0)
    f = open(filename,'w')
    for ln in lines:
        f.write(ln)
    f.close()


def transcrypt_build(target,source,env):
    jsinstdir = env['JSINSTDIR']
    targetdir = '%s/build/__target__' % TOPDIR
    shutil.rmtree(targetdir, ignore_errors=True)
    shutil.rmtree(jsinstdir, ignore_errors=True)
    kwargs = transcrypt_make_execute_kwargs(target,source,env)
    d = transcrypt_execute(**kwargs)
    metadata_js = "%s/__metadata__.js" % os.path.dirname(d['js'].pop())
    f = open(metadata_js, 'w')
    f.write('export var PYTHON_SOURCE_CHECKSUMS = {\n')
    python_lib_dir = kwargs['python_lib_dir']
    for fn in sorted(d['py']):
        m = hashlib.md5()
        content = open(fn,'rb').read()
        m.update(content)
        dgst = m.hexdigest()
        if not fn.startswith(python_lib_dir):
            continue
        fn = fn[len(python_lib_dir)+1:]
        f.write("  '%s': '%s',\n" % (fn,dgst))
    f.write('}\n')
    f.close()
    numfiles = 0
    print()
    print(f'Generated files in {targetdir}:')
    for fn in os.listdir(targetdir):
        ffn = f'{targetdir}/{fn}'
        if fn.endswith('.js'):
            remove_timestamp(ffn)
            numfiles += 1
            print(f"  {fn}")
        else:
            os.remove(f'{targetdir}/{fn}')
    print()
    print(f"  move those %d items in {jsinstdir}" % numfiles)
    shutil.move(targetdir, jsinstdir)
    print()


def _Import(k):
    Import(k)
    v = globals()[k]
    return v


def extract_shell_env(*args):
    d = {}
    for a in args:
        d[a] = os.getenv(a)
    return d


def create_environment():
    pylibdir = _Import('pylibdir')
    sourcemap = _Import('sourcemap')
    minification = _Import('minification')
    transcrypt = _Import('transcrypt')
    env = Environment(ENV = extract_shell_env('PATH','PYTHONPATH'))
    if pylibdir is None:
        p = os.getenv('PYTHONPATH')
        pylibdir = p.split(':')[0]
    pylibdir = os.path.abspath(pylibdir)
    env.Append(PYLIBDIR=pylibdir)
    jsinstdir = 'lib/js/olfactory/'
    jsinstdir = os.path.abspath(jsinstdir)
    env.Append(JSINSTDIR=jsinstdir)
    if sourcemap is None:
        sourcemap = False
    else:
        sourcemap = bool(int(sourcemap))
    env.Append(SOURCEMAP=sourcemap)
    if minification is None:
        minification = False
    else:
        minification = bool(int(minification))
    env.Append(MINIFICATION=minification)
    if transcrypt is None:
        transcrypt='transcrypt'
    else:
        transcrypt=os.path.abspath(transcrypt)
    env.Append(TRANSCRYPT=transcrypt)
    print('* olfactory environment created')
    for k in 'PYLIBDIR', 'SOURCEMAP', 'MINIFICATION', 'JSINSTDIR', 'TRANSCRYPT':
        print("  %s = %r" % (k,env[k]))
    print()
    return env


main()
