with Section('olfactory'):
    default_plugins = [
        'olfactory.plugins.channel',
        'olfactory.plugins.testjob',
        'olfactory.plugins.chanatt',
        'olfactory.plugins.replicant',
        'olfactory.plugins.tty',
        'olfactory.plugins.testext',
        'olfactory.plugins.repl',
        'olfactory.plugins.sh',
        'olfactory.plugins.proc',
        'olfactory.plugins.ssh',
        'olfactory.plugins.jobmng',
        'olfactory.plugins.qemu',
        'olfactory.plugins.inet',
        'olfactory.plugins.logopts',
    ]

    plugins = copy(default_plugins)

    listen_url = 'ws://*:1087'
    connect_url = 'ws://127.0.0.1:1087'
    daemon_user = None
    daemon_group = None

    ct_reexec_delay = 0.7
    test_ocp_listen_url = 'ws://127.0.0.1:9087'
    test_ocp_connect_url = test_ocp_listen_url
