import asyncio
import pytest
from olfactory import plugin
from olfactory.pytest.dec import *
from olfactory.pytest.plg import *


DecFixture('dec',inner=True).inject()
PluginManagerFixture('plgmng').inject()


@pytest.mark.asyncio
async def test_provide0(event_loop,plgmng,dec):
    l = []
    @dec.plg_trigger
    def _(item):
        l.append(item)
    plgmng.provide(plgmng.Provision('item',42),loop=event_loop)
    await asyncio.sleep(0.3)
    assert l[0]==42


@pytest.mark.asyncio
async def test_provide1(event_loop,plgmng,dec):
    l = []
    @dec.plg_trigger
    def _(item:'zorg'):
        l.append(item)
    plgmng.provide(plgmng.Provision('item',42,annotation='zorg'),loop=event_loop)
    await asyncio.sleep(0.3)
    assert l[0]==42


@pytest.mark.asyncio
async def test_provide2(event_loop,plgmng,dec):
    l = []
    @dec.plg_trigger
    def _(item:'zorg'):
        l.append(item)
    plgmng.provide(plgmng.Provision('item',42,annotation='grudu'),loop=event_loop)
    await asyncio.sleep(0.3)
    assert len(l)==0


@pytest.mark.asyncio
async def test_provide3(event_loop,plgmng,dec):
    l = []
    @dec.plg_trigger
    def _(item=42):
        l.append(item)
    plgmng.provide(plgmng.Provision('item',43),loop=event_loop)
    await asyncio.sleep(0.3)
    assert len(l)==0


@pytest.mark.asyncio
async def test_provide4(event_loop,plgmng,dec):
    l = []
    loop = asyncio.get_event_loop()
    @dec.plg_trigger
    def _(item=42):
        l.append(item)
    plgmng.provide(plgmng.Provision('item',42),loop=event_loop)
    await asyncio.sleep(0.3)
    assert l[0]==42


@pytest.mark.asyncio
async def test_provide5(event_loop,plgmng,dec):
    l = []
    @dec.plg_trigger
    def _(a,b:'zorg',c=42):
        l.append(None)
    plgmng.provide(plgmng.Provision('c',42),loop=event_loop)
    await asyncio.sleep(0.3)
    assert len(l)==0
    plgmng.provide(plgmng.Provision('b',annotation='zorg'),loop=event_loop)
    await asyncio.sleep(0.3)
    assert len(l)==0
    plgmng.provide(plgmng.Provision('a'),loop=event_loop)
    await asyncio.sleep(0.3)
    assert l[0]==None


@pytest.mark.asyncio
async def test_provide6(event_loop,plgmng,dec):
    l = []
    @dec.plg_trigger
    def _(x, a:'groz', d='zorg'):
        l.append(x)
    plgmng.provide(plgmng.Provision('a',annotation='groz'),loop=event_loop)
    await asyncio.sleep(0.3)
    assert len(l)==0
    plgmng.provide(plgmng.Provision('d','zorg'),loop=event_loop)
    await asyncio.sleep(0.3)
    assert len(l)==0
    for i in range(3):
        plgmng.provide(plgmng.Provision('x',42),loop=event_loop)
        await asyncio.sleep(0.3)
    assert l==[42]


@pytest.mark.asyncio
async def test_provide7(event_loop,plgmng,dec):
    l = []
    @dec.plg_trigger
    def _(x, a:'groz', d='zorg'):
        l.append(x)
    plgmng.provide(plgmng.Provision('a',annotation='groz'),loop=event_loop)
    await asyncio.sleep(0.3)
    assert len(l)==0
    plgmng.provide(plgmng.Provision('d','zorg'),loop=event_loop)
    await asyncio.sleep(0.3)
    assert len(l)==0
    for i in range(3):
        plgmng.provide(plgmng.Provision('x',42,forcediff=True),loop=event_loop)
        await asyncio.sleep(0.3)
    assert l==[42,42,42]


@pytest.mark.asyncio
async def test_priority0(event_loop,plgmng,dec):
    l = []
    @dec.plg_trigger(priority=100)
    def _(append):
        l.append(1)
    @dec.plg_trigger(priority=200)
    def _(append):
        l.append(2)
    @dec.plg_trigger(priority=300)
    def _(append):
        l.append(3)
    plgmng.provide(plgmng.Provision('append'),loop=event_loop)
    await asyncio.sleep(0.3)
    assert l==[1,2,3]


@pytest.mark.asyncio
async def test_priority1(event_loop,plgmng,dec):
    l = []
    @dec.plg_trigger(priority=100,reverse=True)
    def _(append):
        l.append(1)
    @dec.plg_trigger(priority=200,reverse=True)
    def _(append):
        l.append(2)
    @dec.plg_trigger(priority=300,reverse=True)
    def _(append):
        l.append(3)
    plgmng.provide(plgmng.Provision('append'),loop=event_loop)
    await asyncio.sleep(0.3)
    assert l==[3,2,1]
