import random
import asyncio
import os
import pytest
from olfactory.excdef import *
from olfactory.pytest.ocp import *
from olfactory.pytest.plg import *


LAUNCH_QEMU = os.path.dirname(__file__) + '/qemu/scripts/launch-qemu.py'


PluginManagerFixture('plgmng').inject()
OCPServerFixture('srv').inject()
OCPClientFixture('cl').inject()


@pytest.fixture
async def qemu(cl):
    jb = await cl.xcall('job::run', args=[LAUNCH_QEMU, '--', '-S'],
        kwargs=dict(klass='proc', runsubtty=False, name='qemu', jobexts=['qemu']))
    await asyncio.sleep(1)
    info = await cl.callm(jb, 'getInfo')
    if info['status'] != 'started':
       raise AssertionError('invalid status: {}'.format(info['status']))
    info = await cl.callm(jb,'getSubTTYInfo')
    jobs = {'qemu': jb}
    for k,v in info.items():
        jb = await cl.xcall('job::run', kwargs=dict(klass='tty', name=k, path=v, jobexts=['repl','sh']))
        jobs[k] = jb
    m = jobs['qemu.compat_monitor0']
    await cl.callm(m, 'syncp')
    await cl.callm(m, 'cmd', 'c')
    s = jobs['qemu.serial0']
    await cl.callm(s, 'easytrackp',  r'pi@raspberrypi:~$ ')
    await cl.callm(s, 'expect', 'login:')
    await cl.callm(s, 'tx', 'pi\r')
    await cl.callm(s, 'expect', 'Password:')
    await cl.callm(s, 'tx', 'raspberry\r')
    await cl.callm(s, 'syncp')
    return jobs


@pytest.mark.asyncio
@pytest.mark.async_timeout(120)
async def test_echo0(srv,cl,qemu):
    s = qemu['qemu.serial0']
    r = str(random.random())
    x0 = await cl.callm(s, 'cmd', 'echo %s' % r)
    print(x0.text)
    assert r in x0.out


@pytest.mark.asyncio
@pytest.mark.async_timeout(120)
async def test_shcmd0(srv,cl,qemu):
    s = qemu['qemu.serial0']
    r = str(random.random())
    x0 = await cl.callm(s, 'shcmd', 'touch /tmp/%s' % r)
    print(x0.text)


@pytest.mark.asyncio
@pytest.mark.async_timeout(120)
async def test_shcmd1(srv,cl,qemu):
    s = qemu['qemu.serial0']
    r = str(random.random())
    x0 = await cl.callm(s, 'shcmd', 'touch /tmp/%s' % r)
    x1 = await cl.callm(s, 'shcmd', 'rm /tmp/%s' % r)
    x = x0+x1
    print(x.text)


@pytest.mark.asyncio
@pytest.mark.async_timeout(120)
async def test_shcmd2(srv,cl,qemu):
    s = qemu['qemu.serial0']
    r = str(random.random())
    with pytest.raises(UnexpectedReturnCodeError):
        await cl.callm(s, 'shcmd', 'rm /tmp/%s' % r)


@pytest.mark.asyncio
@pytest.mark.async_timeout(120)
async def test_shcmd3(srv,cl,qemu):
    s = qemu['qemu.serial0']
    for i in range(5):
        r = str(random.random())
        x0 = await cl.callm(s, 'shcmd', 'mkdir /tmp/%s' % r)
        x1 = await cl.callm(s, 'shcmd', 'rmdir /tmp/%s' % r)
        x = x0+x1
        print(x.text)
