import time
import asyncio

from olfactory.testing import *

def test_skip0():
   time.sleep(1)

class Case(BaseTestCase):
    def test_skip1(self):
        time.sleep(2)

    async def test_skip2(self):
        await asyncio.sleep(1)
