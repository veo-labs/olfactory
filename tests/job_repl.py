import time
import asyncio
import pytest
import random
from olfactory.pytest.ocp import *
from olfactory.pytest.plg import *
from olfactory.excdef import *


OCPServerFixture('srv').inject()
OCPClientFixture('cl').inject()
PluginManagerFixture('plgmng').inject()


async def _repl(cl):
    kwargs = {}
    kwargs['class'] = 'replicant'
    kwargs['jobexts'] = ['repl','sh']
    chan = await cl.xcall('job::run', kwargs=kwargs)
    await cl.callm(chan,'syncp')
    return chan


@pytest.fixture
async def repl(cl):
    return await _repl(cl)


@pytest.fixture
async def many_repls(cl):
    l = []
    for i in range(2):
        r = await _repl(cl)
        l.append(r)
    return l


@pytest.mark.asyncio
@pytest.mark.parametrize('cmd',['echo','echon'],ids=['','n'])
async def test_echo0(srv,cl,repl,cmd):
    r = str(random.random())
    x0 = await cl.callm(repl,'cmd','%s %s' % (cmd,r))
    print(x0.text)
    assert x0.text.count(r) == 2


@pytest.mark.asyncio
@pytest.mark.parametrize('cmd',['echo','echon'],ids=['','n'])
async def test_echo1(srv,cl,repl,cmd):
    r = str(random.random())
    x = []
    for i in range(5):
        x0 = await cl.callm(repl,'cmd','%s molo#%d' % (cmd,i))
        x += x0
        print(x.text)
    assert x.text.count('molo') == 10
    assert x.text.count('replicant0') == 6


@pytest.mark.asyncio
async def test_echo2(srv,cl,repl):
    await cl.callm(repl, 'tx', ' ')
    with pytest.raises(StateError):
        await cl.callm(repl,'cmd','echo molo')


@pytest.mark.asyncio
@pytest.mark.async_timeout(15)
async def test_echo3(srv,cl,many_repls):
    for i in range(len(many_repls)):
        repl = many_repls[i]
        await cl.callm(repl,'syncp')
        x = []
        for word in ('one', 'two', 'three', 'four', 'five'):
            x0 = await cl.callm(repl,'cmd', 'echo %s' % word)
            print(x0)


@pytest.mark.asyncio
async def test_calc(srv,cl,repl):
    spl = random.sample(range(100), 6)
    x0 = await cl.callm(repl,'cmd','calc %s' % '+'.join(map(str,spl)))
    print(x0.text)
    assert int(x0.out) == sum(spl)


@pytest.mark.asyncio
async def test_sleep0(srv,cl,repl):
    delay=2
    t0 = time.monotonic()
    x0 = await cl.callm(repl,'cmd','sleep %d' % delay)
    t1 = time.monotonic()
    dt = t1-t0
    print(dt)
    assert pytest.approx(dt,0.1)==delay


@pytest.mark.asyncio
async def test_setprompt0(srv,cl,repl):
    await cl.callm(repl,'resetp')
    x0 = await cl.callm(repl,'cmd','setprompt molo>~')
    await cl.callm(repl,'syncp')


@pytest.mark.asyncio
async def test_setprompt1(srv,cl,repl):
    await cl.callm(repl,'resetp')
    x0 = await cl.callm(repl,'cmd','setprompt molo>~')
    x1 = await cl.callm(repl,'cmd','echo zinzino')
    x = x0+x1
    print(x.text)
    assert x.text.count('replicant0')==1
    assert x.text.count('molo')==3
    assert x.text.count('zinzino')==2


@pytest.mark.asyncio
async def test_setprompt2(srv,cl,repl):
    await cl.callm(repl,'resetp')
    x0 = await cl.callm(repl,'cmd','setprompt $$OK$$')
    await cl.callm(repl,'syncp')
    x1 = await cl.callm(repl,'cmd', 'calc 1+2+39')
    assert int(x1.out)==42


@pytest.mark.asyncio
async def test_bspc0(srv,cl,repl):
    loop = asyncio.get_event_loop()
    for i in range(10):
        await cl.callm(repl,'tx', chr(65+i))
        await asyncio.sleep(0.03)
    async def _():
        for i in range(10):
            await cl.callm(repl,'tx', '\x7f')
            await asyncio.sleep(0.03)
    t1 = loop.create_task(_())
    t2 = loop.create_task(cl.callm(repl,'syncp'))
    await asyncio.wait([t1,t2])
    t1.result()
    t2.result()
    x1 = await cl.callm(repl,'cmd', 'calc 1+2+39')
    assert int(x1.out)==42


@pytest.mark.asyncio
async def test_asciiart0(srv,cl,repl):
    asciiart = await cl.callm(repl,'getAsciiart')
    print(repr(asciiart))
    x0 = await cl.callm(repl,'cmd','asciiart')
    print(repr(x0.out))
    assert asciiart in x0.out


@pytest.mark.asyncio
async def test_shcmd0(srv,cl,repl):
    for i in range(10):
        x0 = await cl.callm(repl,'shcmd', 'echo %d ->%d' % (i,i), expected_returncode=i)


@pytest.mark.asyncio
async def test_shcmd1(srv,cl,repl):
    for i in range(10):
        x0 = await cl.callm(repl,'shcmd', 'echo %d ->%d' % (i,i), expected_returncode=None)


@pytest.mark.asyncio
async def test_shcmd2(srv,cl,repl):
    for i in range(10):
        x0 = await cl.callm(repl,'shcmd', 'echo %d ->%d' % (i,-i), expected_returncode=-i)


@pytest.mark.asyncio
async def test_shcmd3(srv,cl,repl):
    for i in range(10):
        x0 = await cl.callm(repl,'shcmd', 'asciiart ->%d' % i, expected_returncode=i)


@pytest.mark.asyncio
@pytest.mark.async_timeout(20)
async def test_shcmd4(srv,cl,repl):
    for i in range(5):
        x0 = await cl.callm(repl,'shcmd', 'sleep 2 ->%d' % i, expected_returncode=i)


@pytest.mark.asyncio
@pytest.mark.async_timeout(20)
async def test_expect0(srv,cl,repl):
    loop = asyncio.get_event_loop()
    cd = loop.create_task(cl.callm(repl,'cmd', 'countdown 1000 zorg'))
    exp899 = loop.create_task(cl.callm(repl,'expect', '899 - zorg'))
    exp859 = loop.create_task(cl.callm(repl,'expect', '859 - zorg'))
    done,_ = await asyncio.wait([exp899, exp859], timeout=7, return_when=asyncio.ALL_COMPLETED)
    assert len(done)==2
    await cl.callm(repl, 'resetp')
    await cl.callm(repl,'tx', b'\x03')
    x = await cd
    print(x.out)
    if ('899 - zorg' in x.out) and ('859 - zorg' in x.out) and ('-- Cancelled --' in x.out):
        pass
    else:
        raise AssertionError('unexected content')


@pytest.mark.asyncio
@pytest.mark.async_timeout(10)
async def test_trunc0(srv,cl,repl):
    loop = asyncio.get_event_loop()
    x0 = await cl.callm(repl,'cmd', 'countdown 50 zorg', size=250, truncation_method='keepbottom')
    x0.disableTruncationChecking()
    print(x0.text)
    kinds = x0.getKinds()
    assert kinds.count('truncated')==1
    for i in range(16):
        s="%d - zorg" % i
        assert s in x0.out


@pytest.mark.asyncio
async def test_trunc1(srv,cl,repl):
    loop = asyncio.get_event_loop()
    x0 = await cl.callm(repl,'cmd', 'countdown 50 zorg', size=250, truncation_method='removemiddle')
    x0.disableTruncationChecking()
    print(x0.text)
    kinds = x0.getKinds()
    assert kinds.count('truncated')==1
    l = list(range(6)) + list(range(44,50))
    print(l)
    for i in l:
        s="%d - zorg" % i
        assert s in x0.out


@pytest.mark.asyncio
async def test_trunc2(srv,cl,repl):
    loop = asyncio.get_event_loop()
    x0 = await cl.callm(repl,'cmd', 'countdown 50 zorg', size=250, truncation_method='removemiddle')
    with pytest.raises(ValueError):
        text = x0.text


@pytest.mark.asyncio
async def test_prolog0(srv,cl,repl):
    data = []
    def rx(datap):
        data.append(datap.decode())
    cl.bind('job::replicant0<RX>', rx)
    await cl.subscribe('*')
    await asyncio.sleep(1)
    data = ''.join(data)
    print(data)
    assert 'Replicant is a dummy read-eval-print-loop system for testing purpose' in data


@pytest.mark.asyncio
async def test_prolog1(srv,cl,repl):
    data = []
    def _(job,old,new):
        data.append("%s >>> %s" % (old,new))
    cl.bind('job::replicant0<status-changed>', _)
    await cl.subscribe('*')
    await asyncio.sleep(1)
    assert (len(data)==1)  and  (data[0]=='unknown >>> started')
