import asyncio
import time
import pytest
from olfactory.pytest.dec import *
from olfactory.pytest.ocp import *
from olfactory.pytest.secu import *
from olfactory.pytest.plg import *
from olfactory import excdef, config
from olfactory.ocp import client, server


PluginManagerFixture('plgmng').inject()
OCPServerFixture('srv').inject()
adec.inject()
pdec.inject()
OCPNodeFactoryFixture('onff').inject()


def get_listen_url():
    conf = config.Config()
    url = conf.getItem('test_ocp_listen_url')
    return url


@pytest.mark.asyncio
async def testauth_internalerror0(adec,srv,onff):
    cl = onff.instanciateClient(identity='cl', authparams=dict(force='ierr', msg='grand blabla'))
    with pytest.raises(excdef.AuthError) as excinfo:
        await cl.connect()
    assert str(excinfo.value) == 'internal error'


@pytest.mark.asyncio
async def testauth_internalerror1(adec,srv,onff):
    cl = onff.instanciateClient(identity='cl', authparams=dict(force='retnum', msg='42'))
    with pytest.raises(excdef.AuthError) as excinfo:
        await cl.connect()
    assert str(excinfo.value) == 'internal error'


@pytest.mark.asyncio
async def testauth_internalerror2(adec,srv,onff):
    cl = onff.instanciateClient(identity='cl')
    with pytest.raises(excdef.AuthError) as excinfo:
        await cl.connect(authparams=dict(force='ierr', msg='grand blabla'))
    assert str(excinfo.value) == 'internal error'


@pytest.mark.asyncio
async def testauth_internalerror3(adec,srv,onff):
    cl = onff.instanciateClient(identity='cl')
    with pytest.raises(excdef.AuthError) as excinfo:
        await cl.connect(authparams=dict(force='retnum', msg='42'))
    assert str(excinfo.value) == 'internal error'


@pytest.mark.asyncio
async def testauth_stateerror0(adec,srv,onff):
    cl = onff.instanciateClient(identity='cl',username='molo',authparams=dict(username='zorg'))
    with pytest.raises(excdef.StateError) as excinfo:
        await cl.connect()


@pytest.mark.asyncio
async def testauth_stateerror1(adec,srv,onff):
    cl = onff.instanciateClient(identity='cl',password='molo',authparams=dict(password='zorg'))
    with pytest.raises(excdef.StateError) as excinfo:
        await cl.connect()


@pytest.mark.asyncio
async def testauth_valid0(adec,srv,onff):
    cl = onff.instanciateClient(identity='cl', authparams=dict(username='groz', password='secret'))
    await cl.connect()
    await asyncio.sleep(1)


@pytest.mark.asyncio
async def testauth_valid1(adec,srv,onff):
    cl = onff.instanciateClient(identity='cl')
    await cl.connect(authparams=dict(username='groz', password='secret'))
    await asyncio.sleep(1)


@pytest.mark.asyncio
async def testauth_getinfo0(adec,srv,onff):
    cl = onff.instanciateClient(identity='cl', authparams=dict(username='groz', password='secret'))
    await cl.connect()
    assert cl.getAuthInfo() == {}


@pytest.mark.asyncio
async def testauth_getinfo1(adec,srv,onff):
    cl = onff.instanciateClient(identity='cl', authparams=dict(username='groz', password='secret', retpub='yes', retpriv='yes'))
    await cl.connect()
    assert cl.getAuthInfo() == {'public': 'public'}


@pytest.mark.asyncio
async def testauth_getinfo2(adec,srv,onff):
    cl = onff.instanciateClient(identity='cl', authparams=dict(username='groz', password='secret', retpriv='yes'))
    await cl.connect()
    assert cl.getAuthInfo() == {}


@pytest.mark.asyncio
async def testauth_getinfo3(adec,srv,onff):
    cl = onff.instanciateClient(identity='cl', username='groz', password='secret')
    await cl.connect()
    assert cl.getAuthInfo() == {}


@pytest.mark.asyncio
async def testauth_getinfo4(adec,srv,onff):
    cl = onff.instanciateClient(identity='cl', username='groz', authparams=dict(retpub='yes', retpriv='yes', password='secret'))
    await cl.connect()
    assert cl.getAuthInfo() == {'public': 'public'}


@pytest.mark.asyncio
async def testauth_getinfo5(adec,srv,onff):
    cl = onff.instanciateClient(identity='cl', username=None, password='secret', authparams=dict(retpriv='yes', username='groz'))
    await cl.connect()
    assert cl.getAuthInfo() == {}


@pytest.mark.asyncio
async def testauth_invalid0(adec,srv,onff):
    cl = onff.instanciateClient(identity='cl', authparams=dict(force='retmsg', msg='invalid token'))
    with pytest.raises(excdef.AuthError) as excinfo:
        await cl.connect()
    assert str(excinfo.value) == 'invalid token'


@pytest.mark.asyncio
async def testauth_invalid1(adec,srv,onff):
    cl = onff.instanciateClient(identity='cl', authparams=dict(force='aerr', msg='invalid login'))
    with pytest.raises(excdef.AuthError) as excinfo:
        await cl.connect()
    assert str(excinfo.value) == 'invalid login'


@pytest.mark.asyncio
async def testauth_invalid2(adec,srv,onff):
    cl = onff.instanciateClient(identity='cl', authparams=dict(force='retnone'))
    with pytest.raises(excdef.AuthError) as excinfo:
        await cl.connect()
    assert str(excinfo.value) == 'authentification failed'


@pytest.mark.asyncio
async def testauth_invalid3(adec,srv,onff):
    cl = onff.instanciateClient(identity='cl')
    with pytest.raises(excdef.AuthError) as excinfo:
        await cl.connect(authparams=dict(force='retmsg', msg='invalid token'))
    assert str(excinfo.value) == 'invalid token'


@pytest.mark.asyncio
async def testauth_invalid4(adec,srv,onff):
    cl = onff.instanciateClient(identity='cl')
    with pytest.raises(excdef.AuthError) as excinfo:
        await cl.connect(authparams=dict(force='aerr', msg='invalid login'))
    assert str(excinfo.value) == 'invalid login'


@pytest.mark.asyncio
async def testauth_invalid5(adec,srv,onff):
    cl = onff.instanciateClient(identity='cl')
    with pytest.raises(excdef.AuthError) as excinfo:
        await cl.connect(authparams=dict(force='retnone'))
    assert str(excinfo.value) == 'authentification failed'


async def absign_auth_handler(*, server,socket,p0,p1,p2,p3):
    pass

absign = DecFixture('absign')
@absign.plg_trigger
def _absign_init(server:'init'):
    server.setAuthHandler(absign_auth_handler)
absign.inject()


@pytest.mark.asyncio
async def testauth_internalerror5(absign,srv,onff):
    cl = onff.instanciateClient(identity='cl', authparams=dict(p0='A', p1='B'))
    with pytest.raises(excdef.AuthError) as excinfo:
        await cl.connect()
    assert str(excinfo.value) == 'internal error'


@pytest.mark.asyncio
async def testperm_internalerror(pdec,srv,onff):
    cl = onff.instanciateClient(identity='cl', authparams=dict(force='ierr',msg='blablabla'))
    await cl.connect()
    with pytest.raises(excdef.PermError) as excinfo:
        await cl.call('$delayed_div', 1, 84, 2)
    assert str(excinfo.value) == 'internal error'


@pytest.mark.asyncio
async def testperm_granted0(pdec,srv,onff):
    cl = onff.instanciateClient(identity='cl', authparams=dict(force='rettrue'))
    await cl.connect()
    rval = await cl.call('$delayed_div', 1, 84, 2)
    assert rval==42


@pytest.mark.asyncio
async def testperm_granted1(pdec,srv,onff):
    cl = onff.instanciateClient(identity='cl', authparams=dict(evalstr='name=="$delayed_div" and frmtype=="CALL"'))
    await cl.connect()
    rval = await cl.call('$delayed_div', 1, 84, 2)
    assert rval==42


@pytest.mark.asyncio
async def testperm_granted2(pdec,srv,onff):
    cl = onff.instanciateClient(identity='cl')
    for s0 in generate_utf8_strings(num=100,size=8):
        await cl.connect(authparams=dict(force='rettrue',s=s0))
        d = await cl.call('$get_authinfo')
        await cl.disconnect()
        s1 = d.pop('s')
        print((s0,s1))
        assert s0==s1


@pytest.mark.asyncio
async def testperm_denied0(pdec,srv,onff):
    cl = onff.instanciateClient(identity='cl', authparams=dict(evalstr='frmtype!="CALL"'))
    await cl.connect()
    with pytest.raises(excdef.PermError) as excinfo:
        await cl.call('$delayed_div', 1, 84, 2)
    assert str(excinfo.value) == 'permission denied'


def perm_handler(**kwargs):
    return True


ponly = DecFixture('ponly')
@ponly.plg_trigger
def _ponly_init(server:'init'):
    server.setPermHandler(perm_handler)
ponly.inject()


@pytest.mark.asyncio
async def testperm_permhandleronly(ponly,srv,onff):
    cl = onff.instanciateClient(identity='cl')
    await cl.connect()
    rval = await cl.call('$delayed_div', 1, 84, 2)
    assert rval==42


@pytest.mark.asyncio
async def testperm_pub0(pdec,srv,onff):
    pl = []
    cl = onff.instanciateClient(identity='cl',  authparams=dict(evalstr='frmtype=="CALL"  or  (frmtype=="PUBLISH" and name=="zorg")'))
    await cl.connect()
    cl.bind('*', pl.append)
    sid1 = await cl.subscribe('*')
    srv.publishSoon('zorg', 0)
    srv.publishSoon('molo', 1)
    srv.publishSoon('zorg', 2)
    await cl.unsubscribe(sid1)
    await asyncio.sleep(2)
    assert pl==[0,2]
