import pytest
import asyncio
import os
import sys
from olfactory.pytest.ocp import *
from olfactory.pytest.plg import *
from olfactory import config


PORT = 9666


OCPServerFixture('srv').inject()
OCPClientFixture('cl').inject()
OCPClientFixture('lgcl', url=f'ws://127.0.0.1:{PORT}').inject()
PluginManagerFixture('plgmng').inject()


@pytest.fixture
async def lgchan(srv,cl):
    kwargs = {}
    kwargs['name'] = 'lgchan'
    kwargs['class'] = 'proc'
    kwargs['jobexts'] = ['repl']
    conf = config.Config()
    topdir = conf.getTopDir()
    args = [f'{topdir}/tests/examples/logserver/logserver.py', f'{PORT}']
    chan = await cl.xcall('job::run', args=args, kwargs=kwargs)
    await cl.subscribe('job::lgchan.RX')
    await cl.callm(chan, 'expect', 'READY')
    async def _():
        h = await cl.callm(chan, 'hist')
        print(h.text)
        return h.text
    chan.hist = _
    yield chan
    await cl.callm(chan, 'tx', '\x03')


@pytest.mark.asyncio
async def test0(lgchan,lgcl):
    await lgcl.call('parse_logging_args', '--lLd --lCd --lA molo.* --lD molo.groz')
    msg = 'bla bla 12345'
    await lgcl.call('$log', msg, name='molo.pouet', level='debug')
    await lgcl.call('$log', msg, name='molo.groz', level='debug')
    await lgcl.call('$log', msg, name='molo.zorg', level='debug')
    await asyncio.sleep(1)
    h = await lgchan.hist()
    assert h.count(msg)==2


@pytest.mark.asyncio
async def test1(lgchan,lgcl):
    await lgcl.call('parse_logging_args', '--lL warning --lCa')
    msg = 'bla bla 12345'
    await lgcl.call('$log', msg, name='molo.pouet', level='debug')
    await lgcl.call('$log', msg, name='molo.pouet', level='info')
    await lgcl.call('$log', msg, name='molo.pouet', level='warning')
    await lgcl.call('$log', msg, name='molo.pouet', level='error')
    await asyncio.sleep(1)
    h = await lgchan.hist()
    assert h.count("molo.pouet")==2
    assert h.count(msg)==2


@pytest.mark.asyncio
async def test2(lgchan,lgcl):
    await lgcl.call('parse_logging_args', '--log-molo-debug-only')
    msg = 'bla bla 12345'
    await lgcl.call('$log', msg, name='molo.pouet', level='debug')
    await lgcl.call('$log', msg, name='molo.pouet', level='debug')
    await lgcl.call('$log', msg, name='molo.groz', level='debug')
    await lgcl.call('$log', msg, name='zorg.ccc', level='debug')
    await asyncio.sleep(1)
    h = await lgchan.hist()
    assert h.count(msg)==3
