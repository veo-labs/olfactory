import random
import time
import unittest
import asyncio
import re
import os
import pytest
import subprocess
from olfactory.decorators import *
from olfactory.ocp import client
from olfactory.pytest.ocp import *
from olfactory.pytest.plg import *
from olfactory import config, argphlp


OCPServerFixture('srv').inject()
PluginManagerFixture('plgmng').inject()
OCPClientFixture('cl').inject()


class CTError(Exception):
    pass


@ocp_class
class Fraction:
    _instances = set()

    def __init__(self, n, d):
        self._instances.add(self)
        self.n = n
        self.d = d

    @ocp_method
    def asFloat(self):
        return self.n/self.d


def make_fraction(n,d):
    return Fraction(n,d)


def fraction_as_float(frac):
    return frac.asFloat()


@pytest.fixture
async def repl(cl):
    kwargs = {}
    kwargs['class'] = 'replicant'
    kwargs['jobexts'] = ['repl','sh']
    chan = await cl.xcall('job::run', kwargs=kwargs)
    await cl.callm(chan,'syncp')
    return chan


async def run_ct(extra_args=None,eval=True):
    conf = config.Config()
    cmd = 'olfactory-ct -u %s' % conf.getItem('test_ocp_connect_url')
    if extra_args is not None:
        cmd += ' ' + extra_args
    print()
    print()
    print('* Run CT with:')
    print("    args   = %r" % cmd)
    print()
    proc = await asyncio.create_subprocess_shell(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    stdout, _ = await proc.communicate()
    returncode = await proc.wait()
    stdout = stdout.decode()
    topln = ('-'*40) + ' ct-output ' + ('-'*40)
    botln = '-' * len(topln)
    print(topln)
    print(stdout)
    print(botln)
    print()
    print()
    if returncode != 0:
        raise CTError('process exits with status %d' % returncode)
    if 'Traceback (most recent call last):' in stdout:
        raise CTError('got python traceback in output')
    if eval:
        stdout = stdout.split('\n')
        result = []
        for ln in stdout:
            if ln.strip()=='':
                continue
            result += [ argphlp.json_loads(ln) ]
        return result
    else:
        return stdout


async def run_ct_and_eval_one_line(extra_args):
    l = await run_ct(extra_args)
    if len(l) != 1:
        raise AssertionError(f'run_ct() returns {len(l)} line(s)')
    return l[0]


@pytest.mark.asyncio
async def test_argp0(srv,cl):
    d = await run_ct_and_eval_one_line("ocp.call -f '$return_args_and_kwargs'")
    assert d=={'args': [], 'kwargs': {}}


@pytest.mark.asyncio
async def test_argp1(srv,cl):
    d = await run_ct_and_eval_one_line("ocp.call -f '$return_args_and_kwargs' -o a=A,b=B,c,-d,+e,~f")
    assert d=={'args': [], 'kwargs': {'a': 'A', 'b': 'B', 'c': True, 'd': False, 'e': True, 'f': None}}


@pytest.mark.asyncio
async def test_argp2(srv,cl):
    d = await run_ct_and_eval_one_line("ocp.call -f '$return_args_and_kwargs' -- 1 2 3 molo")
    assert d=={'args': ['1','2','3','molo'], 'kwargs': {}}


@pytest.mark.asyncio
async def test_argp3(srv,cl):
    d = await run_ct_and_eval_one_line("ocp.call -f '$return_args_and_kwargs' -a true -a 2")
    assert d=={'args': [True,2], 'kwargs': {}}


@pytest.mark.asyncio
async def test_argp4(srv,cl):
    d = await run_ct_and_eval_one_line("ocp.call -f '$return_args_and_kwargs' -k '{\"one\":1,\"two\":2,\"three\":false}'")
    assert d=={'args': [], 'kwargs': {"one": 1, "two": 2, "three": False}}


@pytest.mark.asyncio
async def test_argp5(srv,cl):
    d = await run_ct_and_eval_one_line(r"ocp.call -f '$return_args_and_kwargs' -aS  '%7a%6f%72%67%0a'")
    assert d=={'args': ['zorg\n'], 'kwargs': {}}


@pytest.mark.asyncio
async def test_argp6(srv,cl):
    d = await run_ct_and_eval_one_line(r"ocp.call -f '$return_args_and_kwargs' -aB 'ls /%0d'")
    assert d=={'args': [b'ls /\r'], 'kwargs': {}}


@pytest.mark.asyncio
async def test_argp7(srv,cl):
    d = await run_ct_and_eval_one_line("ocp.call -f '$return_args_and_kwargs' -p one=1 -p two=2 -p three=false")
    assert d=={'args': [], 'kwargs': {"one": 1, "two": 2, "three": False}}


@pytest.mark.asyncio
async def test_argp8(srv,cl):
    srv.expose('Fraction',Fraction)
    frac = await run_ct_and_eval_one_line("ocp.call -f 'Fraction' -a 3 -a 4")
    flt = await run_ct_and_eval_one_line(f"ocp.call -l '{frac.getLabel()}' -f 'asFloat' ")
    assert flt==0.75


@pytest.mark.asyncio
async def test_argp9(srv,cl):
    srv.expose('make_fraction',make_fraction)
    srv.expose('fraction_as_float',fraction_as_float)
    frac = await run_ct_and_eval_one_line("ocp.call -f 'make_fraction' -a 3 -a 4")
    flt = await run_ct_and_eval_one_line(f"""ocp.call -f 'fraction_as_float' -a '{{"_class": "IRef", "label": "{frac.getLabel()}"}}'""")
    assert flt==0.75


@pytest.mark.asyncio
async def test_argp10(srv,cl):
    d = await run_ct_and_eval_one_line("""ocp.call -f '$return_args_and_kwargs' -a '{"_class": "bytes", "repr": "zorg%0d"}'""")
    assert d=={'args': [b'zorg\r'], 'kwargs': {}}


@pytest.mark.asyncio
async def test_argp11(srv,cl):
    d = await run_ct_and_eval_one_line("""ocp.call -f '$return_args_and_kwargs' -a '{"_class": "IRef", "label": "zorg"}'""")
    assert sorted(d.keys())==['args','kwargs']
    assert d['kwargs']=={}
    assert len(d['args'])==1
    assert d['args'][0].getLabel()=='zorg'


@pytest.mark.asyncio
async def test_argp12(srv,cl):
    d = await run_ct_and_eval_one_line("""dbg.nsargs -o a=A,b=B,c,-d,+e,~f --kwa0""")
    assert sorted(d.keys())==['args','kwargs']
    assert d['kwargs']=={}
    assert len(d['args'])==1
    assert d['args'][0]=={'a':'A', 'b':'B', 'c':True, 'd':False, 'e':True, 'f':None}


@pytest.mark.asyncio
async def test_argp13(srv,cl):
    d = await run_ct_and_eval_one_line("""dbg.nsargs --cwd""")
    assert sorted(d.keys())==['args','kwargs']
    assert d['kwargs']=={'cwd': os.getcwd()}
    assert d['args']==[]


@pytest.mark.asyncio
async def test_argp14(srv,cl):
    d = await run_ct_and_eval_one_line("""dbg.nsargs -x a,b,c""")
    assert sorted(d.keys())==['args','kwargs']
    assert d['kwargs']=={'jobexts': ['a','b','c']}
    assert d['args']==[]


@pytest.mark.asyncio
async def test_argp15(srv,cl):
    d = await run_ct_and_eval_one_line("""dbg.nsargs -x a""")
    assert sorted(d.keys())==['args','kwargs']
    assert d['kwargs']=={'jobexts': ['a']}
    assert d['args']==[]


@pytest.mark.asyncio
async def test_argp16(srv,cl):
    d = await run_ct_and_eval_one_line("""dbg.nsargs -o a=A,b=B --aa0 -- x y z""")
    assert sorted(d.keys())==['args','kwargs']
    assert d['kwargs']=={}
    assert len(d['args'])==1
    assert d['args'][0]== ['x','y','z']


@pytest.mark.asyncio
async def test_job0(srv,cl,repl):
    info = await run_ct_and_eval_one_line('job.info -n replicant0')
    assert info=={'master_label': 'job::replicant0', 'name': 'replicant0', 'ncols': 80, 'nlines': 24, 'status': 'started'}


@pytest.mark.asyncio
async def test_job1(srv,cl):
    await run_ct_and_eval_one_line('job.create -o class=replicant')
    info = await run_ct_and_eval_one_line('job.info -n replicant0')
    assert info=={'master_label': 'job::replicant0', 'name': 'replicant0', 'ncols': 80, 'nlines': 24, 'status': 'stopped'}


@pytest.mark.asyncio
async def test_job2(srv,cl):
    await run_ct('job.create -o class=replicant', eval=False)
    await run_ct('job.start -n replicant0', eval=False)
    info = await run_ct_and_eval_one_line('job.info -n replicant0')
    assert info=={'master_label': 'job::replicant0', 'name': 'replicant0', 'ncols': 80, 'nlines': 24, 'status': 'started'}


@pytest.mark.asyncio
async def test_job3(srv,cl):
    await run_ct('job.create -o class=replicant', eval=False)
    await run_ct('job.create -o class=replicant', eval=False)
    l = await run_ct_and_eval_one_line('job.list')
    assert(len(l)==2)
    l2 = sorted(map(lambda x: x.getLabel(), l))
    assert l2==['job::replicant0', 'job::replicant1']


@pytest.mark.asyncio
async def test_job4(srv):
    await run_ct_and_eval_one_line('job.run -o class=replicant')
    d = await run_ct_and_eval_one_line("ocp.subscribe -c 1 -t 'job::replicant0<*>'")
    print(d)
    assert sorted(d.keys())==['T', 'TYPE', 'args','kwargs', 'topic']
    assert d['kwargs']=={}
    assert len(d['args'])==3
    assert d['args']==['job::replicant0','unknown','started']


@pytest.mark.asyncio
async def test_job5(srv):
    await run_ct_and_eval_one_line('job.run -o class=replicant')
    l = await run_ct("ocp.subscribe -c 2 -t 'job::replicant0<*>'")
    print(l)
    assert len(l)==2
    assert sorted(l[0].keys())==['T', 'TYPE', 'args','kwargs', 'topic']
    assert sorted(l[1].keys())==['T', 'TYPE', 'subscription_id']


@pytest.fixture
def calc(srv,repl):
    class Calc:
        def cmd(self):
            return 'calc (1+2+3)*111'
        async def finish(self):
            out = await run_ct('channel.hist -n replicant0', eval=False)
            assert out.count('replicant0>')==2
            assert out.count('666')==1
    return Calc()


@pytest.mark.asyncio
async def test_chan0(srv,cl,repl,calc):
    await cl.callm(repl,'cmd', calc.cmd())
    await calc.finish()


@pytest.mark.asyncio
async def test_chan1(calc):
    await run_ct("channel.cmd -n replicant0 -- '%s'" % calc.cmd(), eval=False)
    await calc.finish()


@pytest.mark.asyncio
async def test_chan2(calc):
    await run_ct("channel.tx -n replicant0 -aS '%s\r'" % calc.cmd(), eval=False)
    await calc.finish()


@pytest.mark.asyncio
async def test_chan3(calc):
    await run_ct("channel.tx -n replicant0 -aB '%s\r'" % calc.cmd(), eval=False)
    await calc.finish()
