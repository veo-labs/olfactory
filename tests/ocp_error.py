import asyncio
import pytest
from olfactory import excdef
from olfactory.pytest.ocp import *
from olfactory.pytest.err import *
from olfactory.pytest.plg import *


PluginManagerFixture('plgmng').inject()
OCPServerFixture('srv').inject()
OCPNodeFactoryFixture('onff').inject()


class MyError(Exception):
    pass


@pytest.mark.asyncio
async def test_local0():
    with raises(MyError):
        raise MyError


@pytest.mark.asyncio
async def test_local1():
    with pytest.raises(AssertionError):
        with raises(MyError):
            pass


@pytest.mark.asyncio
async def test_local2():
    with raises(MyError):
        raise MyError('zorg')


@pytest.mark.asyncio
async def test_local3():
    with raises(MyError,msg_contains='zorg'):
        raise MyError('zorg')


@pytest.mark.asyncio
async def test_local4():
    with pytest.raises(AssertionError):
        with raises(MyError,msg_contains='groz'):
            raise MyError('zorg')


@pytest.mark.asyncio
async def test_local5():
    exc = MyError('zorg')
    exc.olfactory_fields = dict(molo='groz')
    with raises(MyError,msg_contains='zorg',olfactory_fields=dict(molo='groz')):
        raise exc


@pytest.mark.asyncio
async def test_local6():
    exc = MyError('zorg')
    exc.olfactory_fields = dict(molo='groz')
    with pytest.raises(AssertionError):
        with raises(MyError,msg_contains='zorg',olfactory_fields=dict(molo='GROZ')):
            raise exc


@pytest.mark.asyncio
async def test_local7():
    exc = excdef.NameError('zorg')
    exc.olfactory_fields['molo'] = 'ZORG'
    with raises(excdef.NameError,msg_contains='zorg',olfactory_fields=dict(molo='ZORG',serialized=False)):
        raise exc


@pytest.mark.asyncio
async def test_remote0(srv,onff):
    def my_throw():
        raise AssertionError('zorg')
    srv.expose('my_throw', my_throw)
    def error_handler(error_fields,*triplet):
        error_fields['msg_upper'] = error_fields['msg'].upper()
    srv.setErrorHandler(error_handler)
    cl = onff.instanciateClient()
    await cl.connect()
    with raises(AssertionError,msg_contains='zorg',olfactory_fields=dict(msg_upper='ZORG',serialized=True,name='AssertionError')):
        await cl.call('my_throw')


@pytest.mark.asyncio
async def test_remote1(srv,onff):
    def my_throw():
        e = excdef.LabelError('groz')
        e.olfactory_fields['molo'] = 'azerty'
        raise e
    srv.expose('my_throw', my_throw)
    cl = onff.instanciateClient()
    await cl.connect()
    with raises(excdef.LabelError,msg_contains='groz',olfactory_fields=dict(molo='azerty',msg='groz',serialized=True,name='olfactory.excdef.LabelError')):
        await cl.call('my_throw')
