#!/usr/bin/env python3

import sys
import argparse
import asyncio
import logging
from olfactory import plugin,loghlp
from olfactory.ocp import server
from olfactory.decorators import *


logger = logging.getLogger(__name__)


class LogOptsAppender:
    def append(self,ignored):
        opts = loghlp.Options()
        opts.clearRules()
        opts.setLevel('debug')
        opts.setDefaultDecision(loghlp.DECISION_DROP)
        opts.addAcceptRule('molo.*')


class LoggingArgumentParser(argparse.ArgumentParser):
    def __init__(self):
        super().__init__()
        loghlp.tune_argparser(self)
        a = LogOptsAppender()
        self.add_argument('--log-molo-debug-only', default=a, action="append_const", const=None, help="shortcut for '--lCd --lA molo.* --lLd'")


def parse_logging_args(s):
    args = []
    for a in s.split(' '):
        if a=='':
            continue
        args.append(a)
    p = LoggingArgumentParser()
    opts = loghlp.Options()
    opts.setStreamname('stdout')
    ns = p.parse_args(args)
    loghlp.install_from_ns(ns)


async def setup():
    m = plugin.Manager()
    m.importAll()
    try:
        port = int(sys.argv[1])
    except:
        raise AssertionError('listening port expected as first argument')
    url = "ws://127.0.0.1:%d" % port
    s = server.Server(url, identity='lgsrv')
    s.expose('parse_logging_args', parse_logging_args)
    parse_logging_args(' '.join(sys.argv[2:]))
    await s.listen()
    print('READY')
    print()
    return s


if __name__=='__main__':
    loop = asyncio.get_event_loop()
    s = loop.run_until_complete(setup())
    loop.run_forever()
