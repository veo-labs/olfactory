import asyncio
import time
import pytest
from urllib.parse import quote as encodeURIComponent
from olfactory.pytest.wtr import *
from olfactory.pytest.dec import *
from olfactory.pytest.ocp import *
from olfactory.pytest.secu import *
from olfactory.pytest.plg import *
from olfactory.pytest.err import *
from olfactory import excdef, config
from olfactory.ocp import client, server


OCPServerFixture('srv').inject()
adec.inject()
pdec.inject()
WebTestRunnerFixture('wtr').inject()
PluginManagerFixture('plgmng').inject()



JSCODE_BEGINNING = '''
    import {{__kwargtrans__ as $kw}} from '/olfactory/org.transcrypt.__runtime__.js';
    import * as client from "/olfactory/olfactory.ocp.client.js";
    export async function test()
    {{
        var authparams = {{ }};
        var authinfo;
        authparams['force'] = '{force}';
        authparams['msg'] = '{msg}';
        authparams['retpub'] = 'yes';
        authparams['retpriv'] = 'yes';
        authparams['username'] = '{username}';
        authparams['password'] = '{password}';
'''

JSCODE_v1 = '''
        var cl = client.Client('{url}', $kw({{'identity': 'JScli', 'authparams': authparams}}));
        await cl.connect();
'''

JSCODE_v2 = '''
        var url = '{url}';
        url = url.replace('ws://', 'ws://{username}:{password}@');
        delete authparams['username'];
        delete authparams['password'];
        var cl = client.Client(url, $kw({{'identity': 'JScli', 'authparams': authparams}}));
        await cl.connect();
'''

JSCODE_v3 = '''
        var cl = client.Client('{url}', $kw({{'identity': 'JScli'}}));
        await cl.connect($kw({{'authparams': authparams}}));
'''

JSCODE_v4 = '''
        var cl = client.Client('ws://__invalid__/zorg', $kw({{'identity': 'JScli'}}));
        await cl.connect('{url}', $kw({{'authparams': authparams}}));
'''

JSCODE_v5 = '''
        var cl = client.Client();
        await cl.connect('{url}', $kw({{'authparams': authparams}}));
'''

JSCODE_ENDING = '''
        authinfo = cl.getAuthInfo().py_items();
        return authinfo;
    }}
'''


@pytest.mark.asyncio
@pytest.mark.parametrize('force,msg,expected_msg,username,password,jscode', [
        ('ierr',   'x',       'internal error', 'smith', 'azerty', JSCODE_v1),
        ('aerr',   'zorg',    'zorg',           'smith', 'azerty', JSCODE_v1),
        ('x',      'x',       None,             'groz',  'secret', JSCODE_v1),
        ('ierr',   'x',       'internal error', 'smith', 'azerty', JSCODE_v2),
        ('aerr',   'zorg',    'zorg',           'smith', 'azerty', JSCODE_v2),
        ('x',      'x',       None,             'groz',  'secret', JSCODE_v2),
        ('ierr',   'x',       'internal error', 'smith', 'azerty', JSCODE_v3),
        ('aerr',   'zorg',    'zorg',           'smith', 'azerty', JSCODE_v3),
        ('x',      'x',       None,             'groz',  'secret', JSCODE_v3),
        ('ierr',   'x',       'internal error', 'smith', 'azerty', JSCODE_v4),
        ('aerr',   'zorg',    'zorg',           'smith', 'azerty', JSCODE_v4),
        ('x',      'x',       None,             'groz',  'secret', JSCODE_v4),
        ('ierr',   'x',       'internal error', 'smith', 'azerty', JSCODE_v5),
        ('aerr',   'zorg',    'zorg',           'smith', 'azerty', JSCODE_v5),
        ('x',      'x',       None,             'groz',  'secret', JSCODE_v5),
    ],
    ids=[
        'ierr-v1','aerr-v1','valid-v1',
        'ierr-v2','aerr-v2','valid-v2',
        'ierr-v3','aerr-v3','valid-v3',
        'ierr-v4','aerr-v4','valid-v4',
        'ierr-v5','aerr-v5','valid-v5',
        ]
)
async def test_kwargs(adec,srv,wtr,force,msg,expected_msg,username,password,jscode):
    conf = config.Config()
    url = conf.getItem('test_ocp_connect_url')
    text = JSCODE_BEGINNING + jscode + JSCODE_ENDING
    text = text.format(**locals())
    await wtr.setText(text)
    if expected_msg is None:
        authinfo = await wtr.execute(timeout=2)
        assert authinfo==[['public','public']]
    else:
        with raises(excdef.AuthError,msg_contains=expected_msg):
            await wtr.execute(timeout=2)


utf8_strings = generate_utf8_strings(num=20,size=20)
@pytest.mark.asyncio
@pytest.mark.parametrize('ustr', utf8_strings, ids=list(map(lambda idx: f"#{idx:02d}", range(len(utf8_strings)))))
async def test_utf8(pdec,srv,wtr,ustr):
    enc_ustr = encodeURIComponent(ustr)
    conf = config.Config()
    url = conf.getItem('test_ocp_connect_url')
    text = f'''
    import {{__kwargtrans__ as $kw}} from '/olfactory/org.transcrypt.__runtime__.js';
    import * as client from "/olfactory/olfactory.ocp.client.js";
    export async function test()
    {{
        var ustr = decodeURIComponent("{enc_ustr}");
        var authparams = {{'ustr': ustr, 'force': 'rettrue' }};
        var cl = client.Client('{url}', $kw({{'authparams': authparams}}));
        await cl.connect();
        var d = await cl.call('$get_authinfo');
        return d;
    }}
    '''
    await wtr.setText(text)
    d = await wtr.execute(timeout=2)
    ustr2 = d.pop('ustr')
    print(ustr,ustr2)
    assert ustr2==ustr
