import asyncio
import time
import pytest
from olfactory.pytest.ocp import *
from olfactory.pytest.plg import *


PluginManagerFixture('plgmng').inject()


class Publi:
    def __repr__(self):
        return f'<Publi {self.__dict__!r}>'


class Handler:
    def __init__(self, cl):
        self.publications = []
        self.sum = 0
        self.keys_counter = {}

    def _cbStore(self, frame):
        print("_cbStore(): %r" % frame)
        p = Publi()
        p.topic = frame[1]
        p.args = frame[2]
        p.kwargs = frame[3]
        self.publications.append(p)

    def _cbCountKeys(self, *args, **kwargs):
        for k in kwargs.keys():
            try:
                self.keys_counter[k] += 1
            except KeyError:
                self.keys_counter[k] = 1

    def _cbAccumulate(self, value):
        print("_cbAccumulate(): %r" % value)
        self.sum += value

    def _cbError(self, *args, **kwargs):
        msg = f"args={args!r} kwargs={kwargs!r}"
        print(f"_cbError(): {msg}")
        raise AssertionError(msg)


OCPServerFixture('srv').inject()
client_fixture = OCPClientFixture('cl').inject()


@pytest.fixture
def hdlr():
    return Handler(client_fixture)


@pytest.mark.asyncio
async def test00(srv,cl,hdlr):
    sid = await cl.subscribe('*')
    rval = await cl.unsubscribe(sid)
    assert rval==True


@pytest.mark.asyncio
async def test01(srv,cl,hdlr):
    rval = await cl.unsubscribe(0xdeadface)
    assert rval==False


@pytest.mark.asyncio
async def test02(srv,cl,hdlr):
    sid1 = await cl.subscribe('molo.*')
    sid2 = await cl.subscribe('zorg.*')
    sid3 = await cl.subscribe('abc.xyz')
    rvals = []
    rvals += [await cl.unsubscribe(sid1)]
    rvals += [await cl.unsubscribe(sid2)]
    rvals += [await cl.unsubscribe(sid3)]
    assert rvals==[True]*3


@pytest.mark.asyncio
async def test03(srv,cl,hdlr):
    cl.bind('*', hdlr._cbStore, raw=True)
    subscription_id = await cl.subscribe('*')
    num = await cl.unsubscribe(subscription_id)
    assert num==1


@pytest.mark.asyncio
async def test04(srv,cl,hdlr):
    cl.bind('*', hdlr._cbStore, raw=True)
    subscription_id = await cl.subscribe('*')
    await cl.unsubscribe(42)
    assert len(hdlr.publications)==0


@pytest.mark.asyncio
async def test05(srv,cl,hdlr):
    cl.bind('*', hdlr._cbStore, raw=True)
    await cl.subscribe('*')
    sid1 = await srv.publish('abc.xyz', value=42)
    await cl.unsubscribe(sid1)
    assert len(hdlr.publications)==1
    p0 = hdlr.publications[0]
    assert p0.kwargs['value']==42


@pytest.mark.asyncio
async def test06(srv,cl,hdlr):
    cl.bind('*', hdlr._cbStore, raw=True)
    sid1 = await cl.subscribe('*.xyz')
    sid2 = await cl.subscribe('abc.*')
    sid3 = await srv.publish('abc.xyz', value=42)
    await cl.unsubscribe(sid1)
    await cl.unsubscribe(sid2)
    await cl.unsubscribe(sid3)
    assert len(hdlr.publications)==1
    p0 = hdlr.publications[0]
    assert p0.kwargs['value']==42


@pytest.mark.asyncio
async def test07(srv,cl,hdlr):
    cl.bind('*', hdlr._cbStore, raw=True)
    sid1 = await cl.subscribe('*.xyz')
    sid2 = await cl.subscribe('abc.*')
    await srv.publish('abc.xyz', value=42)
    await srv.publish('abc.xyz', value=666)
    await cl.unsubscribe(sid1)
    await cl.unsubscribe(sid2)
    assert len(hdlr.publications)==2
    p0 = hdlr.publications[0]
    p1 = hdlr.publications[1]
    assert p0.kwargs['value']==42
    assert p1.kwargs['value']==666


@pytest.mark.asyncio
async def test08(srv,cl,hdlr):
    cl.bind('*', hdlr._cbStore, raw=True)
    await cl.subscribe('*')
    sid1 = await srv.publish('abc.xyz', 1,2,3,4, value=42, molo=66)
    await cl.unsubscribe(sid1)
    assert len(hdlr.publications)==1
    p0 = hdlr.publications[0]
    assert p0.args==[1,2,3,4]
    assert p0.kwargs=={'value': 42, 'molo': 66}


@pytest.mark.asyncio
async def test09(srv,cl,hdlr):
    cl.bind('abc.*', hdlr._cbStore, raw=True)
    cl.bind('*.xyz', hdlr._cbStore, raw=True)
    sid1 = await cl.subscribe('*')
    await srv.publish('abc.xyz', value=42)
    await cl.unsubscribe(sid1)
    assert len(hdlr.publications)==2
    p0 = hdlr.publications[0]
    p1 = hdlr.publications[1]
    assert p0.kwargs['value']==42
    assert p1.kwargs['value']==42


@pytest.mark.asyncio
async def test10(srv,cl,hdlr):
    cl.bind('molo.accumulate', hdlr._cbAccumulate)
    await cl.subscribe('molo.accumulate')
    for i in range(5):
        await srv.publish('molo.accumulate', value=i+1)
    await asyncio.sleep(1)
    assert hdlr.sum==1+2+3+4+5


@pytest.mark.asyncio
async def test11(srv,cl,hdlr):
    cl.bind('molo.accumulate', hdlr._cbAccumulate)
    cl.bind('molo.alert', cl.alert)
    reason = "azerty"
    await cl.subscribe('molo.*')
    async def coro():
        await srv.publish('molo.accumulate',7)
        await srv.publish('molo.accumulate',8)
        await srv.publish('molo.accumulate',9)
        await srv.publish('molo.alert', reason)
    fut = asyncio.ensure_future(coro())
    result = await cl.watch()
    assert reason==result
    assert hdlr.sum==7+8+9


@pytest.mark.asyncio
async def test12(srv,cl,hdlr):
    cl.bind('*', hdlr._cbError)
    sid1 = await cl.subscribe('*')
    await srv.publish('err.xxx', 1, 'b', ['c', 2], zorg=42, molo='z')
    await cl.unsubscribe(sid1)


@pytest.mark.asyncio
async def test13(srv,cl,hdlr):
    cl.bind('*', hdlr._cbStore, raw=True)
    sid1 = await cl.subscribe('*')
    await srv.publish('abc.xyz', 1)
    await cl.unsubscribe(sid1)
    await srv.publish('abc.xyz', 2)
    assert len(hdlr.publications)==1


@pytest.mark.asyncio
async def test14(srv,cl,hdlr):
    def prolog(socket):
        srv.publishSoonOnSocket('zorg.schmutz', socket)
    srv.addPrologue('zorg.schmutz', prolog)
    cl.bind('*', hdlr._cbStore, raw=True)
    await cl.subscribe('zorg.*')
    assert len(hdlr.publications)==0
    await asyncio.sleep(0.3)
    assert len(hdlr.publications)==1


@pytest.mark.asyncio
async def test15(srv,cl,hdlr):
    async def prolog(socket):
        await srv.publishOnSocket('zorg.schmutz', socket)
    srv.addPrologue('zorg.schmutz', prolog)
    cl.bind('*', hdlr._cbStore, raw=True)
    await cl.subscribe('zorg.*')
    assert len(hdlr.publications)==1


@pytest.mark.asyncio
async def test16(srv,cl,hdlr):
    cl.bind('*', hdlr._cbError)
    cl.bind('*', hdlr._cbStore, raw=True)
    sid1 = await cl.subscribe('*')
    await srv.publish('pouet', zorg=42)
    await cl.unsubscribe(sid1)
    assert len(hdlr.publications)==1
    print(hdlr.publications)


@pytest.mark.asyncio
async def test17(srv,cl,hdlr):
    cl.bind('*', hdlr._cbError, schedule=True)
    cl.bind('*', hdlr._cbStore, raw=True, schedule=True)
    sid1 = await cl.subscribe('*')
    await srv.publish('pouet', zorg=42)
    await cl.unsubscribe(sid1)
    assert len(hdlr.publications)==1
    print(hdlr.publications)


@pytest.mark.asyncio
async def test18(srv,cl,hdlr):
    cl.bind('abc.*', hdlr._cbStore, raw=True)
    cl.bind('*.xyz', hdlr._cbStore, raw=True)
    sid1 = await cl.subscribe('*')
    await cl.publish('abc.xyz', value=42)
    await asyncio.sleep(1)
    await cl.unsubscribe(sid1)
    assert len(hdlr.publications)==2
    p0 = hdlr.publications[0]
    p1 = hdlr.publications[1]
    assert p0.kwargs['value']==42
    assert p1.kwargs['value']==42


@pytest.mark.asyncio
async def test19(srv,cl,hdlr):
    cl.bind('abc.*', hdlr._cbStore, raw=True)
    cl.bind('*.xyz', hdlr._cbStore, raw=True)
    sid1 = await cl.subscribe('*')
    await cl.xpublish('abc.xyz', kwargs=dict(value=42))
    await asyncio.sleep(1)
    await cl.unsubscribe(sid1)
    assert len(hdlr.publications)==2
    p0 = hdlr.publications[0]
    p1 = hdlr.publications[1]
    assert p0.kwargs['value']==42
    assert p1.kwargs['value']==42


@pytest.mark.asyncio
async def test20(srv,cl,hdlr):
    cl.bind('zorg', hdlr._cbCountKeys)
    await cl.subscribe('*')
    await cl.publish('zorg', A=True, B=True)
    await cl.publish('zorg', B=True, C=True)
    await cl.publish('groz', B=True, C=True)
    await cl.publish('zorg', A=True, C=True)
    await asyncio.sleep(1)
    assert hdlr.keys_counter==dict(A=2,B=2,C=2)


@pytest.mark.asyncio
async def test21(srv,cl,hdlr):
    cl.bind('zorg', hdlr._cbCountKeys, extra_kwargs=dict(X=True))
    await cl.subscribe('*')
    await cl.publish('zorg', A=True, B=True)
    await cl.publish('zorg', C=True)
    await asyncio.sleep(1)
    assert hdlr.keys_counter==dict(A=1,B=1,C=1,X=2)
