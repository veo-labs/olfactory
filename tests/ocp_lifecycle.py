import time
import asyncio
import websockets
import unittest
import pytest
import random
from olfactory.pytest.ocp import *
from olfactory.pytest.plg import *
from olfactory import excdef, config
from olfactory.ocp import server, client


PluginManagerFixture('plgmng').inject()
OCPNodeFactoryFixture('onff').inject()
OCPServerFixture('srv').inject()
OCPServerFixture('cl').inject()


async def check_connection(cl):
    s = f"---{random.random}---"
    d = await cl.call('$return_args_and_kwargs', s)
    assert d['args'][0]==s


@pytest.mark.asyncio
async def test_listen0(onff):
    srv = onff.instanciateServer(identity='srv')


@pytest.mark.asyncio
async def test_listen1(onff):
    srv = onff.instanciateServer(identity='srv')
    await srv.listen()


@pytest.mark.asyncio
async def tes_listen2(onff):
    srv1 = onff.instanciateServer(identity='srv1')
    srv2 = onff.instanciateServer(identity='srv2')
    await srv1.listen()
    with pytest.raises(OSError):
        await srv2.listen()


@pytest.mark.asyncio
async def test_listen3(onff):
    srv = onff.instanciateServer(identity='srv')
    await srv.listen()
    with pytest.raises(excdef.StateError):
        await srv.listen()


@pytest.mark.asyncio
async def test_close0(onff):
    srv = onff.instanciateServer(identity='srv')
    with pytest.raises(excdef.StateError):
        await srv.close()


@pytest.mark.asyncio
async def test_connect0(onff):
    cl1 = onff.instanciateClient(identity='cl1')


@pytest.mark.asyncio
async def test_connect1(onff):
    cl1 = onff.instanciateClient(identity='cl1')
    with pytest.raises(ConnectionRefusedError):
        await cl1.connect()


@pytest.mark.asyncio
async def test_connect2(srv,onff):
    cl = onff.instanciateClient(identity='cl')
    await cl.connect()
    await asyncio.sleep(2)


@pytest.mark.asyncio
async def test_connect3(srv,onff):
    for ident in 'cl1','cl2','cl3':
        cl = onff.instanciateClient(identity=ident)
        await cl.connect()
        await check_connection(cl)


@pytest.mark.asyncio
async def test_connect4(srv,onff):
    cl = onff.instanciateClient(identity='cl')
    await cl.connect()
    await check_connection(cl)
    with pytest.raises(excdef.StateError):
        await cl.connect()


@pytest.mark.asyncio
async def test_connect5(onff):
    srv = onff.instanciateServer(identity='srv')
    cl = onff.instanciateClient(identity='cl')
    await srv.listen()
    await asyncio.sleep(1)
    await cl.connect()
    await check_connection(cl)
    await srv.close()
    await asyncio.sleep(1)
    with pytest.raises(ConnectionRefusedError):
        await cl.connect()
    await srv.listen()
    await asyncio.sleep(1)
    await cl.connect()
    await check_connection(cl)


@pytest.mark.asyncio
@pytest.mark.async_timeout(12)
async def test_disconnect0(srv,onff):
    cl = onff.instanciateClient(identity='cl')
    for i in range(10):
        await cl.connect()
        await check_connection(cl)
        await cl.disconnect()
        await asyncio.sleep(1)


@pytest.mark.asyncio
async def test_disconnect1(srv,onff):
    cl = onff.instanciateClient(identity='cl')
    await cl.connect()
    await check_connection(cl)
    await cl.disconnect()
    await asyncio.sleep(1)
    with pytest.raises(excdef.StateError):
        await check_connection(cl)


@pytest.mark.asyncio
async def test_disconnect2(srv,onff):
    cl = onff.instanciateClient(identity='cl')
    with pytest.raises(excdef.StateError):
        await cl.disconnect()


@pytest.mark.asyncio
async def test_disconnect3(srv,onff):
    cl = onff.instanciateClient(identity='cl')
    await cl.connect()
    await check_connection(cl)
    await cl.disconnect()
    await asyncio.sleep(1)
    with pytest.raises(excdef.StateError):
        await cl.disconnect()


@pytest.mark.asyncio
async def test_watch0(onff):
    srv = onff.instanciateServer(identity='srv')
    cl = onff.instanciateClient(identity='cl')
    await srv.listen()
    await cl.connect()
    async def coro():
        await check_connection(cl)
        await srv.close()
    loop = asyncio.get_event_loop()
    fut = loop.create_task(coro())
    with pytest.raises(excdef.ConnectionClosed):
        await cl.watch()
    await fut
