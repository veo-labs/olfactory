import * as test from "/test.js";

var _info = {};

export var start = function()
{
    var rval = null;
    var promise;

    _info = {};
    try {
        rval = test.test();
    }
    catch (exc) {
        promise = Promise.reject(exc);
        promise.then(_resolve_promise, _reject_promise);
        return;
    }
    
    if (rval.then) {
        promise = rval;
        promise.then(_resolve_promise, _reject_promise);
        return;
    }

    promise = Promise.resolve(rval);
    promise.then(_resolve_promise, _reject_promise);
}

var _resolve_promise = function(result)
{
    _mark_done();
    _info['_result'] = result;
}

var _reject_promise = function(exc)
{
    console.log(exc);
    _mark_done();
    _info['_exc'] = true;
    if (exc instanceof Event) {
        _info['msg'] = 'Event - type=' + exc['type'] + ' srcElement=' +  exc.srcElement.__proto__.toString();
    }
    else if (exc.hasOwnProperty('olfactory_fields')) {
        for (var [k, v] of exc.olfactory_fields.py_items ()) {
            _info[k] = exc.olfactory_fields[k];
        }
    }
    else if (exc.__args__) {
        _info['name'] = exc.__proto__.__module__ + '.' + exc.__proto__.__name__;
        _info['msg'] = exc.__args__[0];
    }
    else {
        _info['name'] = exc.__proto__.name;
        _info['msg'] = exc.message;
    }
}

var _mark_done = function()
{
    var html_code = "<div id='test_done'><pre>Test done !</pre></div>"
    var tmpl = document.createElement('template');
    tmpl.innerHTML = html_code;
    var place = document.getElementById('place');
    place.appendChild(tmpl.content.firstChild);
}

export var get_info = function()
{
    return _info;
}

