#!/usr/bin/env python3

import os
import sys
import shutil
import glob
from olfactory import config


QEMU = 'qemu-system-arm'
IMAGE = './2019-04-08-raspbian-stretch-lite.img'
KERNEL = 'qemu-rpi-kernel-master/kernel-qemu-4.14.79-stretch'
DTB = 'qemu-rpi-kernel-master/versatile-pb.dtb'


def main():
    conf = config.Config()
    wd = "%s/tests/qemu" % conf.getTopDir()
    os.chdir(wd)
    largs = []
    rargs = []
    xargs = largs
    for a in sys.argv[1:]:
        if a=='--':
            xargs = rargs
            continue
        xargs.append(a)
    cmd = [QEMU]
    cmd += ['-nographic']
    cmd += ['-M', 'versatilepb']
    cmd += ['-cpu', 'arm1176']
    cmd += ['-m', '256']
    cmd += ['-hda', IMAGE]
    cmd += ['-net', 'nic']
    cmd += ['-dtb', DTB]
    cmd += ['-kernel', KERNEL]
    cmd += ['-append', 'root=/dev/sda2 ro panic=1 console=ttyAMA0,38400']
    cmd += ['-serial', 'pty']
    cmd += ['-monitor', 'pty']
    cmd += rargs
    os.execvp(cmd[0], cmd)


if __name__ == '__main__':
    main()
