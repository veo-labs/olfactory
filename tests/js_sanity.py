import asyncio
import aiohttp
import pytest
import arsenic
import hashlib
import os
import random
from olfactory import config, excdef
from olfactory.pytest.wtr import *
from olfactory.pytest.ocp import *
from olfactory.pytest.plg import *
from olfactory.pytest.err import *


WebTestRunnerFixture('wtr').inject()
OCPServerFixture('srv').inject()
PluginManagerFixture('plgmng').inject()


@pytest.mark.asyncio
async def test_checksum0(srv,wtr):
    conf = config.Config()
    topdir = conf.getTopDir()
    await wtr.startHTTPD()
    async with aiohttp.ClientSession() as session:
        for bn in os.listdir(f'{topdir}/lib/js/olfactory'):
            fn = f'{topdir}/lib/js/olfactory/{bn}'
            if not os.path.isfile(fn):
                continue
            print("%s:" % bn)
            m = hashlib.md5()
            content = open(fn,'rb').read()
            m.update(content)
            lh = m.hexdigest()
            print('  local  checksum: %s' % lh)
            m = hashlib.md5()
            async with session.get(f'{HTTPD_BASEURL}/olfactory/{bn}') as resp:
                content = await resp.text()
            m.update(content.encode())
            rh = m.hexdigest()
            print('  remote checksum: %s' % rh)
            print()
            if rh != lh:
                raise AssertionError(f'checksum differs for {bn!r}')


@pytest.mark.asyncio
async def test_checksum1(srv,wtr):
    text = f'''
    import {{PYTHON_SOURCE_CHECKSUMS as checksums}} from '/olfactory/__metadata__.js';
    export function test()
    {{
        return checksums;
    }}
    '''
    await wtr.setText(text)
    checksums = await wtr.execute()
    dn = os.path.dirname(__file__)
    topdir = os.path.abspath(f'{dn}/../lib/python')
    for fn,mh in checksums.items():
        print("%s:" % fn)
        print('  meta   checksum: %s' % mh)
        m = hashlib.md5()
        content = open(f'{topdir}/{fn}','rb').read()
        m.update(content)
        lh = m.hexdigest()
        print('  local  checksum: %s' % lh)
        print()
        if lh != mh:
            raise AssertionError(f'checksum differs for {fn!r}')


@pytest.mark.asyncio
async def test_err0(srv,wtr):
    r = random.random()
    expected_msg = "===zorg_{r}_groz==="
    print(f'expected_msg = {expected_msg!r}')
    text = f'''
        export var test = function()
        {{
           throw Error('{expected_msg}');
        }}
       '''
    await wtr.setText(text)
    with raises(excdef.UnexpectedError, msg_contains=expected_msg, olfactory_fields=dict(name='Error')):
        await wtr.execute()


@pytest.mark.asyncio
async def test_err1(srv,wtr):
    text = f'''
        export var test = function()
        {{
           throw Error('zorg');
        }}
       '''
    await wtr.setText(text)
    with raises(excdef.UnexpectedError, msg_contains='zorg', olfactory_fields=dict(name='Error')):
        await wtr.execute()


@pytest.mark.asyncio
async def test_div0(srv,wtr):
    text = '''
        export function test()
        {{
            return {a}/{b};
        }}
    '''
    await wtr.testDiv(text)


@pytest.mark.asyncio
async def test_div1(srv,wtr):
    text = '''
    function _test()
    {{
        var p = new Promise(
            resolve =>
            {{
                setTimeout( () => {{ resolve({a}/{b}); }}, 1000);
            }});
        return p;
    }}
    export async function test()
    {{
        var result = await _test();
        return result;
    }}
    '''
    await wtr.testDiv(text)
