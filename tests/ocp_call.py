import asyncio
import time
import unittest
import logging
import pytest
import weakref
import functools


from olfactory.decorators import *
from olfactory import ocp, excdef
from olfactory.ocp.client import IRef
from olfactory.pytest.ocp import *
from olfactory.pytest.plg import *


PluginManagerFixture('plgmng').inject()


logger = logging.getLogger(__name__)


def add(a,b):
    return a+b

async def delayed_add(delay,a,b):
    await asyncio.sleep(delay)
    return a+b

async def sleep(delay):
    await asyncio.sleep(delay)

def throw_assertion_error(msg=None):
    if msg is None:
        raise AssertionError()
    else:
        raise AssertionError(msg)

def throw_name_error(msg=None):
    if msg is None:
        raise excdef.NameError()
    else:
        raise excdef.NameError(msg)


class MyError(Exception):
    pass


def throw_my_error(msg=None):
    if msg is None:
        raise MyError()
    else:
        raise MyError(msg)


@ocp_exception
class MyCustomError(Exception):
    pass


def throw_my_custom_error(msg=None):
    if msg is None:
        raise MyCustomError()
    else:
        raise MyCustomError(msg)


class CancellableSleep:
    async def sleep(self,delay):
        logger.info('begin sleeping for %ds' % delay)
        self.cancelled = False
        self.done = False
        try:
            await asyncio.sleep(delay)
        except asyncio.CancelledError:
            self.cancelled = True
            logger.info('cancel sleeping')
        else:
            self.done = True
            logger.info('end sleeping')
        if self.cancelled:
            raise asyncio.CancelledError


_holder = None

@pytest.fixture
def holder():
    global _holder
    _holder = set()
    yield _holder
    _holder.clear()
    _holder = None


@ocp_class
class Rectangle:
    def __init__(self, w, h):
        self.w = w
        self.h = h
        _holder.add(self)

    def __repr__(self):
        return "<Rectangle %dx%d>" % (self.w,self.h)

    @ocp_method
    def surface(self):
        return self.w * self.h

    @ocp_method
    def perimeter(self):
        return (self.w + self.h) * 2


@ocp_class
class Base:
    def __init__(self):
        _holder.add(self)

    @ocp_method
    def greets(self,name):
        return f"Hello {name} from base"


@ocp_class
class Empty(Base):
    pass


@ocp_class
class Override1(Base):
    def greets(self,name):
        return f"Hello {name} from override #1"


@ocp_class
class Override2(Base):
    @ocp_method
    def greets(self,name):
        return f"Hi {name} from override #2"


def get_rectangle_info(rectangle):
    r = rectangle
    return '%r  -  surface: %d  -  perimeter: %d' % (r,r.surface(),r.perimeter())


OCPServerFixture('srv').inject()
OCPClientFixture('cl,cl_alias').inject()
OCPClientFixture('cl_bis').inject()


async def _para(srv,cl0,cl1):
    srv.expose('delayed_add',delayed_add)
    t0 = time.monotonic()
    delay = 1.5
    done, _ = await asyncio.wait(
        [cl0.call('delayed_add', delay, 32, 10),
         cl1.call('delayed_add', delay, 444, 222)])
    t1 = time.monotonic()
    results = []
    for t in done:
        results.append(t.result())
    results.sort()
    print(results)
    assert results==[42, 666]
    dt = t1 - t0
    print(dt)
    assert dt==pytest.approx(delay,0.01)

@pytest.mark.asyncio
async def test_para0(srv,cl,cl_alias):
    await _para(srv,cl,cl_alias)

@pytest.mark.asyncio
async def test_para1(srv,cl,cl_bis):
    await _para(srv,cl,cl_bis)


@pytest.mark.asyncio
async def test_func0(srv,cl):
    srv.expose('add',add)
    res = await cl.call('add', 32, 10)
    assert res==42


@pytest.mark.asyncio
async def test_func1(srv,cl):
    delay = 1.5
    delta = 0.1
    srv.expose('sleep',sleep)
    t0 = time.monotonic()
    res = await cl.call('sleep', delay)
    t1 = time.monotonic()
    dt = t1-t0
    print(delay, dt)
    assert delay-delta < dt < delay+delta


@pytest.mark.asyncio
async def test_func2(srv,cl):
    with pytest.raises(excdef.NameError):
        await cl.call('invalid', 42)


@pytest.mark.asyncio
async def test_func3(srv,cl):
    srv.expose('throw_name_error',throw_name_error)
    try:
        await cl.call('throw_name_error', 'abc')
    except excdef.NameError as ev:
        assert str(ev)=='abc'
    else:
         raise AssertionError()


@pytest.mark.asyncio
async def test_func4(srv,cl):
    srv.expose('throw_assertion_error',throw_assertion_error)
    try:
        await cl.call('throw_assertion_error', 'xyz')
    except AssertionError as ev:
         assert str(ev)=='xyz'
    else:
         raise AssertionError()


@pytest.mark.asyncio
async def test_func5(srv,cl):
    srv.expose('throw_name_error',throw_name_error)
    try:
        await cl.call('throw_name_error')
    except excdef.NameError as ev:
        pass
    else:
         raise AssertionError()


@pytest.mark.asyncio
async def test_func6(srv,cl):
    srv.expose('throw_assertion_error',throw_assertion_error)
    try:
        await cl.call('throw_assertion_error')
    except AssertionError as ev:
        pass
    else:
         raise AssertionError()


@pytest.mark.asyncio
async def test_func7(srv,cl):
    srv.expose('delayed_add',delayed_add)
    fut = asyncio.ensure_future(cl.call('delayed_add', 3, 32, 10))
    res = await fut
    assert res==42


@pytest.mark.asyncio
async def test_func8(srv,cl,holder):
    minus58 = functools.partial(add, -58)
    holder.add(minus58)
    srv.expose('minus58',minus58)
    res = await cl.call('minus58', 100)
    assert res==42


@pytest.mark.asyncio
async def test_func9(srv,cl,holder):
    delayed_minus58 = functools.partial(delayed_add, 2, -58)
    holder.add(delayed_minus58)
    srv.expose('delayed_minus58',delayed_minus58)
    res = await cl.call('delayed_minus58', 100)
    assert res==42


@pytest.mark.asyncio
async def test_func10(srv,cl):
    srv.expose('throw_my_error',throw_my_error)
    with pytest.raises(excdef.UnexpectedError):
        await cl.call('throw_my_error')


@pytest.mark.asyncio
async def test_func11(srv,cl):
    srv.expose('throw_my_custom_error',throw_my_custom_error)
    with pytest.raises(MyCustomError):
        await cl.call('throw_my_custom_error')


@pytest.mark.asyncio
async def test_abort0(srv,cl):
    srv.expose('delayed_add',delayed_add)
    fut = asyncio.ensure_future(cl.call('delayed_add', 10, 32, 10))
    await asyncio.sleep(2)
    fut.cancel()


@pytest.mark.asyncio
async def test_abort1(srv,cl):
    srv.expose('delayed_add',delayed_add)
    requestid = []
    fut = asyncio.ensure_future(cl.xcall('delayed_add', args=[10, 32, 10], requestid_getter=requestid.append))
    await asyncio.sleep(2)
    await cl.abort(requestid[0])
    with pytest.raises(asyncio.CancelledError):
        await fut


@pytest.mark.asyncio
async def test_abort2(srv,cl):
    srv.expose('delayed_add',delayed_add)
    fut = asyncio.ensure_future(cl.call('delayed_add', 10, 32, 10))
    with pytest.raises(asyncio.TimeoutError):
        await asyncio.wait_for(fut, 2)
    if not fut.cancelled():
        raise AssertionError('future not cancelled')


@pytest.mark.asyncio
async def test_abort3(srv,cl):
    cs = CancellableSleep()
    srv.expose('sleep',cs.sleep)
    requestid = []
    await cl.xpush('sleep', args=[10], requestid_getter=requestid.append)
    await asyncio.sleep(2)
    await cl.abort(requestid[0])
    await asyncio.sleep(1)
    if cs.done or not cs.cancelled:
        raise AssertionError('abort failed')


@pytest.mark.asyncio
async def test_inst0(srv,cl,holder):
    srv.expose('Rectangle', Rectangle)
    rect = await cl.call('Rectangle', 3, 4)
    p = await cl.callm(rect, 'perimeter')
    print(p)
    assert p==14


@pytest.mark.asyncio
async def test_inst1(srv,cl,holder):
    srv.expose('Rectangle', Rectangle)
    rect = await cl.call('Rectangle', 3, 4)
    s = await cl.callm(rect, 'surface')
    print(s)
    assert s==12


@pytest.mark.asyncio
async def test_inst2(srv,cl,holder):
    srv.expose('Rectangle', Rectangle)
    rect = await cl.call('Rectangle', 3, 4)
    with pytest.raises(excdef.NameError):
       await cl.callm(rect, 'invalid')


@pytest.mark.asyncio
async def test_inst3(srv,cl):
    inst = IRef('0123456789')
    with pytest.raises(excdef.LabelError):
        await cl.callm(inst, 'invalid')


@pytest.mark.asyncio
async def test_inst4(srv,cl):
    inst = IRef('0x0123456789')
    with pytest.raises(excdef.LabelError):
        await cl.callm(inst, 'invalid')


@pytest.mark.asyncio
async def test_inst5(srv,cl,holder):
    srv.expose('Rectangle', Rectangle)
    srv.expose('get_rectangle_info', get_rectangle_info)
    rect = await cl.call('Rectangle', 5, 2)
    s = await cl.call('get_rectangle_info', rect)
    print(s)
    if s != '<Rectangle 5x2>  -  surface: 10  -  perimeter: 14':
        raise AssertionError('unexpected returned value for get_rectangle_info(): %r' % s)


@pytest.mark.asyncio
async def test_inst6(srv,cl):
    inst = IRef('0x0123456789')
    with pytest.raises(excdef.LabelError):
        await cl.call('$get_attribute', inst, 'zorg')


@pytest.mark.asyncio
async def test_inst7(srv,cl,holder):
    srv.expose('Empty', Empty)
    d = await cl.call('Empty')
    msg = await cl.callm(d, 'greets', 'zorg')
    print(msg)
    assert 'base' in msg


@pytest.mark.asyncio
async def test_inst8(srv,cl,holder):
    srv.expose('Override1', Override1)
    d = await cl.call('Override1')
    with pytest.raises(excdef.NameError):
        msg = await cl.callm(d, 'greets', 'zorg')


@pytest.mark.asyncio
async def test_inst9(srv,cl,holder):
    srv.expose('Override2', Override2)
    d = await cl.call('Override2')
    msg = await cl.callm(d, 'greets', 'zorg')
    print(msg)
    assert 'override #2' in msg


@pytest.mark.asyncio
async def test_inst10(srv,cl,holder):
    _ = lambda w,h: weakref.ref(Rectangle(w,h))
    srv.expose('Rectangle', _)
    rect = await cl.call('Rectangle', 8, 13)
    p = await cl.callm(rect, 'perimeter')
    print(p)
    assert p==42
