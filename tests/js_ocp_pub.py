import pytest
from olfactory.pytest.wtr import *
from olfactory.pytest.ocp import *
from olfactory.pytest.plg import *
from olfactory import config


WebTestRunnerFixture('wtr').inject()
OCPServerFixture('srv').inject()
PluginManagerFixture('plgmng').inject()


@pytest.mark.asyncio
async def test_pub0(srv,wtr):
    conf = config.Config()
    url = conf.getItem('test_ocp_connect_url')
    text = f'''
    import {{__kwargtrans__ as $kw}} from '/olfactory/org.transcrypt.__runtime__.js';
    import * as client from "/olfactory/olfactory.ocp.client.js";
    import * as asyncio from "/olfactory/olfactory.transcrypt.asyncio.js";
    export async function test()
    {{
        var result = 0;
        var acc = function(value) {{
            result += value;
        }}
        var cl = client.Client('{url}', $kw({{'identity': 'JScli'}}));
        await cl.connect();
        cl.bind('*', acc);
        await cl.subscribe('*.acc');
        for(var i=1; i<=5; i++) {{
            await cl.publish("z{{}}z.acc".format(i), 2*i);
            await cl.publish("acc.z{{}}z".format(i), 3*i);
        }}
        await asyncio.sleep(1);
        return result;
    }}
    '''
    await wtr.setText(text)
    result = await wtr.execute(timeout=2)
    assert result==2+4+6+8+10


JSCODE_PUB1_v1 = """
        cl.bind('*.zorg', count_kwarg_keys);
        await cl.publish("pouet.groz", 1, 2, 3, $kw({'A': true, 'B': true}));
        await cl.publish("pouet.zorg", 4, 5, 6, $kw({'A': true, 'C': true}));
        await cl.publish("schmutz.zorg", 7, 8, 9, $kw({'B': true, 'C': true}));
        await cl.publish("xxxx.zorg", $kw({'A': true, 'B': true}));
"""

JSCODE_PUB1_v2 = """
        cl.bind('*.zorg', count_kwarg_keys);
        await cl.xpublish("pouet.groz", $kw({'args': [1, 2], 'kwargs': {'A': true}}));
        await cl.xpublish("pouet.zorg", $kw({'args': [3, 4], 'kwargs': {'B': true}}));
        await cl.xpublish("schmutz.zorg", $kw({'args': [5, 6], 'kwargs': {'C': true}}));
        await cl.xpublish("xxxx.zorg", $kw({'kwargs': {'A': true}}));
"""

JSCODE_PUB1_v3 = """
        cl.bind('*.zorg', count_kwarg_keys, $kw({'extra_kwargs': {'X': true}}));
        await cl.publish("1.zorg", $kw({'A': true, 'B': true}));
        await cl.publish("2.zorg", $kw({'A': true, 'C': true}));
        await cl.publish("3.zorg");
"""

@pytest.mark.asyncio
@pytest.mark.parametrize('jscode,expected_result', [
    (JSCODE_PUB1_v1, dict(A=2,B=2,C=2,_total_args_length=6)),
    (JSCODE_PUB1_v2, dict(A=1,B=1,C=1, _total_args_length=4)),
    (JSCODE_PUB1_v3, dict(A=2,B=1,C=1,X=3,_total_args_length=0))
    ], ids=['v1', 'v2', 'v3'])
async def test_pub1(srv,wtr,jscode,expected_result):
    conf = config.Config()
    url = conf.getItem('test_ocp_connect_url')
    text = f'''
    import {{__kwargtrans__ as $kw}} from '/olfactory/org.transcrypt.__runtime__.js';
    import * as client from "/olfactory/olfactory.ocp.client.js";
    import * as asyncio from "/olfactory/olfactory.transcrypt.asyncio.js";
    export async function test()
    {{
        var counters = {{ '_total_args_length': 0 }};
        var count_kwarg_keys = function(...args) {{
            var kwargs = args.pop();
            counters['_total_args_length'] += args.length;
            for (var k in kwargs) {{
                if (k in counters)
                    counters[k] += 1;
                else
                    counters[k] = 1;
            }}
        }}
        var cl = client.Client('{url}', $kw({{'identity': 'JScli'}}));
        await cl.connect();
        await cl.subscribe('*');
    '''
    text += jscode
    text += '''
        await asyncio.sleep(1);
        return counters;
    }
    '''
    await wtr.setText(text)
    result = await wtr.execute(timeout=2)
    assert result==expected_result
