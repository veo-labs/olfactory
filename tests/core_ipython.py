import asyncio
import random
import time
import pytest
from olfactory.pytest.ipy import *
from olfactory.pytest.ocp import *
from olfactory.pytest.plg import *
from olfactory.excdef import *


class MyIPythonFixture(IPythonFixture):
    async def _up(self):
        ipy = await super()._up()
        await ipy.cmd('%load_ext olfactory.ipython')
        await ipy.cmd('%%run %s' % __file__)
        await ipy.cmd('from olfactory import deleg')
        await ipy.cmd('mill = deleg.Function(mill)')
        await ipy.cmd('m = Mill()')
        await ipy.cmd('m = deleg.Instance(m)')
        return ipy


OCPServerFixture('srv').inject()
OCPClientFixture('cl').inject()
PluginManagerFixture('plgmng').inject()
MyIPythonFixture('ipy').inject()


async def mill(*, pr=None, count=None, ret=None, tick=0.4):
    i = 0
    while True:
        if pr is not None:
            print('%s' % pr)
        await asyncio.sleep(tick)
        i += 1
        if count is not None:
            if i >= count:
                break
    if ret is not None:
        return ret


@pytest.fixture
async def repl(cl):
    kwargs = {}
    kwargs['class'] = 'replicant'
    kwargs['jobexts'] = ['repl','sh']
    chan = await cl.xcall('job::run', kwargs=kwargs)
    await cl.callm(chan,'syncp')
    return chan


class Mill:
    async def mill(self, **kwargs):
        return await mill(**kwargs)


@pytest.mark.asyncio
async def test_echo0(srv,ipy):
    r = str(random.random())
    x0 = await ipy.cmd('!echo %s' % r)
    print(x0.text)
    if x0.text.count(r) != 2:
        raise AssertionError("unexpected text")


@pytest.mark.asyncio
async def test_err0(srv,ipy):
    with pytest.raises(IPythonCmdError):
        await ipy.cmd('raise AssertionError')


@pytest.mark.asyncio
@pytest.mark.parametrize("cmd", [
    'mill(count=5,pr="ZORG")',
    'm.mill(count=5,pr="ZORG")'],
    ids=['func','meth'])
async def test_mill0(srv,cl,ipy,cmd):
    x = await ipy.cmd(cmd)
    print(x.out)
    assert x.out.count('ZORG')==5


@pytest.mark.asyncio
@pytest.mark.parametrize("cmd", [
    'fut = mill.opts(blocking=False)(count=5,tick=0.35,pr=None,ret="ZORG")',
    'fut = m.mill.opts(blocking=False)(count=5,tick=0.35,pr=None,ret="ZORG")'],
    ids=['func','meth'])
async def test_mill1(srv,cl,ipy,cmd):
    await ipy.cmd(cmd)
    x = await ipy.cmd('fut')
    print(x.out)
    if 'pending' not in x.out:
        raise AssertionError("'pending' expected")
    await asyncio.sleep(2)
    x = await ipy.cmd('fut')
    print(x.out)
    if 'finished' not in x.out:
        raise AssertionError("'finished' expected")
    x = await ipy.cmd('fut.result()')
    print(x.out)
    if 'ZORG' not in x.out:
        raise AssertionError("'ZORG' expected")


@pytest.mark.asyncio
async def test_ext0(srv,ipy):
    await ipy.cmd('olf.connect("ws://127.0.0.1:9087")')
    try:
        x = await ipy.cmd('olf.call("$delayed_div", 0.5, 84, 2)')
        if '42' not in x.out:
            raise AssertionError('42 expected in output')
    finally:
        await ipy.cmd('olf.disconnect()')


@pytest.mark.asyncio
async def test_repl0(srv,ipy,repl):
    await ipy.cmd('olf.connect("ws://127.0.0.1:9087")')
    try:
        await ipy.cmd('olf.rescanJobs()')
        x = await ipy.cmd('olf.jobs')
        assert x.out.count('replicant0')==1
    finally:
        await ipy.cmd('olf.disconnect()')
