import os
import aiohttp
import hashlib
import random
import arsenic
import pytest
import asyncio
from olfactory import excdef, config
from olfactory.pytest.wtr import *
from olfactory.pytest.ocp import *
from olfactory.pytest.plg import *
from olfactory.pytest.err import *
from olfactory.decorators import *


OCPServerFixture('srv').inject()
OCPServerFixture('srv2',close_on_teardown=False).inject()
WebTestRunnerFixture('wtr').inject()
PluginManagerFixture('plgmng').inject()


_holder = None

@pytest.fixture
def holder():
    global _holder
    _holder = set()
    yield
    _holder.clear()
    _holder = None


@ocp_class
class Rectangle:
    def __init__(self, w, h):
        self.w = w
        self.h = h
        _holder.add(self)

    def __repr__(self):
        return "<Rectangle {}x{}>".format(self.w,self.h)

    @ocp_method
    def surface(self):
        return self.w * self.h

    @ocp_method
    def perimeter(self):
        return (self.w + self.h) * 2

def get_rectangle_info(rectangle):
    r = rectangle
    return '%r  -  surface: %d  -  perimeter: %d' % (r,r.surface(),r.perimeter())


@pytest.mark.asyncio
async def test_div2(srv,wtr):
    text = '''
    import {{__kwargtrans__ as $kw}} from '/olfactory/org.transcrypt.__runtime__.js';
    import * as client from "/olfactory/olfactory.ocp.client.js";
    export async function test()
    {{
        var cl = client.Client('{url}', $kw({{'identity': 'JScli'}}));
        await cl.connect();
        var result = await cl.call('$delayed_div', 1, {a}, {b});
        return result;
    }}
    '''
    await wtr.testDiv(text)


@pytest.mark.asyncio
async def test_div3(srv,wtr):
    text = '''
    import {{__kwargtrans__ as $kw}} from '/olfactory/org.transcrypt.__runtime__.js';
    import * as client from "/olfactory/olfactory.ocp.client.js";
    export async function test()
    {{
        var cl = client.Client('{url}', $kw({{'identity': 'JScli'}}));
        await cl.connect();
        var result = await cl.call('$delayed_div', 1, {a}, 0);
        return result;
    }}
    '''
    with raises(ZeroDivisionError,msg_contains='division by zero'):
        await wtr.testDiv(text)


@pytest.mark.asyncio
async def test_div4(srv,wtr):
    text = '''
    import {{__kwargtrans__ as $kw}} from '/olfactory/org.transcrypt.__runtime__.js';
    import * as asyncio from "/olfactory/olfactory.transcrypt.asyncio.js";
    import * as client from "/olfactory/olfactory.ocp.client.js";
    export async function test()
    {{
        var cl = client.Client('{url}', $kw({{'identity': 'JScli'}}));
        await cl.connect();
        var requestid;
        var p = cl.xcall('$delayed_div', $kw({{'args': [500, {a}, {b}], 'requestid_getter': _=>{{requestid=_;}} }}));
        await asyncio.sleep(1);
        await cl.abort(requestid);
        await p;
    }}
    '''
    with raises(asyncio.CancelledError):
        await wtr.testDiv(text)


@pytest.mark.asyncio
async def test_iref0(srv,wtr):
    conf = config.Config()
    url = conf.getItem('test_ocp_connect_url')
    text = f'''
    import {{__kwargtrans__ as $kw}} from '/olfactory/org.transcrypt.__runtime__.js';
    import * as asyncio from "/olfactory/olfactory.transcrypt.asyncio.js";
    import * as client from "/olfactory/olfactory.ocp.client.js";
    export async function test()
    {{
        var cl = client.Client('{url}', $kw({{'identity': 'JScli'}}));
        await cl.connect();
        var iref0 = client.IRef('iref0');
        await cl.call('$get_attribute', iref0, 'zorg');
    }}
    '''
    await wtr.setText(text)
    with raises(excdef.LabelError):
        await wtr.execute(timeout=2)


@pytest.mark.asyncio
async def test_rect0(srv,wtr,holder):
    conf = config.Config()
    url = conf.getItem('test_ocp_connect_url')
    srv.expose('Rectangle', Rectangle)
    text = f'''
    import {{__kwargtrans__ as $kw}} from '/olfactory/org.transcrypt.__runtime__.js';
    import * as client from "/olfactory/olfactory.ocp.client.js";
    export async function test()
    {{
        var cl = client.Client('{url}', $kw({{'identity': 'JScli'}}));
        await cl.connect();
        var r = await cl.call('Rectangle', 3, 4);
        var s = await cl.callm(r, 'surface');
        return s;
    }}
    '''
    await wtr.setText(text)
    s = await wtr.execute(timeout=2)
    print()
    print("*** surface={} ***".format(s))
    print()
    assert s==12


@pytest.mark.asyncio
async def test_rect1(srv,wtr,holder):
    conf = config.Config()
    url = conf.getItem('test_ocp_connect_url')
    srv.expose('Rectangle', Rectangle)
    srv.expose('get_rectangle_info', get_rectangle_info)
    text = f'''
    import {{__kwargtrans__ as $kw}} from '/olfactory/org.transcrypt.__runtime__.js';
    import * as client from "/olfactory/olfactory.ocp.client.js";
    export async function test()
    {{
        var cl = client.Client('{url}', $kw({{'identity': 'JScli'}}));
        await cl.connect();
        var r = await cl.call('Rectangle', 5, 2);
        var s = await cl.call('get_rectangle_info',r);
        return s;
    }}
    '''
    await wtr.setText(text)
    s = await wtr.execute(timeout=2)
    print("*** %s ***" % s)
    if s != '<Rectangle 5x2>  -  surface: 10  -  perimeter: 14':
        raise AssertionError('unexpected returned value for get_rectangle_info(): %r' % s)


@pytest.mark.asyncio
async def test_kwargs0(srv,wtr):
    conf = config.Config()
    url = conf.getItem('test_ocp_connect_url')
    text = f'''
    import {{__kwargtrans__ as $kw}} from '/olfactory/org.transcrypt.__runtime__.js';
    import * as client from "/olfactory/olfactory.ocp.client.js";
    export async function test()
    {{
        var cl = client.Client('{url}', $kw({{'identity': 'JScli'}}));
        await cl.connect();
        var d = await cl.call('$return_args_and_kwargs', 1, 2, 3, $kw({{'A': 'zorg', 'B': true}}));
        return d;
    }}
    '''
    await wtr.setText(text)
    d = await wtr.execute(timeout=2)
    print(d)
    assert d==dict(args=[1,2,3],kwargs={'A': 'zorg', 'B': True})


@pytest.mark.asyncio
async def test_kwargs1(srv,wtr):
    conf = config.Config()
    url = conf.getItem('test_ocp_connect_url')
    text = f'''
    import {{__kwargtrans__ as $kw}} from '/olfactory/org.transcrypt.__runtime__.js';
    import * as client from "/olfactory/olfactory.ocp.client.js";
    export async function test()
    {{
        var cl = client.Client('{url}', $kw({{'identity': 'JScli'}}));
        await cl.connect();
        var d = await cl.xcall('$return_args_and_kwargs', $kw({{'args': [10,20,30], 'kwargs': {{'A': 'zorg', 'B': true}} }} ));
        return d;
    }}
    '''
    await wtr.setText(text)
    d = await wtr.execute(timeout=2)
    print(d)
    assert d==dict(args=[10,20,30],kwargs={'A': 'zorg', 'B': True})


class MyError(Exception):
    pass


def throw_my_error(msg=None):
    if msg is None:
        raise MyError()
    else:
        raise MyError(msg)


@ocp_exception
class MyCustomError(Exception):
    pass


def throw_my_custom_error(msg=None):
    if msg is None:
        raise MyCustomError()
    else:
        raise MyCustomError(msg)


@pytest.mark.asyncio
async def test_error1(srv,wtr):
    srv.expose('throw_my_error',throw_my_error)
    text = '''
    import {{__kwargtrans__ as $kw}} from '/olfactory/org.transcrypt.__runtime__.js';
    import * as client from "/olfactory/olfactory.ocp.client.js";
    export async function test()
    {{
        var cl = client.Client('{url}', $kw({{'identity': 'JScli'}}));
        await cl.connect();
        var result = await cl.call('throw_my_error','coucou');
        return result;
    }}
    '''
    with raises(excdef.UnexpectedError,msg_contains='coucou',olfactory_fields=dict(serialized=True)):
        await wtr.testDiv(text)


@pytest.mark.asyncio
async def test_error2(srv,wtr):
    srv.expose('throw_my_custom_error',throw_my_custom_error)
    text = '''
    import {{__kwargtrans__ as $kw}} from '/olfactory/org.transcrypt.__runtime__.js';
    import * as client from "/olfactory/olfactory.ocp.client.js";
    export async function test()
    {{
        var cl = client.Client('{url}', $kw({{'identity': 'JScli'}}));
        await cl.connect();
        var result = await cl.call('throw_my_custom_error','zorg');
        return result;
    }}
    '''
    with raises(MyCustomError,msg_contains='zorg',olfactory_fields=dict(serialized=True)):
        await wtr.testDiv(text)


@pytest.mark.asyncio
async def test_error3(srv,wtr):
    srv.expose('throw_my_custom_error',throw_my_custom_error)
    text = '''
    import {{__kwargtrans__ as $kw}} from '/olfactory/org.transcrypt.__runtime__.js';
    import * as excdef from "/olfactory/olfactory.excdef.js";
    export async function test()
    {{
        var ev = new excdef.NameError('zorg');
        throw ev;
    }}
    '''
    with raises(excdef.NameError,msg_contains='zorg',olfactory_fields=dict(serialized=False)):
        await wtr.testDiv(text)


@pytest.mark.asyncio
@pytest.mark.async_timeout(None)
async def test_watch0(srv2,wtr):
    conf = config.Config()
    url = conf.getItem('test_ocp_connect_url')
    text = f'''
    import {{__kwargtrans__ as $kw}} from '/olfactory/org.transcrypt.__runtime__.js';
    import {{isinstance}} from '/olfactory/org.transcrypt.__runtime__.js';
    import * as client from "/olfactory/olfactory.ocp.client.js";
    import * as excdef from "/olfactory/olfactory.excdef.js";
    export async function test()
    {{
        var cl = client.Client('{url}', $kw({{'identity': 'JScli'}}));
        await cl.connect();
        await cl.watch();
    }}
    '''

    await wtr.setText(text)
    async def _():
        for i in range(3,-1,-1):
            print((i))
            await asyncio.sleep(1)
        await srv2.close()
        print('boom !')
    with raises(excdef.ConnectionClosed):
        await asyncio.gather(_(), wtr.execute(timeout=5))
