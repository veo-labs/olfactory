import asyncio
import pytest
from olfactory import prochlp


FIND_CMD = r'find / -exec echo {} \; -exec sleep 0.3 \;'
LS_CMD = 'ls /'


@pytest.mark.asyncio
@pytest.mark.async_timeout(20)
async def test_terminate0():
    p = prochlp.ProcessRunner(FIND_CMD,identity='find',shell=True)
    await p.launch()
    await asyncio.sleep(5)
    p.terminate()
    await asyncio.sleep(1)


@pytest.mark.asyncio
@pytest.mark.async_timeout(20)
async def test_terminate1():
    p = prochlp.ProcessRunner(LS_CMD,identity='ls',shell=True,auto_relaunch=1)
    await p.launch()
    await asyncio.sleep(5)
    p.disableAutoRelaunch()
    p.terminate()
    await asyncio.sleep(1)


@pytest.mark.asyncio
async def test_cancel0():
    p = prochlp.ProcessRunner(FIND_CMD,identity='find',shell=True)
    await p.launch()
    await asyncio.sleep(2)
    p.cancelBackgroundTask()
    await p.wait()
    await asyncio.sleep(1)


@pytest.mark.asyncio
@pytest.mark.async_timeout(20)
async def test_cancel1():
    p = prochlp.ProcessRunner(LS_CMD,identity='ls',shell=True,auto_relaunch=1)
    await p.launch()
    await asyncio.sleep(5)
    p.disableAutoRelaunch()
    p.cancelBackgroundTask()
    await p.wait()
    await asyncio.sleep(1)
